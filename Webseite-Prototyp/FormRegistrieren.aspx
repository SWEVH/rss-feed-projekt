﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FormRegistrieren.aspx.cs" Inherits="FormRegistrieren" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 191px;
            text-align: right;
        }
        .auto-style3 {
            width: 191px;
            text-align: right;
            height: 26px;
        }
        .auto-style4 {
            height: 26px;
            width: 229px;
            text-align: left;
        }
        .auto-style5 {
            width: 68px;
        }
        .auto-style6 {
            height: 26px;
            width: 68px;
        }
        .auto-style7 {
            width: 229px;
            text-align: left;
        }
        .auto-style8 {
            width: 100%;
            text-align: center;
        }
        .auto-style9 {
            font-size: x-large;
        }
    </style>
        <script src="jquery-2.2.2.js"></script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <div class="auto-style8">
    
        <strong class="auto-style9">Registrieren</strong></div>
    
        <table class="auto-style1">
            <tr>
                <td class="auto-style2">E-mail:</td>
                <td class="auto-style5">
                    <asp:TextBox ID="TextBoxEmail" runat="server" Height="25px" Width="200px"></asp:TextBox>
                </td>
                <td class="auto-style7">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxEmail" ErrorMessage="Email ist erforderlich" ForeColor="Red"></asp:RequiredFieldValidator>
                    <br />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxEmail" ErrorMessage="Email Adresse ist nicht gültig" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Passwort:</td>
                <td class="auto-style6">
                    <asp:TextBox ID="TextBoxPass" runat="server" Height="25px" TextMode="Password" Width="200px"></asp:TextBox>
                </td>
                <td class="auto-style4">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxPass" ErrorMessage="Passwort ist erforderlich" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Passwort wiederholen:</td>
                <td class="auto-style5">
                    <asp:TextBox ID="TextBoxWPass" runat="server" Height="25px" TextMode="Password" Width="200px"></asp:TextBox>
                </td>
                <td class="auto-style7">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxWPass" ErrorMessage="Passwort wiederholen ist erforderlich" ForeColor="Red"></asp:RequiredFieldValidator>
                    <br />
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TextBoxPass" ControlToValidate="TextBoxWPass" ErrorMessage="Beide Passwörter müssen gleich sein" ForeColor="Red"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style5">
                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Registrieren" />
                </td>
                <td class="auto-style7">&nbsp;</td>
            </tr>
            </table>
    
    </div>
    </form>
</body>
</html>
