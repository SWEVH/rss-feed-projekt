﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Main.aspx.cs" Inherits="Main" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Pressespiegel anzeigen</title>
    <link rel="stylesheet" type="text/css" href="design.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="content header">
            <header>
                <br />
                <br />
                <h1>Online Pressespiegel</h1>
                <br />
            </header>
        </div>
        <div id="page">
            <h2>Einfach Pressespiegel</h2>
            <h3>Aus Online-Quellen</h3>
            <div id="main">
                <br />
                <br />
                <asp:ListBox ID="oListe" runat="server"  BackColor="#3399ff"  OnSelectedIndexChanged ="Clickevent" Height="700px" Width="80%" ></asp:ListBox>            
                <br />
                <br />
            </div>
            <br />
            <br />
            <br />
            <div id="page-footer">
                Dieses projekt wurde im Auftrag der DHBW Horb erstellt<br />
                <a href="http://jacky.hackergrotte.de:85/Wiki/index.php/Hauptseite">das Team</a>&nbsp;|
                  <a href="">Link2</a>&nbsp;|
                  <a href="">Link3</a>&nbsp;|
                  <a href="" target="_blank">Link4</a>&nbsp;|
                  <a href="" target="_blank">Link5</a>
            </div>
        </div>
    </form>
</body>
</html>