﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;
using System.IO;
using System.Net.Mail;
using System.Net;

public partial class FormRegistrieren : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string salt = "bgfe7384yeuhf49873y";

    protected void Button1_Click(object sender, EventArgs e)
    {

        Database oDatabase = new Database("root", "", "rss");

        //Prüfen ob die Email Adresse schon vorhanden ist
        ArrayList checkEmail = oDatabase.GetColumnValues("user", "Email");

        for (int i = 0; i < checkEmail.Count; i++)
        {
            if (checkEmail[i].ToString().Equals(TextBoxEmail.Text))
            {
                Response.Write("Email Adresse ist schon vorhanden");
                return;
            }
           
        }
        

        // Passwort verschlüsseln
        byte[] hash;
        using (var sha1 = new SHA1CryptoServiceProvider())
        {
            hash = sha1.ComputeHash(sha1.ComputeHash(Encoding.Unicode.GetBytes(TextBoxPass.Text + salt)));
        }
        var sb = new StringBuilder();
        foreach (byte b in hash) sb.AppendFormat("{0:x2}", b);
        
        //Email und Password in der Datenbank speichern - ein Konto in der Datenbank einlegen 
        ArrayList insAr = oDatabase.ExceuteCommand("insert into user(Email, Username, Passwort) values ( '" + TextBoxEmail.Text + "','" + TextBoxEmail.Text + "','" +sb.ToString()+ "')");


        string Email = TextBoxEmail.Text;
        string Receiver = Email;
        MailMessage mail = new MailMessage();
        SmtpClient client = new SmtpClient();
        client.Port = 25;
        client.DeliveryMethod = SmtpDeliveryMethod.Network;
        client.UseDefaultCredentials = false;
        client.Host = "localhost";
        mail.To.Add(new MailAddress(Receiver));
        mail.From = new MailAddress("RSS-Feed@jacky.hackergrotte.de");
        mail.Subject = "Registrierung abschließen"; 
        mail.Body = "Klicken Sie <a href=http://jacky.hackergrotte.de:82/Settings/Einstellungen.aspx?Email=" + Email + ">hier</a> um sich ihre Registrierung abzuschließen.";
        mail.Sender = new MailAddress("RSS-Feed@jacky.hackergrotte.de");
        mail.IsBodyHtml = true;
        client.Send(mail);

        Response.Write("Sie haben sich erfolgreich registrieren");

        Response.Redirect("FormLogin.aspx");


    }
}