﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;
using System.IO;
using System.Net.Mail;
using System.Net;

public partial class FormLogin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string salt = "bgfe7384yeuhf49873y";
    public Boolean found = false;
    protected void ButtonAnmelden_Click(object sender, EventArgs e)
    {

        Database oDatabase = new Database("root", "", "rss");

        // Prüfen ob die eingegebene Email Adresse in der Datenbank schon vorhanden ist
        ArrayList selAr = oDatabase.GetColumnValues("user", "Email");

       for (int i = 0; i < selAr.Count; i++)
        {
            if (selAr.Contains(EmailLogin.Text))
            {

                //Versclüsselung den eingegebenen Passwort um zu prüfen ob er gleich wie in der Datenbank gespeichertes Passwort ist. 
                byte[] hash;
                using (var sha1 = new SHA1CryptoServiceProvider())
                {
                    hash = sha1.ComputeHash(sha1.ComputeHash(Encoding.Unicode.GetBytes(PasswortLogin.Text + salt)));
                }
                var sb = new StringBuilder();
                foreach (byte b in hash) sb.AppendFormat("{0:x2}", b);

                String passwordHolen = sb.ToString().Replace(" ", "");

                // Prüfen ob der eingegebenen Passwort gleich wie in der Datenbank gespeichertes Passwort mit der entsprechende Email Adresse ist
                ArrayList selectPass = oDatabase.GetColumnValues("user", "Passwort");
                              
                for (int j = 0; j < selectPass.Count; j++)
                {
                    if (selectPass.Contains(passwordHolen))
                    {
                        Session["New"] = EmailLogin.Text;
                       // Response.Write("Richtiges Passwort");
                        Response.Redirect("~/Main.aspx?Email="+EmailLogin.Text);
                    }
                    else
                    {
                        found = true;
                        

                    }
                }
            }
            else
            {
                found = false;
                
            }
        }
        if (found == true)
        {
            Response.Write("Passwort ist falsch");
        }
        else
        {
            Response.Write("Email ist falsch");
        }

        
    }
}