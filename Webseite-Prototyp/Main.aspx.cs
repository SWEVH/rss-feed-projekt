﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

public partial class Main : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

            
            string Email = "Test@Test.com";
            if (Request.QueryString.HasKeys())
            {
                Email = Request.QueryString.Get("Email");
            }
            Database oDatabase = new Database("root", "", "rss");

            ArrayList User = oDatabase.ExceuteCommand("SELECT ID FROM user WHERE Email = '" + Email + "'");
            int UserID = (int)User[0];
            //benutzer abfragen

            ArrayList IDUserinteresse = oDatabase.ExceuteCommand("SELECT InteressenID FROM userinteresse WHERE UserID = " + UserID);
            //0 = UserID     1 = InteresseID
            //anhand UserID bekomme Interessen

            List<int> Interessen = new List<int>();
            for (int i = 0; i < IDUserinteresse.Count; i++)
            {
                Interessen.Add((int)IDUserinteresse[i]);
            }


            ArrayList IDliste = oDatabase.ExceuteCommand("SELECT * FROM berichtinteresse");
            //0 = BerichtID     1 = InteresseID
            //jede gerade Zahl ist ein BerichtID


            ArrayList BerichtIDs = new ArrayList();
            //Hier wird anhand der Userinteressen BerichtID in eine Liste geladen
            for (int i = 0; i < Interessen.Count; i++)
            {
                for (int j = 1; j < IDliste.Count; j += 2)
                {
                    if ((int)Interessen[i] == (int)IDliste[j])
                    {
                        BerichtIDs.Add(Interessen[i]);
                        BerichtIDs.Add((int)IDliste[j-1]);
                    }
                }
            }
            //nun sind in BerichtsID die benötigten Berichte mit ihrerer Interessensgruppe wie folgt gespeichert:
            //0 = InteresseID 1= BerichtID

            ArrayList Berichtliste = oDatabase.ExceuteCommand("SELECT * FROM bericht");
            //5Elemente
            //0 = ID    1=Titel     2=Quelle    3=Abrufzeit     4=Text

            //Listbox Grafisch herrichten
            ListBox list = oListe;

            //Um Interessen darzustellen werden die Namen gebraucht
            ArrayList Interessennamen = oDatabase.ExceuteCommand("SELECT name FROM interesse");


            //benötigt um zu erkennen wann sich eine Interesse geändert hat
            int tempInteressenID = 1;
            bool Überschriftgesetzt = false;

            //Schleife Interessen
            for (int j = 1; j < BerichtIDs.Count; j += 2)
            {
                // Für die Übersichtlichkeit wird hier die Interessensgruppe in der ListBox dargestellt
                if (tempInteressenID != (int)BerichtIDs[j - 1] || !Überschriftgesetzt)
                {
                    int index = (int)BerichtIDs[j - 1];
                    ListItem Interessensnamen = new ListItem(Interessennamen[index - 1].ToString());
                    Interessensnamen.Attributes.CssStyle.Add("font-size", "200%");
                    list.Items.Add(Interessensnamen);
                    tempInteressenID = (int)BerichtIDs[j - 1];
                    Überschriftgesetzt = true;
                }
                //Schleife Berichte
                for (int i = 0; i < Berichtliste.Count; i += 5)
                {
                    //Abfrage wenn BerichtID übereinstimmt
                    if ((int)Berichtliste[i] == (int)BerichtIDs[j])
                    {

                        //Titel darstellen
                        ListItem Titelitem = new ListItem(Berichtliste[i + 1].ToString());
                        Titelitem.Attributes.CssStyle.Add("font-weight", "bold");
                        list.Items.Add(Titelitem);

                        //Bericht darstellen
                        string currBericht = Berichtliste[i + 4].ToString();
                        int Zeilenlänge = 150;
                        int Zeilen = currBericht.Length / Zeilenlänge;

                        ListItem Textitem = new ListItem(currBericht);
                        Textitem.Attributes.Add("width", "200px");
                        list.Items.Add(Textitem);

                        list.Items.Add("");
                    }
                }
            }
            
        


    }

    protected void Clickevent(object sender, EventArgs e)
    {
        int Index = oListe.SelectedIndex;


    }

}