﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Einstellungen.aspx.cs" Inherits="Einstellungen" EnableViewState="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            
            <asp:Panel ID="Panel1" runat="server" BackColor="Gray" BorderColor="Gray">
                <br />

                <br />
            </asp:Panel>

        </div>
        
        <br />
        <br />
        
        <div>

            Organisation
            <asp:DropDownList ID="Organisation" runat="server" EnableTheming="True" Height="19px" ToolTip="Bitte Organisation auswählen" Width="170px" OnSelectedIndexChanged="Organisation_SelectedIndexChanged" style="position: relative; left: 400px;"> 
            </asp:DropDownList>

        </div>
        
        <br />
        <br />
        <br />
            <div style="position: relative; left: 480px;">
                <asp:PlaceHolder ID="PlaceHolder1" runat="server" EnableViewState="true">
                    <asp:CheckBoxList ID="Interessen" runat="server"></asp:CheckBoxList>
                </asp:PlaceHolder>    
            </div>
        
        <br />

            <div style="position: relative; left: 480px;">
                <asp:Button ID="Button_Save" runat="server" Text="Organisation hinzufügen" OnClick="Button_Save_Click" />
            </div>
        
        <br />

        Ausgewählte Organisationen:<br />

        <br />
            <div style="position: relative; left: 50px;">
                <asp:PlaceHolder ID="PlaceHolder2" runat="server" EnableViewState="true">
                <asp:TreeView ID="OrgTree" runat="server" Visible="false" OnDataBinding="OrgTree_DataBinding"></asp:TreeView>
                </asp:PlaceHolder>
            </div>

        <br />

            <div style="position: relative; left: 480px;">

                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Letztes Entfernen" />

            </div>


        <br />
        <br />
        <br />
                <asp:Button ID="Button2" runat="server" Text="Einstellungen speichern" Enabled="false" OnClick="Button2_Click"/>



    </form>
</body>
</html>
