﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;
using System.IO;
using System.Net.Mail;
using System.Net;

public partial class FormPassAendern : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    public string salt = "bgfe7384yeuhf49873y";
    public Boolean found = false;
    protected void AendernButton_Click(object sender, EventArgs e)
    {
        Database oDatabase = new Database("root", "", "rss");

        // Prüfen ob die eingegebene Email Adresse in der Datenbank schon vorhanden ist
        ArrayList selAr = oDatabase.GetColumnValues("user", "Email");

        for (int i = 0; i < selAr.Count; i++)
        {
            if (selAr[i].ToString().Equals(EmailPassAendern.Text))
            {
                found = true;
                //Versclüsselung den eingegebenen Passwort 
                byte[] hash;
                using (var sha1 = new SHA1CryptoServiceProvider())
                {
                    hash = sha1.ComputeHash(sha1.ComputeHash(Encoding.Unicode.GetBytes(neuPassvVergess.Text + salt)));
                }
                var sb = new StringBuilder();
                foreach (byte b in hash) sb.AppendFormat("{0:x2}", b);

                String passwordHolen = sb.ToString().Replace(" ", "");

                //Password in der Datenbank für die entsprechende Email Adresse ändern
                ArrayList insAr = oDatabase.ExceuteCommand("update user set Passwort ='" + sb.ToString() + "' where Email = '"+EmailPassAendern.Text+"'");

                //Response.Write("Sie haben den Passwort geändert");

                Response.Redirect("FormLogin.aspx");

            }
            else
            {
                found = false;
               // Response.Write("Falsche Email Adresse");
            }
        }
        if (found == false)
        {
            Response.Write("Email Adresse ist falsch");
        }
    }
}