﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data;
using MySql.Data.MySqlClient;


public partial class Einstellungen : System.Web.UI.Page
{   

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Database DB = new Database("root", "", "rss");
            if (Request.QueryString.HasKeys())
            {
                string Email = Request.QueryString.Get("Email").ToString().Replace("%40","@");
                //überprüfen ob Email existiert
                ArrayList Test = DB.ExceuteCommand("SELECT ID FROM user WHERE Email='" + Email + "'");
                if(Test.Count == 0)
                {
                    
                }
            }
            else
            {
                Response.Redirect("404.htm");
            }


            
            ArrayList Results = DB.ExceuteCommand("SELECT Name FROM organisation");


            System.Collections.ArrayList Organisationen = new ArrayList();
            Organisationen.Add("Organisation wählen");
            Organisationen.AddRange(Results);
            Organisation.DataSource = Organisationen;
            Organisation.DataBind();
            Organisation.AutoPostBack = true;
        }
        /*if(IsPostBack)
        {
            Organisation_SelectedIndexChanged(Organisation, e);
        }*/
    }

    protected void Organisation_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!(Organisation.SelectedValue == "Organisation wählen"))
        {
            Database DB = new Database("root", "", "rss");
            ArrayList InteressenListe = new ArrayList();
            string Org = Organisation.SelectedItem.ToString();
            ArrayList ID = DB.ExceuteCommand("SELECT ID FROM organisation WHERE Name = '" + Org + "'");
            ArrayList IntIDs = DB.ExceuteCommand("SELECT InteressenID FROM organisationsinteresse WHERE OrganisationsID = " + ID[0] + "");
            foreach (int interesse in IntIDs)
            {
                InteressenListe.AddRange(DB.ExceuteCommand("SELECT Name FROM interesse WHERE ID = " + interesse + ""));
            }
            PlaceHolder1.Controls.Clear();
            Interessen.DataSource = InteressenListe;
            Interessen.DataBind();
            foreach (ListItem Item in Interessen.Items)
            {
                Item.Selected = true;
            }

            PlaceHolder1.Controls.Add(Interessen);
        }
        else
        {
            Interessen.Items.Clear();
        }

    }

    protected void Button_Save_Click(object sender, EventArgs e)
    {
        if (!(Organisation.SelectedValue == "Organisation wählen"))
        { 
            TreeNode Org = new TreeNode(Organisation.SelectedValue);
            Org.SelectAction = TreeNodeSelectAction.Expand;
            OrgTree.Visible = true;
            if (OrgTree.FindNode(Org.Text) == null)
            {
                OrgTree.Nodes.Add(Org);
                OrgTree.DataBind();

                foreach (ListItem Interesse in Interessen.Items)
                {
                    if(Interesse.Selected == true)
                    {
                        int i = OrgTree.Nodes.Count - 1;
                        TreeNode ChildNode = new TreeNode(Interesse.ToString());
                        ChildNode.SelectAction = TreeNodeSelectAction.None;
                        OrgTree.Nodes[i].ChildNodes.Add(ChildNode);
                    }

                }
            }

        }

    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        int i = OrgTree.Nodes.Count - 1;
        if(i >= 0 )
        {
            OrgTree.Nodes.RemoveAt(i);
        }
        OrgTree.DataBind();

        
    }

    protected void OrgTree_DataBinding(object sender, EventArgs e)
    {
        if(OrgTree.Nodes.Count > 0)
        {
            Button2.Enabled = true;
        }
        else
        {
            Button2.Enabled = false;
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        string Email = "";
        if (Request.QueryString.HasKeys())
        {
           Email = Request.QueryString.Get("Email").ToString().Replace("%40", "@");
        }
        Database DB = new Database("root", "", "rss");
        foreach (TreeNode Node in OrgTree.Nodes)
        {
            foreach (TreeNode Childnode in Node.ChildNodes)
            {
                ArrayList IntID = DB.ExceuteCommand("SELECT ID FROM interesse WHERE Name = '" + Childnode.Text + "'");
                ArrayList UserID = DB.ExceuteCommand("SELECT ID FROM user WHERE Email = '" + Email + "'");
                DB.ExceuteCommand("INSERT INTO userinteresse (UserID, InteressenID) VALUES (" + UserID[0] + "," + IntID[0] + ") ");
            }
        }
        Response.Redirect("~/Main.aspx?Email="+Email);
    }
}