﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FormLogin.aspx.cs" Inherits="FormLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            font-size: large;
            text-align: center;
        }
        .auto-style2 {
            width: 100%;
        }
        .auto-style3 {
            width: 241px;
        }
        .auto-style4 {
            width: 241px;
            text-align: right;
        }
    </style>
    <script src="jquery-2.2.2.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="auto-style1">
    
        <strong style="font-size: x-large">Login</strong></div>
        <table class="auto-style2">
            <tr>
                <td class="auto-style4">Email:</td>
                <td>
                    <asp:TextBox ID="EmailLogin" runat="server" Width="180px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="EmailLogin" ErrorMessage="Bitte Email eingeben" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">Passwort:</td>
                <td>
                    <asp:TextBox ID="PasswortLogin" runat="server" TextMode="Password" Width="180px" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="PasswortLogin" ErrorMessage="Bitte Passwort eingeben" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td>
                    <asp:Button ID="ButtonAnmelden" runat="server" Text="Anmelden" OnClick="ButtonAnmelden_Click" />
                </td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td>
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Login/FormRegistrieren.aspx">Hier können Sie sich registrieren</asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td>
                    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Login/FormPassAendern.aspx">Passwort vergessen</asp:HyperLink>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
