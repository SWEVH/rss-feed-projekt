﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FormPassAendern.aspx.cs" Inherits="FormPassAendern" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 222px;
        }
        .auto-style3 {
            width: 222px;
            text-align: right;
        }
        .auto-style4 {
            width: 222px;
            text-align: right;
            height: 49px;
        }
        .auto-style5 {
            height: 49px;
        }
        .auto-style6 {
            width: 205px;
        }
        .auto-style7 {
            height: 49px;
            width: 205px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="Label1" runat="server" Text="Neues Passwort eingeben" style="font-weight: 700; font-size: x-large; text-align: center"></asp:Label>
    
    &nbsp;</div>
        <table class="auto-style1">
            <tr>
                <td class="auto-style3">
                    <asp:Label ID="Label4" runat="server" Text="Email:"></asp:Label>
                </td>
                <td class="auto-style6">
                    <asp:TextBox ID="EmailPassAendern" runat="server" Height="25px" Width="180px"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="EmailPassAendern" ErrorMessage="Email ist erforderlich" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">
                    <asp:Label ID="Label2" runat="server" Text="Neues Passwort:"></asp:Label>
                </td>
                <td class="auto-style6">
                    <asp:TextBox ID="neuPassvVergess" runat="server" Height="25px" TextMode="Password" Width="180px"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="neuPassvVergess" ErrorMessage="Passwort ist erforderlich" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <asp:Label ID="Label3" runat="server" Text="Neues Passwort wiederholen:"></asp:Label>
                </td>
                <td class="auto-style7">
                    <asp:TextBox ID="neuPassVergW" runat="server" Height="25px" TextMode="Password" Width="180px"></asp:TextBox>
                </td>
                <td class="auto-style5">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="neuPassVergW" ErrorMessage="Passwort wiederholen ist erforderlich" ForeColor="Red"></asp:RequiredFieldValidator>
                    <br />
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="neuPassvVergess" ControlToValidate="neuPassVergW" ErrorMessage="Beide Passwörter müssen gleich sein" ForeColor="Red"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style6">
                    <asp:Button ID="AendernButton" runat="server" OnClick="AendernButton_Click" Text="Ändern" />
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
</body>
</html>
