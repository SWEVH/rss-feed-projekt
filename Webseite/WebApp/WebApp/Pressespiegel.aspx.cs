﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class Pressespiegel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Email"] == null)
        {
            Response.Redirect("index.aspx");
        }
        bool isAdmin = (bool)Session["IsAdmin"];
        if (isAdmin)
        {
            //<li><a href="index.aspx">Abmelden</a></li>
            HtmlGenericControl oAdminMenue = new HtmlGenericControl("li");
            oAdminMenue.InnerHtml = "<a href=\"AdminPage.aspx\">Administration</a>";
            FindControl("MenuePlaceholder").Controls.Add(oAdminMenue);
        }
        HtmlGenericControl div_general = new HtmlGenericControl("div");
            div_general.Attributes.Add("class", "text-left well well-sm");
            List<Button> ButtonList = new List<Button>();
            for (int i = 0; i < 3; i++)
            {
                HtmlGenericControl div_Org = new HtmlGenericControl("div");
                HtmlGenericControl div_Berichte = new HtmlGenericControl("div");
                div_Org.Attributes.Add("runat", "server");
                div_Org.ID = "Div_Org" + i.ToString();
                div_Berichte.ID = "Div_Berichte" + i.ToString();


                div_Berichte.Attributes.Add("style", "border-color:aqua");
                div_Berichte.Attributes.Add("style", "border-width:thin");
                div_Berichte.Attributes.Add("style", "border-style:groove");


                Button Btn = new Button();
                Btn.Attributes.Add("class", "btn btn-lg");
                Btn.Text = "Aufklappen";
                Btn.ID = "Button" + i;
                HtmlGenericControl h1 = new HtmlGenericControl("h2");
                h1.InnerHtml = "Titel";
                h1.Controls.Add(Btn);

                ButtonList.Add(Btn);

                div_Org.Controls.Add(h1);
                div_Org.Controls.Add(div_Berichte);
                div_Berichte.Visible = false;


                div_general.Controls.Add(div_Org);
                for (int j = 0; j < 5; j++)
                {
                    HtmlGenericControl div = new HtmlGenericControl("div");
                    div.Attributes.Add("class", "well");
                    HtmlGenericControl head = new HtmlGenericControl("h2");
                    head.InnerText = "Schlagzeile";
                    div.Controls.Add(head);
                    HtmlGenericControl p = new HtmlGenericControl("p");
                    p.InnerText = "Artikel";
                    div.Controls.Add(p);
                    div_Berichte.Controls.Add(div);
                }
                Btn.Click += new EventHandler(this.ChangeVisible);
            }

        FindControl("ControlContainer").Controls.Add(div_general);
        //ControlContainer.Controls.Add(div_general);

        //create(ButtonList);
    }

    public void ChangeVisible(object sender, EventArgs e)
    {
        if ((sender as Button).Parent.Parent.Controls[1].Visible)
        {
            (sender as Button).Parent.Parent.Controls[1].Visible = false;
            //(sender as Button).Parent.Parent.Controls[1].ID
            (sender as Button).Text = "Aufklappen";
        }
        else
        {
            (sender as Button).Parent.Parent.Controls[1].Visible = true;
            (sender as Button).Text = "Zuklappen";
        }
        
    }





    /*public void showOrganisation(string[] Organisationen, string[][] Berichte)
    {
        for (int j = 0; j < Organisationen.Length; j++)
        {
            string Titel = Organisationen[j];
            string Inhalt = "";
            for (int i = 0; i < Berichte.Length; i++)
            {
                string Schlagzeile = Berichte[i][0];
                string Artikeltext = Berichte[i][1];
                Inhalt += "<div class=\"well\"><h2>" + Schlagzeile + "</h2><p>" + Artikeltext + "</p></div>";
            }
            Org_literal.Text += "<div class=\"text-left well well-sm\"> <h2>" + Titel + "</h2>" + Inhalt + "</div>";
        }

    }*/

}