﻿<%@ Assembly Name="DatabaseLibrary" %>
<%@ Assembly Name="ViewLibrary" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AdminPage.aspx.cs" Inherits="WebApp.AdminPage" EnableViewState="true" MaintainScrollPositionOnPostback="true" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css" rel="stylesheet" type="text/css" />
    
    <!-- Css Files -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="css/homePopup.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="css/pe-icon-7-stroke.css" rel="stylesheet" type="text/css" />
    <link href="css/animate.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,300" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <!-- Bootstrap -->
        <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="js/buttonsRegLog.js"></script>
    <script type="text/javascript" src="js/buttonsValidierung.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <!--script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></!--script> -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css" rel="stylesheet" type="text/css">
    <title>Administration</title>
        <style>
        .wrapper {
            width: 100%;
            overflow: hidden;
        }
        .left {
            width: 50%;
            float: left;
        }
        .right {
            width: 50%;
            float: right;
        }
        .checkboxlist {
            max-height: 100px;
            height: auto;
            height: 100px;
            overflow: scroll;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
                <!-- header section -->
            <div id="topping">
            </div>
            <section id="top">
                <header>
                    <div class="container">
                        <!-- logo -->
                        <div id="logo">
                            <a href="#topping">Pressespiegel</a>
                        </div>
                        <!-- menu -->
                        <nav class="navmenu">
                            <ul>
                                <li class="scrollable"><a href="Profil.aspx#topping">Startseite</a></li>
                                <li class="scrollable"><a href="#ueberUns">Über uns</a></li>
                                <li class="scrollable"><a href="View.aspx">Pressespiegel</a></li>
                                <li class="dropdown"><a href="#profileDropDown" id="profileDropDown" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" runat="server">Profil &nbsp;<i class="fa fa-caret-down"></i></a>
                                    <ul class="dropdown-menu" role="menu" id="profileMenueDropDown" style="display: none">
                                        <div class="row">
                                            <li><a href="Profil.aspx">Profil Einstellungen</a></li>
                                            <li><a href="Einstellungen.aspx">Pressespiegel Einstellungen</a></li>
                                            <li class="divider"></li>
                                            <li><a href="index.aspx">Abmelden</a></li>
                                            <asp:PlaceHolder ID="MenuePlaceholder" runat="server">
                                                
                                            </asp:PlaceHolder>
                                        </div>
                                    </ul>
                                </li>
                            </ul>
                        </nav>

                        <!-- end menu -->
                        <div class="clear"></div>
                    </div>
                </header>
            </section>
            <!-- //end header section -->
            
            <!-- Admin section -->
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <hr/>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div style="float: left;"><h1>Administrierung</h1></div> 
                        <div style="float: right;"><asp:Button CssClass="btn btn-lg btn-primary" ID="Button_NewOrganization" Text="Neue Organisation" OnClick="Button_Add" runat="server"/></div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <hr/>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-right">
                        <asp:PlaceHolder ID="OrganisationPlaceholder" runat="server">
                            
                        </asp:PlaceHolder>  
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">

                            <asp:PlaceHolder ID="LeftPlaceholder" runat="server">
                                
                            </asp:PlaceHolder>

                            <asp:PlaceHolder ID="RightPlaceholder" runat="server">
                                
                            </asp:PlaceHolder>

                </div>
            </div>
            <div class="container">
                                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h4>
                            <asp:PlaceHolder ID="EmailPlaceholder" runat="server">
                    
                            </asp:PlaceHolder>                 
                        </h4>
                        <div class="row text-right">
                            <asp:PlaceHolder ID="SaveButton" runat="server">
                                
                            </asp:PlaceHolder>
                            <asp:Button ID="Button_Save" Text="Speichern" CssClass="btn btn-primary btn-lg" OnClick="ButtonSave" runat="server"/>
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
