﻿$(function () {

    $("#buttonLogin").click(function (e) {
        var isValid = true;
        $("#benutzernameLog,#passwortLog").each(function () {
            if ($.trim($(this).val()) === "") {
                isValid = false;
                $(this).css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
            }
            else {
                $(this).css({
                    "border": "",
                    "background": ""
                });
            }
        });
        if (isValid === false)
            e.preventDefault();

    });
    $("#buttonReg").click(function (e) {
        var isValid = true;
        $("#benutzernameReg, #EmailReg, #passwortReg, #passwortW").each(function () {
            if ($.trim($(this).val()) === "") {
                isValid = false;
                $(this).css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
            }
            else {
                $(this).css({
                    "border": "",
                    "background": ""
                });
            }
        });
        if (isValid === false)
            e.preventDefault();

    });

    $("#buttonPasswortVergessen").click(function (e) {
        var isValid = true;
        $("#EmailPassVergessen").each(function () {
            if ($.trim($(this).val()) === "") {
                isValid = false;
                $(this).css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
            }
            else {
                $(this).css({
                    "border": "",
                    "background": ""
                });
            }
        });
        if (isValid === false)
            e.preventDefault();

    });

    $("#buttonPassAnfordern").click(function (e) {
        var isValid = true;
        $("#PasswortAnfordern, #PasswortWiederholenAnfordern").each(function () {
            if ($.trim($(this).val()) === "") {
                isValid = false;
                $(this).css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
            }
            else {
                $(this).css({
                    "border": "",
                    "background": ""
                });
            }
        });
        if (isValid === false)
            e.preventDefault();

    });
});
