﻿$(function () {

    $("#registrierenLog")
        .click(function (e) {
            e.preventDefault();

            $("#anmelden").hide();
            $("#registrieren").show();

            $("#benutzernameLog").val("");
            $("#passwortLog").val("");
        });

    $("#passwortVergessenLog")
        .click(function (e) {
            e.preventDefault();

            $("#anmelden").hide();
            $("#passwortVergessen").show();

            $("#benutzernameLog").val("");
            $("#passwortLog").val("");
        });

    $("#anmeldenPassVergessen")
       .click(function (e) {
           e.preventDefault();

           $("#passwortVergessen").hide();
           $("#anmelden").show();

           $("#EmailPassVergessen").val("");
        });

    $("#anmeldenReg")
        .click(function (e) {
            e.preventDefault();

            $("#registrieren").hide();
            $("#anmelden").show();

            $("#benutzernameReg").val("");
            $("#EmailReg").val("");
            $("#passwortReg").val("");
            $("#passwortW").val("");
        });

    $("#profileDropDown")
        .click(function (e) {

        $("#profileMenueDropDown").toggle();
        });

    $("#benutzernameAendernProfile")
       .click(function (e) {
           e.preventDefault();

           $("#benutzernameAendern").toggle();
       });

    $("#emailAendernProfile")
      .click(function (e) {
          e.preventDefault();

          $("#emailAendern").toggle();
      });

    $("#passwortAendernProfile")
      .click(function (e) {
          e.preventDefault();

          $("#passwortAendern").toggle();
      });

});
