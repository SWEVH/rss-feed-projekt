﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace WebApp
{
    public partial class View : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int nID = 0;
            
            if (!string.IsNullOrEmpty(Request.QueryString["Email"]))
            {
                Session["Email"] = Request.QueryString["Email"];
            }
            if (Session["Email"] == null)
            {
                Response.Redirect("index.aspx");
            }
            bool isAdmin = false;
            if (Session["IsAdmin"] != null)
            {
                isAdmin = (bool)Session["IsAdmin"];
            }
            if (isAdmin)
            {
                HtmlGenericControl oAdminMenue = new HtmlGenericControl("li");
                oAdminMenue.InnerHtml = "<a href=\"AdminPage.aspx\">Administration</a>";
                FindControl("MenuePlaceholder").Controls.Add(oAdminMenue);
            }
            //Extrahieren der relevanten Artikel aus DB
            PressElement[,] Data = getData();

            string sPathToImages = @"C:/Projekt/images/Sources/";

            //Darstellen der relevanten Artikel aus Data
            for (int i = 0; i < Data.GetLength(0); i++)
            {
                //Verhindern des Absturzen wenn keine Ergebnisse erhalten wurden
                if (Data.GetLength(1) != 0)
                {
                    if (Data[i, 0] != null)
                    {
                        

                        HtmlGenericControl Gruppencontrol = new HtmlGenericControl("div");
                        Gruppencontrol.Attributes.Add("class", "container");
                        Gruppencontrol.Attributes.Add("id", "Group" + Data[i, 0].Interesse);
                        HtmlGenericControl Heading = new HtmlGenericControl("h2");
                        Heading.InnerText = Data[i, 0].Interesse;
                        Gruppencontrol.Controls.Add(Heading);

                        for (int j = 0; j < Data.GetLength(1); j++)
                        {
                            if (Data[i, j] != null)
                            {
                                //Feststellen der Quelle
                                string sSource = Data[i, j].Quelle;
                                string sIconNaME = "";
                                bool bAdd = false;
                                for (int z = 0; z < sSource.Length; z++)
                                {
                                    if (sSource[z] == '.' && !bAdd)
                                    {
                                        bAdd = true;
                                    }
                                    else if (sSource[z] == '.')
                                    {
                                        break;
                                    }
                                    else if (bAdd)
                                    {
                                        sIconNaME += sSource[z];
                                    }
                                }
				if(Data[i,j].Quelle.Contains("heise"))
				{
					sIconNaME = "heise";
				}
                                sIconNaME = sIconNaME.Replace("-stuttgart", string.Empty);
                                Image oImage = new Image();
                                string sWorkingDirectory = Server.MapPath("~");
                                if (System.IO.File.Exists(sWorkingDirectory + @"images\Pressespiegel Quellen icons\128x128\" + sIconNaME + ".png"))
                                {
                                    oImage.ImageUrl = @"images\Pressespiegel Quellen icons\128x128\" + sIconNaME +".png";
                                }
                                else
                                {
                                    oImage.ImageUrl = @"images\Pressespiegel Quellen icons\128x128\logo.png";
                                }
                                oImage.ID = "Image_" + nID;
                                oImage.Width = 42;
                                oImage.Height = 42;
                                nID++;
                                
                                HtmlGenericControl Subtitle = new HtmlGenericControl("h3");
                                Subtitle.InnerHtml = "&nbsp;" + Data[i, j].Heading;
                                Subtitle.Attributes.Add("style", "vertical-align: bottom;");

                                HtmlGenericControl Header = new HtmlGenericControl("div");
                                Header.Attributes.Add("class", "tableSubtitle");
                                HtmlGenericControl ImageCell = new HtmlGenericControl("div");
                                ImageCell.Attributes.Add("class","cellImage");
                                ImageCell.Controls.Add(oImage);
                                HtmlGenericControl HeaderCell = new HtmlGenericControl("div");
                                HeaderCell.Attributes.Add("class","cellHeader");
                                HeaderCell.Controls.Add(Subtitle);
                                Header.Controls.Add(ImageCell);
                                Header.Controls.Add(HeaderCell);

                                Gruppencontrol.Controls.Add(Header);
                                Gruppencontrol.Controls.Add(new LiteralControl("<br>"));

                                //Text des Berichts einfügen
                                HtmlGenericControl Text = new HtmlGenericControl("p");
                                Text.InnerText = Data[i, j].Text;
                                Gruppencontrol.Controls.Add(Text);

                                HtmlAnchor titleAnchor = new HtmlAnchor();
                                titleAnchor.HRef = Data[i, j].Quelle;
				titleAnchor.InnerText = Data[i,j].Quelle;
                                //titleAnchor.InnerText = "Hier geht es zum kompletten Artikel";
                                Gruppencontrol.Controls.Add(titleAnchor);
                                Gruppencontrol.Controls.Add(new LiteralControl("<br>"));
                                Gruppencontrol.Controls.Add(new HtmlGenericControl("br"));
                            }
                        }
                        Page.Controls.Add(Gruppencontrol);
                        Page.Controls.Add(new LiteralControl("<br />"));
                    }
                }
            }
        }


        private PressElement[,] getData()
        {
            try
            {
                string Email = "";
                //if (Request.QueryString.HasKeys())
                //{
                //    Email = Request.QueryString.Get("Email");
                //}
                Email = Session["Email"].ToString();
                Database oDatabase = new Database("root", "", "rss");

                //benutzer abfragen
                ArrayList User = oDatabase.ExceuteCommand("SELECT ID FROM user WHERE Email = '" + Email + "'");
                int idUser = (int)User[0]; //Während der Entwicklung User hier direkt festlegen

                ArrayList IDUserinteresse = oDatabase.ExceuteCommand("SELECT * FROM Userinteresse WHERE UserID = " + idUser);
                //0 = UserID     1 = InteresseID    

                //Um Interessen darzustellen werden die Namen gebraucht
                ArrayList Interessennamen = oDatabase.ExceuteCommand("SELECT name FROM interesse");
                //ein Array mit dem InteressenIndex (passend) und den namen dazu aufgelistet

                List<int> Interessen = new List<int>(); //Liste aller Interessen
                for (int i = 1; i < IDUserinteresse.Count; i += 2)
                {
                    //Interesse ist nun mit dem richtigen Index 
                    Interessen.Add((int)IDUserinteresse[i]);
                }

                ArrayList IDliste = oDatabase.ExceuteCommand("SELECT * FROM berichtinteresse");
                //0 = BerichtID     1 = InteresseID

                ArrayList BerichtIDs = new ArrayList();
                //Hier wird anhand der Userinteressen BerichtID in eine Liste geladen
                for (int i = 0; i < Interessen.Count; i++)
                {
                    for (int j = 0; j < IDliste.Count; j += 2)
                    {
                        if (Interessen[i] == (int)IDliste[j + 1])
                        {
                            BerichtIDs.Add((int)IDliste[j]);
                            BerichtIDs.Add((int)IDliste[j + 1]);
                        }
                    }
                }
                //nun sind in BerichtsID die benötigten Berichte mit ihrerer Interessensgruppe wie folgt gespeichert:
                //0 = BerichtID   1= InteresseID     --> nur aktueller User!

                ArrayList Elements = new ArrayList();
                ArrayList Berichtliste = oDatabase.ExceuteCommand("SELECT * FROM bericht");
                //5Elemente
                //0 = ID    1=Titel     2=Quelle    3=Abrufzeit     4=Text

              
                //PressElements erstellen und befüllen
                for (int q = 0; q < Berichtliste.Count; q += 5)
                {
                    DateTime dt = (DateTime)Berichtliste[q + 3];
                    
                    if (!((DateTime.Today.Ticks - dt.Ticks) > 864000000000))
                    {
                        for (int m = 0; m < BerichtIDs.Count; m += 2)
                        {
                            if (((int)Berichtliste[q]) == ((int)BerichtIDs[m]))
                            {
                                PressElement Element = new PressElement();
                                Element.BerichtID = (int)Berichtliste[q];
                                Element.Heading = Berichtliste[q + 1].ToString();
                                Element.Quelle = Berichtliste[q + 2].ToString();
                                Element.Datum = Berichtliste[q + 3].ToString();
                                Element.Text = Berichtliste[q + 4].ToString();
                                int ElInteresse = (int)BerichtIDs[m + 1] - 1;
                                Element.Interesse = Interessennamen[ElInteresse].ToString();
                                Elements.Add(Element);
                            }
                        }
                    }

                }
                //nun haben wir in einem Array alle Berichte 

                //nun werden die Elemente in eine matrix Geladen
                //[ Interesse  ,  Elemente ]
                PressElement[,] Data = new PressElement[Interessennamen.Count, Elements.Count];
                //erstellen der Verwaltung
                for (int m = 0; m < Interessennamen.Count; m++)
                {
                    int k = 0;
                    foreach (PressElement Element in Elements)
                    {
                        if (Element.Interesse == Interessennamen[m].ToString())
                        {
                            Data[m, k] = Element;
                            k++;
                        }
                    }
                }
                return Data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
    }
}




#region kruscht



//DateTime ikddt = new DateTime(2016, 1, 1);
//DateTime hfdr = new DateTime(2016, 1, 2);

//long dayone = ikddt.Ticks;
//long daytwo = hfdr.Ticks;

//long oneday = daytwo - dayone;
#endregion

