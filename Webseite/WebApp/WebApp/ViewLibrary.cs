﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp
{
    public class ViewLibrary
    {
        Database oDatabase = new Database("root", "", "rss");

 #region getter
        /// <summary>
        /// Methode gibt anhand der Email die Interessen des Users wieder
        /// </summary>
        /// <param name="Email">Email, womit der User angemeldet ist</param>
        /// <returns>ArrayList an Userinteressen</returns>
        public ArrayList getUserinterests(string Email)
        {
            try
            {
                ArrayList User = oDatabase.ExceuteCommand("SELECT ID FROM user WHERE Email = '" + Email + "'");
                //!!!!!!!!!!!!!!!!!
                int idUser = (int)User[0]; //Während der Entwicklung User hier direkt festlegen

                ArrayList IDUserinteresse = oDatabase.ExceuteCommand("SELECT * FROM Userinteresse WHERE UserID = " + idUser);
                //0 = UserID     1 = InteresseID   

                //Um Interessen darzustellen werden die Namen gebraucht
                ArrayList Interessennamen = oDatabase.ExceuteCommand("SELECT name FROM interesse");
                //ein Array mit dem InteressenIndex (passend) und den namen dazu aufgelistet
                ArrayList Userinterests = new ArrayList(Interessennamen.Count);

                List<int> Interessen = new List<int>(); //Liste aller Interessen
                for (int i = 1; i < IDUserinteresse.Count; i += 2)
                {
                    int ID = (int)IDUserinteresse[i];
                    //Interesse ist nun mit dem richtigen Index 
                    Interessen.Add(ID);
                    Userinterests.Add(Interessennamen[ID - 1]);
                }

                return Userinterests;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Methode gibt anhand der Email das Userpasswort aus
        /// </summary>
        /// <param name="Email">Useremail</param>
        /// <returns>UserPasswort</returns>
        public string getUserPasswort(string Email)
        {
            try
            {
                string Passwort;
                ArrayList User = oDatabase.ExceuteCommand("SELECT ID FROM user WHERE Email = '" + Email + "'");
                int idUser = (int)User[0];
                ArrayList LPasswort = oDatabase.ExceuteCommand("SELECT passwort FROM User WHERE ID = " + idUser);
                Passwort = LPasswort[0].ToString();
                return Passwort;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
        }


        /// <summary>        
        /// Methode gibt anhand der Email den Username aus
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        public string getUsername(string Email)
        {
            string Username;
            ArrayList User = oDatabase.ExceuteCommand("SELECT ID FROM user WHERE Email = '" + Email + "'");
           int idUser = (int)User[0]; 

            ArrayList ALUsername = oDatabase.ExceuteCommand("SELECT username FROM User WHERE ID = " + idUser);
            Username = ALUsername[0].ToString();



            return Username;
        }

        #endregion


 #region setter
        /// <summary>        
        /// Methode setzt anhand der Email den Username
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        public void setUsername(string Username, string Email)
        {
            ArrayList User = oDatabase.ExceuteCommand("SELECT ID FROM user WHERE Email = '" + Email + "'");
            //!!!!!!!!!!!!!!!!!
            int idUser =  (int)User[0];
            oDatabase.ExceuteCommand("UPDATE `rss`.`user` SET `Username` = '"+ Username + "' WHERE `user`.`ID` = "+idUser);
            // UPDATE user SET Username = '" + Username + "' WHERE Email = " + Email;

        }


        /// <summary>        
        /// Methode setzt anhand der alten Email die neue Email
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        public void setEmail(string Emailneu, string Emailalt)
        {
            ArrayList User = oDatabase.ExceuteCommand("SELECT ID FROM user WHERE Email = '" + Emailalt + "'");
            //!!!!!!!!!!!!!!!!!
            int idUser = (int)User[0]; 
            oDatabase.ExceuteCommand("UPDATE `rss`.`user` SET `Email` = '" + Emailneu + "' WHERE `user`.`ID` = " + idUser);
            // UPDATE user SET Username = '" + Username + "' WHERE Email = " + Email;

        }


        /// <summary>        
        /// Methode setzt anhand der Email das neue Passwort
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        public void setPasswort(string Passwort, string Email)
        {
            ArrayList User = oDatabase.ExceuteCommand("SELECT ID FROM user WHERE Email = '" + Email + "'");
           int idUser = (int)User[0]; 
            oDatabase.ExceuteCommand("UPDATE `user` SET `Passwort` = '" + Passwort + "' WHERE `ID` = " + idUser);
            
        }






        #endregion







    }
}