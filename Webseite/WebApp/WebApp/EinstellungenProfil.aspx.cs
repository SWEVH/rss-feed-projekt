﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;
using System.IO;
using System.Net.Mail;
using System.Net;

namespace WebApp
{
    public partial class EinstellungenProfil : System.Web.UI.Page
    {
        public string salt = "bgfe7384yeuhf49873y";
        public bool found = false;
        public Database oDatabase = new Database("root", "", "rss");
        public string Email = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Email"] == null)
            {
                Response.Redirect("index.aspx");
            }
            //Email aus Session Variable auslesen
            Email = Session["Email"].ToString();
             
            //Email = "mnjf@dmsjkf.de";//dh diese löschen
            //!!!!!!!!!!!!!!!!Emailabfrage an server anpassen

            //aufruf eigene DLL zugreifen
            ViewLibrary vL = new ViewLibrary();
            //Benutzername  

            TextBoxName.Text = vL.getUsername(Email);
            TextBoxEmail.Text = Email;

            // var NameButton = 


        }


        /// <summary>
        /// Ereignis, welches bei drücken von "Anderungen speichern" des
        /// Usernames eintritt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnChangeUsername_Click(object sender, EventArgs e)
        {
            string Eingabe = Request.Form["TextBoxName"].ToString();
            //aufruf eigene DLL zugreifen
            ViewLibrary vL = new ViewLibrary();
            if (Email.Length > 1)
            {
                vL.setUsername(Eingabe, Email);
            }
            else
            {
                Session["Errormessage"] = "Ihre anfrage konnte leider nicht erfolgen. Bitte versuchen Sie es noch einmal.";
                //TextBoxName.Text = ;
                //getElementById('demo')
            }


        }


        /// <summary>
        /// Ereignis, welches bei drücken von "Anderungen speichern" der
        /// E-Mail-Adresse eintritt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnChangeEMail_Click(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// Ereignis, welches bei drücken von "Anderungen speichern" des
        /// Passwortes eintritt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnChangePassword_Click(object sender, EventArgs e)
        {

        }


    }
}