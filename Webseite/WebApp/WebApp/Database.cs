﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data;
using MySql.Data.Types;
using System.Text;
using System.Dynamic;


/// <summary>
/// Zusammenfassungsbeschreibung für Database
/// </summary>

public class Database
{
    public const string Server = "127.0.0.1";

    public string User { get; set; }

    public string Password { get; set; }

    public string Databasename { get; set; }

    public MySqlConnection Conn { get; set; }



    public Database(string User, string Password, string Databasename)
    {
        this.User = User;
        this.Password = Password;
        this.Databasename = Databasename;
        string ConnectionString = "server=" + Server + ";uid=" + User + ";pwd=" + Password + ";database=" + Databasename + ";";

        this.Conn = new MySqlConnection(ConnectionString);
    }

    public ArrayList ExceuteCommand(string CommandString)
    {
        ArrayList Result=new ArrayList();
        MySqlCommand Command = new MySqlCommand(CommandString, this.Conn);
        try
        {
            this.Conn.Open();
        }
        catch(MySqlException ex)
        {
            Console.WriteLine("Connection to Database failed.");
        }
        MySqlDataReader Reader = Command.ExecuteReader();
        DataTable Table = new DataTable();
        Table.Load(Reader);
        foreach(DataRow row in Table.Rows)
        {
            Result.AddRange(row.ItemArray);
        }
        Reader.Close();
        this.Conn.Close();
        return Result;
    }

    public ExpandoObject CreateObject(string Tablename)
    {
        MySqlCommand Command = new MySqlCommand("SHOW COLUMNS FROM " + Tablename, this.Conn);
        try
        {
            this.Conn.Open();
        }
        catch (MySqlException ex)
        {
            Console.WriteLine("Connection to Database failed.");
        }
        MySqlDataReader Reader = Command.ExecuteReader();
        DataTable Table = new DataTable();
        Table.Load(Reader);
        ArrayList Columnnames = new ArrayList();
        foreach (DataRow row in Table.Rows)
        {
            Columnnames.Add(row["Field"].ToString());
        }
        Reader.Close();
        this.Conn.Close();

        ExpandoObject Object = AssignMembers(Tablename,Columnnames);
        return Object;

    }
    public ArrayList GetColumnValues(string Tablename, string Columnname)
    {
        MySqlCommand Command = new MySqlCommand("SELECT " + Columnname + " FROM " + Tablename, this.Conn);
        try
        {
            this.Conn.Open();
        }
        catch (MySqlException ex)
        {
            Console.WriteLine("Connection to Database failed.");
        }

        MySqlDataReader Reader = Command.ExecuteReader();
        ArrayList result = new ArrayList();
        while(Reader.Read())
        {
            for(int i = 0;i< Reader.FieldCount;i++)
            {
                if(Reader[i] != DBNull.Value)
                {
                    Reader[i].GetType();
                    result.Add(Reader.GetString(i));
                }
                else
                {
                    result.Add("null");
                }
                
            }
        }
        Reader.Close();
        this.Conn.Close();
        return result;
    }

    public ExpandoObject AssignMembers(string Tablename, ArrayList Columnnames)
    {
        dynamic DatabaseObject = new ExpandoObject();
        IDictionary<string, object> Table = DatabaseObject;
        foreach (string Column in Columnnames)
        {
            ArrayList Values = GetColumnValues(Tablename, Column);
            Table.Add(Column.ToString(),Values);
        }
        Table.Add("Tablename", Tablename);
        Table.Add("Columns", Columnnames);
        return DatabaseObject;

    }

    public bool UpdateDatabase(dynamic Object)
    {
        string Tablename = Object.Tablename;
        ExpandoObject CompObject = CreateObject(Tablename);
        
        return true;
            
    }
}