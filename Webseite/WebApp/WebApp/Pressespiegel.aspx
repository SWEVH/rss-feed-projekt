﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Pressespiegel.aspx.cs" Inherits="Pressespiegel" EnableViewState="true" MaintainScrollPositionOnPostback="true"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css" rel="stylesheet" type="text/css">
    <title>Pressespiegel</title>
</head>
<body>
    <form id="form1" runat="server" enableviewstate="true">
        <div class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button><a class="navbar-brand" href="#">
                        <span>pressespiegel.de</span></a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-ex-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#">Home</a></li>
                        <li class="active"><a href="#">Pressespiegel</a></li>
                        <li><a href="#">About us</a></li>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Benutzer &nbsp;<i class="fa fa-caret-down"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Profil</a></li>
                                <li><a href="#">Einstellungen</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Ausloggen</a></li>
                                <asp:PlaceHolder ID="MenuePlaceholder" runat="server">
                                                
                                </asp:PlaceHolder>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <asp:PlaceHolder ID="ControlContainer" runat="server">

                        </asp:PlaceHolder>
                    </div>
                </div>
            </div>
        </div>
        

    </form>
</body>
</html>
