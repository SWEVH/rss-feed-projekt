﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="Startseite" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <meta charset="utf-8">
    <title>Pressespiegel</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Css Files -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/plugins.css" rel="stylesheet" type="text/css">
    <link href="css/homePopup.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <link href="css/pe-icon-7-stroke.css" rel="stylesheet" type="text/css">
    <link href="css/animate.css" rel="stylesheet" type="text/css">
    <!-- Font Awesome -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/buttonsRegLog.js"></script>
    <script type="text/javascript" src="js/buttonsValidierung.js"></script>

</head>
<body>
    <form id="formStartseite" runat="server">
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">
        </div>
    </div>
    <!--//Preloader -->
    <!-- #page -->
    <div id="page">
        <!-- header section -->
        <div id="topping">
        </div>
        <section id="top">
            <header> 
            	<div class="container">                                   
                    <!-- logo -->
                    
                    <div id="logo" >
                        <a href="#topping">Pressespiegel</a>
         			</div>
                    <!-- menu -->
                    <nav class="navmenu">
                        <ul>
                            <li class="scrollable"><a href="#topping">Startseite</a></li>  
                            <li class="scrollable"><a href="#ueberUns">Über uns</a></li>
                            <li class="scrollable"><a href="#pressespiegel">Pressespiegel</a></li>  
                            <li class="scrollable"><a href="#anmeldung">Anmeldung</a></li>
                        </ul>
                    </nav><!-- end menu --> 
                    <div class="clear"></div>
            	</div>
            </header>               
        </section>
        <!-- //end header section -->
        <!-- home content -->
        <section id="home">
        	<!--Slider-->
            <div class="full_slider">
                <div id="fullwidth_slider" class="flexslider">                	
                    <ul class="slides">
                        <li> 
                        	<div class="slideshow-overlay"></div>                                    
                            <img src="images/slider/neuesteNachrichten.jpg" alt="" class="slide_bg" />
                            <div class="full_slider_caption">
                                <div class="container">
                                	<div class="cont">                                   
                                        <div class="title">Neueste Nachrichten</div>
                                        <br>Wirtschaft
                                    </div>
                                </div>                              
                            </div>                        
                        </li>
                        <li>
                        	<div class="slideshow-overlay"></div>
                            <img src="images/slider/2.jpg" alt="" class="slide_bg" />                            	
                            <div class="full_slider_caption">
                                <div class="container">
                                	<div class="cont">
                                        <div class="title">Neueste Nachrichten</div>
                                        <br>Politik
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                        	<div class="slideshow-overlay"></div>
                            <img src="images/slider/digital-news.jpg" alt="" class="slide_bg" />                            	
                            <div class="full_slider_caption">
                                <div class="container">
                                	<div class="cont">
                                        <div class="title">Neueste Nachrichten</div>
                                        <br>Privatleben
                                    </div>
                                </div>
                            </div>
                        </li>                                                       
                    </ul>
                </div>
             </div>          
            <!--//Slider-->            
        </section>
        <!-- //end home content -->
        <!-- anmeldung content-->
        <section class="page_section" id="anmeldung">
            <div class="container block-wrap wow fadeInUp">
            	<div class="row">
                	<div class="col-md-8">
                        <img src="images/online-press.jpg" alt="" />                            	
                    </div>
                    <div id="anmelden" class="col-md-4" runat="server">
                    	<div class="panel panel-default black">
                        	<header class="head_section">
                            	<div class="icon-write">
                                  <i class="pe-5x pe-va pe-7s-diamond"></i> 
                                </div> 
                              <h2>ANMELDUNG</h2>
                              <div class="separator left"></div>
                            </header>
                            <div id="fields">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <label>Benutzername</label>
                                    <asp:TextBox class="inp" id="benutzernameLog" placeholder="Email oder Benutzername" title="Benutzername"  runat="server"></asp:TextBox>
                                </div>
                                <div class="clear"></div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <label>Passwort</label>
                                    <asp:TextBox class="inp" id="passwortLog" TextMode="Password" name="passwortLog" placeholder="Passwort" runat="server" title="Passwort"></asp:TextBox>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <a href="#registrieren" id="registrierenLog" onclick="preventDefault" runat="server"><i class="fa fa-user"></i>Registrierung</a>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <a href="#passwortVergessen" id="passwortVergessenLog" onclick="preventDefault" runat="server"><i class="fa fa-key"></i>Passwort vergessen</a>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <asp:Button class="shortcode_button" ID="buttonLogin" OnClick="buttonLogin_Click" name="buttonLogin" Text="Anmelden" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>           
                    <div id="registrieren" class="col-md-4" style="display: none">
                    	<div class="panel panel-default black">
                        	<header class="head_section">
                            	<div class="icon-write">
                                  <i class="pe-5x pe-va pe-7s-diamond"></i> 
                                </div> 
                              <h2>REGISTRIERUNG</h2>
                              <div class="separator left"></div>
                            </header>
                            <div id="fields1">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <label runat="server">Benutzername</label>
                                    <asp:TextBox class="inp" ID="benutzernameReg" placeholder="Benutzername" runat="server" title="Benutzername"></asp:TextBox>
                                </div>
                                <div class="clear"></div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <label runat="server">Email</label>
                                    <asp:TextBox class="inp" ID="EmailReg" placeholder="Email" title="Email" runat="server"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegEXEmailReg" runat="server" ControlToValidate="EmailReg" ErrorMessage="Email Adresse ist nicht gültig" Display="Dynamic" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>

                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <label runat="server">Passwort</label>
                                    <asp:TextBox class="inp" TextMode="Password" ID="passwortReg" placeholder="Passwort" title="Passwort" runat="server"></asp:TextBox>
                                    <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "passwortReg" ID="ValidatorPasswortLaenge" ValidationExpression = "^[\s\S]{5,16}$" runat="server" ErrorMessage="Das Passwort muss min. 5 und max. 16 Zeichen haben." ForeColor="Red"></asp:RegularExpressionValidator>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <label runat="server">Passwort wiederholen</label>
                                    <asp:TextBox class="inp" TextMode="Password" ID="passwortW" placeholder="Passwort wiederholen" title="Passwort wiederholen" runat="server"></asp:TextBox>
                                    <asp:CompareValidator ID="CompareValidatorPasswortVergleich" runat="server" ControlToCompare="passwortReg" ControlToValidate="passwortW" Display="Dynamic" ErrorMessage="Beide Passwörter müssen gleich sein" ForeColor="Red"></asp:CompareValidator>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <a href="#anmelden" ID="anmeldenReg" onclick="preventDefault" runat="server"><i class="fa fa-user"></i>Anmelden</a>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <asp:Button class="shortcode_button" ID="buttonReg" OnClick="buttonReg_Click" Text="Registrieren"  runat="server" />
                                </div>
                            </div>
                        </div>
                    </div> 
                   <div id="passwortVergessen" class="col-md-4" style="display: none">
                    	<div class="panel panel-default black">
                        	<header class="head_section">
                            	<div class="icon-write">
                                  <i class="pe-5x pe-va pe-7s-diamond"></i> 
                                </div> 
                              <h2>PASSWORT VERGESSEN</h2>
                              <div class="separator left"></div>
                            </header>
                            <div id="fields2">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <label runat="server">Email Adresse</label>
                                    <asp:TextBox class="inp" ID="EmailPassVergessen" placeholder="Email Adresse" title="Email Adresse" runat="server"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegexValidatorEmailPassVergessen" runat="server" ControlToValidate="EmailPassVergessen" ErrorMessage="Email Adresse ist nicht gültig" Display="Dynamic" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <label runat="server">Passwort</label>
                                    <asp:TextBox class="inp" TextMode="Password" ID="PasswortAnfordern" placeholder="Passwort" title="Passwort" runat="server"></asp:TextBox>
                                    <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "PasswortAnfordern" ID="ValidatorPassAnfordern" ValidationExpression = "^[\s\S]{5,16}$" runat="server" ErrorMessage="Das Passwort muss min. 5 und max. 16 Zeichen haben." ForeColor="Red"></asp:RegularExpressionValidator>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <label runat="server">Passwort wiederholen</label>
                                    <asp:TextBox class="inp" TextMode="Password" ID="PasswortWiederholenAnfordern" placeholder="Passwort wiederholen" title="Passwort wiederholen" runat="server"></asp:TextBox>
                                    <asp:CompareValidator ID="CompareValidatorPassAnfordern" runat="server" ControlToCompare="PasswortAnfordern" ControlToValidate="PasswortWiederholenAnfordern" Display="Dynamic" ErrorMessage="Beide Passwörter müssen gleich sein" ForeColor="Red"></asp:CompareValidator>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <a href="#anmelden" ID="anmeldenPassVergessen" onclick="preventDefault" runat="server"><i class="fa fa-user"></i>Anmelden</a>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <asp:Button class="shortcode_button" ID="buttonPasswortVergessen" Text="Absenden" OnClick="buttonPasswortVergessen_Click" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
                <div class="clear"></div>
            </div><!-- end .container -->
        </section>
        <!-- //ende anmeldung content-->
        <!-- Tab Section -->
        <!-- // end tab section -->
        <!-- pressespiegel content-->
        <section class="page_section" id="pressespiegel">                     
            <!-- section content -->   
            <div class="container wow fadeInUp">            				
                <div class="about_block bg_gray mb30">                	              	
                    <div class="col_cont">                    	                     	
                        <div class="wrap_cont">                        	
                            <header class="head_section">                            	                         	
                            	<h2>Ihr persönlicher Pressespiegel</h2>
                              <div class="separator left"></div>                              	
                            </header>
							<p>Damit sie Ihren persönlichen Pressespiegel anschauen können, müssen Sie sich erst anmelden.</p>
                            <asp:HyperLink ID="zumAnmelden" class="shortcode_button" runat="server" NavigateUrl="#anmeldung">Anmelden</asp:HyperLink>                            
                        </div>
                    </div>                     
                    <div class="col_img"></div>	
                    <div class="clear"></div>
                </div>
            </div>                                	
			<!-- //section content --> 
        </section>
        <!-- //ende pressespiegel content-->
  
         <!-- ueber uns content -->
        <section class="page_section" id="ueberUns">
            <div class="container block-wrap wow fadeInUp">
            	<div class="row carousel-box">
                	<div class="col-md-3">
                    	<div class="control-block bg_black block">
                        	<header class="head_section">
                            	<div class="icon-write">
                                  <i class="pe-5x pe-va pe-7s-diamond"></i> 
                                </div> 
                           	  <h2>Unser Projekt</h2>
                              <div class="separator left"></div>
                            </header>
							<p>Mit unserem Pressespiegel bekommen Sie täglich die neuesten Nachrichten.</p>
                            <p>Dieser Pressespiegel ist als Projekt von den Studenten der DHBW Stuttgart / Campus Horb entstanden.</p>
                        	<div class="customNavigation">
                            	<a class="btn-prev"><i class="fa fa-angle-left"></i></a>
                            	<a class="btn-next"><i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                    	<div class="owl-carousel">
                        	<div class="item-service block">                            	                       	 
								<img src="images/Projektmitglieder/Janna Dreher.jpg" alt="">
                            	<div class="cont">
                                    <h3>Janna Dreher</h3>
                                    <h4>Verantwortliche für Tests</h4>
                                    <div class="separator"></div>
                                    <p>Die Verantwortliche für Tests koordiniert die Erstellung eines Testkonzepts und überwacht dessen Umsetzung.</p>                                    
                                    <a href="mailto:i14004@hb.dhbw-stuttgart.de">Kontakt</a>
                                </div>
                            </div>
                            <div class="item-service block">
                            	<img src="images/Projektmitglieder/Felix Geiselhardt.jpg" alt="">
                            	<div class="cont">
                                    <h3>Felix Geiselhart</h3>
                                    <h4>Verantwortlich für Recherche</h4>
                                    <div class="separator"></div>
                                    <p>Der Verantwortliche für Recherche koordiniert die Anforderungsanalyse.</p>
                                    <a href="mailto:i14009@hb.dhbw-stuttgart.de">Kontakt</a>
                                </div>
                            </div>
                            <div class="item-service block">                            
                            	<img src="images/Projektmitglieder/Michal Liska.jpg" alt="">
                            	<div class="cont">
                                    <h3>Michal Liska</h3>
                                    <h4>Technischer Assistent</h4>
                                    <div class="separator"></div>
                                    <p>Der technische Assistent ist für die Verwaltung der Grouppenressourcen verantwortlich.</p>
                                    <a href="mailto:i14016@hb.dhbw-stuttgart.de">Kontakt</a>
                                </div>
                            </div>
                            <div class="item-service block">                            
                            	<img src="images/Projektmitglieder/Danche Nedeska.jpg" alt="">
                            	<div class="cont">
                                    <h3>Danche Nedeska</h3>
                                    <h4>Verantwortliche für Modellierung</h4>                                   
                                    <div class="separator"></div>
                                    <p>Die Verantwortliche für Modellierung koordiniert die Modellierungsphase.</p>
                                    <a href="mailto:i14020@hb.dhbw-stuttgart.de">Kontakt</a>
                                </div>
                            </div>
                            <div class="item-service block">                            
                            	<img src="images/Projektmitglieder/Sebastian Ritt.jpg" alt="">
                            	<div class="cont">
                                    <h3>Sebastian Ritt</h3>
                                    <h4>Projektleiter</h4>                                   
                                    <div class="separator"></div>
                                    <p>Der Projektleiter koordiniert die Arbeit der Gruppe und ist Ansprechpartner für den Kunden.</p>
                                    <a href="mailto:i14021@hb.dhbw-stuttgart.de">Kontakt</a>
                                </div>
                            </div>
                            <div class="item-service block">                            
                            	<img src="images/ticket book.jpg" alt="">
                            	<div class="cont">
                                    <h3>Christopher Schroth</h3>
                                    <h4>Verantwortlicher für Implementierung</h4>                                   
                                    <div class="separator"></div>
                                    <p>Der Verantwortlicher für Implementierung koordiniert den Prozess der Implementierung.</p>
                                    <a href="mailto:i14022@hb.dhbw-stuttgart.de">Kontakt</a>
                                </div>
                            </div>                
                            <div class="item-service block">                            
                            	<img src="images/Projektmitglieder/Angela Werder.jpg" alt="">
                            	<div class="cont">
                                    <h3>Angela Werder</h3>
                                    <h4>Verantwortliche für Qualitätssicherung und Dokumentation</h4>                                   
                                    <div class="separator"></div>
                                    <p>Die Verantwortliche für Qualitätssicherung und Dokumentation, koordiniert die Erstellung eines Dokumentations- und Qualitätssicherungskonzepts, überwacht dessen Umsetzung und die termingerechte Erstellung, Abnahme und Veröffentlichung der erforderlichen Dokumente.</p>
                                    <a href="mailto:i14023@hb.dhbw-stuttgart.de">Kontakt</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end .container -->
        </section>
        <!-- //ende ueber uns content-->
            
        <!--footer-->
        <div class="footer">
            <!-- footer_bottom -->
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="footer_block">
                            <div class="title">
                                <h3>Kontakt</h3>
                            </div>
                            <ul class="list-posts">
                                <li>
                                    <p>Duale Studiengang Informatik</p>
                                    <p>Florianstraße 15</p>
                                    <p>72160 Horb am Neckar</p>
                                    <p>Projektleiter</p>
                                    <a href="mailto:i14021@hb.dhbw-stuttgart.de">Sebastian Ritt</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="footer_block">
                            <div class="title">
                                <h3>Newsletter abonnieren</h3>
                            </div>
                            <p>In unserem Newsletter informieren wir Sie über alle wissenswerten Themen  rund um die Presseclipping.</p>
                            <div class="ns_block">
                                <input type="text" class="ns_input" placeholder="Email Adresse" title="Email Adresse">
                                <input type="submit" class="shortcode_button" value="Jetzt abonnieren">
                            </div>
                            <div class="social">
                                <a href="#" class="soc-f">Pr</a><a href="#" class="soc-t">e</a><a href="#" class="soc-g">
                                    ss</a><a href="#" class="soc-n">e</a><a href="#" class="soc-v">sp</a><a href="#"
                                        class="soc-r">ie</a><a href="#" class="soc-i">g</a><a href="#" class="soc-i">e</a><a href="#" class="soc-i">l</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="copyright">
                    &copy; Copyright 2016. Pressespiegel All Rights Reserved.
                </div>
            </div>
            <!-- //footer_bottom -->
        </div>
        <!--//footer-->
    </div>
    <!-- end #page -->
    <header class="fixed-menu"></header>
    <!-- javascript files-->
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="js/sorting.js"></script>
    <script src="js/homePopupLoad.js" type="text/javascript"></script>
    <script src="js/homePopup.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/plugins.js"></script>
    <script type="text/javascript" src="js/current.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <!-- animation on scrolling-->
    <script>
        new WOW().init();
    </script>
    </form>
</body>
</html>
