﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DatabaseLibrary;
using System.Security.Cryptography;
using System.Text;

namespace WebApp
{
    public partial class Profil : System.Web.UI.Page
    {
        public string Email = "";
        public string salt = "bgfe7384yeuhf49873y";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Email"] == null)
            {
                Response.Redirect("index.aspx");
            }
			Email = Session["Email"].ToString();
            bool isAdmin = false;
            try
            {
                isAdmin = new ViewDB().IsAdmin(Session["Email"].ToString());
                Session["IsAdmin"] = isAdmin;
            }
            catch (Exception ex)
            {
                
            }
            if (isAdmin)
            {
                //<li><a href="index.aspx">Abmelden</a></li>
                HtmlGenericControl oAdminMenue = new HtmlGenericControl("li");
                oAdminMenue.InnerHtml = "<a href=\"AdminPage.aspx\">Administration</a>";
                FindControl("MenuePlaceholder").Controls.Add(oAdminMenue);
            }
            //if (Request.QueryString.HasKeys())
            //{
            //    Email = Request.QueryString.Get("Email");
            //}
            Email = Session["Email"].ToString();
            //falls jemand ohne serveranbindung schauen will, einfach das hier:
            //Email = "mnjf@dmsjkf.de";  
            //wieder einkommentieren ;)

            //aufruf eigene DLL zugreifen
            ViewLibrary vL = new ViewLibrary();
            //Benutzername  
            TextBoxName.Text = vL.getUsername(Email);
            TextBoxEmail.Text = Email;

        }


        /// <summary>
        /// Ereignis, welches bei drücken von "Anderungen speichern" eintritt
        /// Die in den Textfeldern stehenden daten werden gespeichert
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnChangeData_Click(object sender, EventArgs e)
        {
            //aufruf eigene DLL zugreifen
            ViewLibrary vL = new ViewLibrary();

            string EingabeName = Request.Form["TextBoxName"].ToString();
             if (Email.Length > 1)
            {
                vL.setUsername(EingabeName, Email);
                Messagelabel.Text = "Sie haben erfolgreich ihren Username geändert";
            }
            else
            {
                Messagelabel.Text = "Ihre anfrage konnte leider nicht erfolgen. Bitte versuchen Sie es noch einmal.";
                Messagelabel.BackColor = System.Drawing.Color.Red;
            }
            TextBoxName.Text = EingabeName;


            //Email
            string EingabeEmail = Request.Form["TextBoxEmail"].ToString();
            if (Email.Length > 1)
            {
                vL.setEmail(EingabeEmail, Email);
                Messagelabel.Text = "Sie haben erfolgreich ihre Emailadresse geändert";
            }
            else
            {
                Messagelabel.Text = "Ihre anfrage konnte leider nicht erfolgen. Bitte versuchen Sie es noch einmal.";
                Messagelabel.BackColor = System.Drawing.Color.Red;
            }

            TextBoxEmail.Text = EingabeEmail;


            //Passwort
            string oldPW = Request.Form["TextBoxPW"].ToString();
            byte[] hash;
            using (var sha1 = new SHA1CryptoServiceProvider())
            {
                hash = sha1.ComputeHash(sha1.ComputeHash(Encoding.Unicode.GetBytes(oldPW + salt)));
            }
            var sb = new StringBuilder();
            foreach (byte b in hash) sb.AppendFormat("{0:x2}", b);

            String passwordHolen = sb.ToString().Replace(" ", "");

            string newPW = Request.Form["TextBoxPWneu"].ToString();
            string newPWwdh = Request.Form["TextBoxPWneuWDH"].ToString();
            string oldPWDB = vL.getUserPasswort(Email);

            if (oldPWDB == passwordHolen)
            {
                if (newPW == newPWwdh)
                {
                    if (Email.Length > 1)
                    {
                        byte[] Newhash;
                        using (var sha1 = new SHA1CryptoServiceProvider())
                        {
                            Newhash = sha1.ComputeHash(sha1.ComputeHash(Encoding.Unicode.GetBytes(newPW + salt)));
                        }
                        var newsb = new StringBuilder();
                        foreach (byte b in Newhash) newsb.AppendFormat("{0:x2}", b);

                        String newpasswordHolen = newsb.ToString().Replace(" ", "");
                        vL.setPasswort(newpasswordHolen, Email);
                        Messagelabel.Text = "Sie haben erfolgreich ihr Passwort geändert";
                    }
                    else
                    {
                        Messagelabel.Text = "Ihre anfrage konnte leider nicht erfolgen. Bitte versuchen Sie es noch einmal.";
                        Messagelabel.BorderColor = System.Drawing.Color.Yellow;

                    }
                }
                else
                {
                    Messagelabel.Text = "Ihr neues Passwort stimmt nicht überein";
                    Messagelabel.BackColor = System.Drawing.Color.Yellow;
                }

            }
            else
            {
                Messagelabel.Text = "Ihr Passwort stimmt nicht";
                Messagelabel.BackColor = System.Drawing.Color.Red;
            }



        }



        /// <summary>
        /// Ereignis, welches bei drücken von "Anderungen speichern" des
        /// Passwortes eintritt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnChangePassword_Click(object sender, EventArgs e)
        {
           

        }


    }
}