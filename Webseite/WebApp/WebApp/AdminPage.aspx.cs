﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DatabaseLibrary;
using ViewLibrary;

namespace WebApp
{
    public partial class AdminPage : System.Web.UI.Page
    {
        private bool isAdmin;

        int ID
        {
            get { return (int) Session.Contents["ID"]; }
            set { Session.Add("ID", value); }
        }

        CheckBoxList CheckBoxList
        {
            get { return (CheckBoxList) Session.Contents["CheckBoxList"]; }
            set { Session.Contents.Add("CheckBoxList", value); }
        }

        List<TextBox> SchlagwortTextBox
        {
            get { return (List<TextBox>) Session.Contents["SchlagwortTextBox"]; }
            set { Session.Contents.Add("SchlagwortTextBox",value); }
        }

        List<Interesse> AllInteressen
        {
            get { return (List<Interesse>) Session.Contents["AllInteressen"]; }
            set { Session.Contents.Add("AllInteressen", value); }
        }

        List<HtmlGenericControl> ControlsOrganization
        {
            get { return (List<HtmlGenericControl>) Session.Contents["ControlsOrganisation"]; }
            set { Session.Add("ControlsOrganisation", value); }
        }

        List<HtmlGenericControl> ControlsLeft
        {
            get { return (List<HtmlGenericControl>)Session.Contents["ControlsLeft"]; }
            set { Session.Add("ControlsLeft", value); }
        }

        List<HtmlGenericControl> ControlsRight
        {
            get { return (List<HtmlGenericControl>)Session.Contents["ControlsRight"]; }
            set { Session.Add("ControlsRight", value); }
        }

        Button oSaveButton
        {
            get { return (Button)Session.Contents["SaveButton"]; }
            set { Session.Add("SaveButton", value); }
        }

        Label oOrganisationLabel
        {
            get { return (Label) Session.Contents["OrganisationLabel"]; }
            set { Session.Add("OrganisationLabel", value); }
        }

        TextBox oOrganisationTextBox
        {
            get { return (TextBox) Session.Contents["OrganisationTextBox"]; }
            set { Session.Add("OrganisationTextBox", value); }
        }

        List<Label> oInteressenLabel
        {
            get { return (List<Label>) Session.Contents["InteressenLabel"]; }
            set { Session.Add("InterressenLabel", value);}
        }

        List<TextBox> oInteressenTextBox
        {
            get { return (List<TextBox>) Session.Contents["InteressenTextBox"]; }
            set { Session.Add("InteressenTextBox", value); }
        }

        Button oAddButton
        {
            get { return (Button) Session.Contents["AddButton"]; }
            set { Session.Add("AddButton", value); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["Email"] = "mrdrum95@aol.com";
            //Session["IsAdmin"] = true;


            if (Session["Email"] == null)
            {
                Response.Redirect("index.aspx");
            }
            isAdmin = (bool) Session["IsAdmin"];
            if (isAdmin)
            {
                //<li><a href="index.aspx">Abmelden</a></li>
                HtmlGenericControl oAdminMenue = new HtmlGenericControl("li");
                oAdminMenue.InnerHtml = "<a href=\"AdminPage.aspx\">Administration</a>";
                FindControl("MenuePlaceholder").Controls.Add(oAdminMenue);
            }
            if (!Page.IsPostBack)
            {
                ID = 0;
                CheckBoxList = new CheckBoxList();
                oInteressenLabel = new List<Label>();
                oInteressenTextBox = new List<TextBox>();
                SchlagwortTextBox = new List<TextBox>();
                oAddButton = new Button();
                oSaveButton = new Button();
                oSaveButton.ID = "ButtonSave";
                oSaveButton.Text = "Speichern";
                oSaveButton.Attributes.Add("class", "btn btn-lg btn-primary");
                ControlsOrganization = new List<HtmlGenericControl>();
                ControlsRight = new List<HtmlGenericControl>();
                ControlsLeft = new List<HtmlGenericControl>();
                AllInteressen = new List<Interesse>();
            }
            //Controls anfügen
            ControlsOrganization.ForEach(each =>
            {
                FindControl("OrganisationPlaceholder").Controls.Add(each);
            });
            ControlsLeft.ForEach(each =>
            {
                FindControl("LeftPlaceholder").Controls.Add(each);
            });
            ControlsRight.ForEach(each =>
            {
                FindControl("RightPlaceholder").Controls.Add(each);
            });

            oSaveButton.Attributes.Add("OnClick", "ButtonSave");
            oSaveButton.Click += ButtonSave;
            oAddButton.Click += ButtonAdd;
        }

        /// <summary>
        /// Öffnet die Forms um einen neue Organisation anzulegen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_Add(object sender, EventArgs e)
        {
            //Inputbox und Label 1 Reihe
            oOrganisationLabel = new Label();
            oOrganisationLabel.Text = "Organisationssname:";
            oOrganisationLabel.Attributes.Add("style","float:left;");
            oOrganisationTextBox = new TextBox();
            HtmlGenericControl o1Control = new HtmlGenericControl("h3");
            o1Control.Controls.Add(oOrganisationLabel);
            o1Control.Controls.Add(oOrganisationTextBox);
            ControlsOrganization.Add(o1Control);
            ControlsOrganization.Add(oAbstand());
            FindControl("OrganisationPlaceholder").Controls.Add(o1Control);
            //Strich
            FindControl("OrganisationPlaceholder").Controls.Add(oAbstand());

            //Interessenslisten
            //LinkerPlatzhalter
            HtmlGenericControl oTemp = GetLeftPlatzhalter();
            ControlsLeft.Add(oTemp);
            HtmlGenericControl Abstand = this.oAbstand();
            ControlsLeft.Add(Abstand);
            FindControl("LeftPlaceholder").Controls.Add(oTemp);
            FindControl("LeftPlaceholder").Controls.Add(Abstand);


            //RechterPlatzhalter
            FindControl("RightPlaceholder").Controls.Add(GetRightPlatzhalter());
        }

        /// <summary>
        /// Befüllt den Rechten Platzhalter mit einer Liste der bestehenden Interessen
        /// </summary>
        /// <returns></returns>
        public HtmlGenericControl GetRightPlatzhalter()
        {
            HtmlGenericControl oGesamtDiv = new HtmlGenericControl("div");
            try
            {
                AllInteressen = new ViewDB().GetAllInteressen();
            }
            catch (Exception e)
            {
                
            }
            CheckBoxList oCheckBoxList = new CheckBoxList();
            oCheckBoxList.ID = ID.ToString();
            ID++;
            AllInteressen.ForEach(each =>
            {
                oCheckBoxList.Items.Add(new ListItem("&nbsp;" + each.sInteresse));
            });
            HtmlGenericControl oInteressen = new HtmlGenericControl("h3");
            oInteressen.InnerHtml = "Bestehende Interessen";
            oGesamtDiv.Controls.Add(oInteressen);
            oGesamtDiv.Controls.Add(oAbstand());
            oGesamtDiv.Controls.Add(oCheckBoxList);
            CheckBoxList = oCheckBoxList;
            ControlsRight.Add(oGesamtDiv);
            return oGesamtDiv;
        }

        /// <summary>
        /// Befüllt den Linken Platzhalter mit einer Dynamischen Liste neuer Interessen
        /// </summary>
        /// <returns></returns>
        public HtmlGenericControl GetLeftPlatzhalter()
        {
            Label oLabel = new Label();
            oLabel.Text = "Interessen";
            HtmlGenericControl oControl = new HtmlGenericControl("h3");
            oControl.Controls.Add(oLabel);
            HtmlGenericControl o1Div = new HtmlGenericControl("div");
            o1Div.Attributes.Add("style", "float: left;");
            o1Div.Controls.Add(oControl);
            
            oAddButton = new Button();
            oAddButton.Text = "Neues Interesse";
            oAddButton.ID = "AddButtonInteresse";
            oAddButton.Attributes.Add("class", "btn btn-lg btn-primary");
            oAddButton.Click += ButtonAdd;
            HtmlGenericControl oDiv2 = new HtmlGenericControl("div");
            oDiv2.Attributes.Add("style", "float:right;");
            oDiv2.Controls.Add(oAddButton);
            HtmlGenericControl oGesamtDiv = new HtmlGenericControl("div");
            oGesamtDiv.Attributes.Add("class", "col-md-12");
            oGesamtDiv.Controls.Add(o1Div);
            oGesamtDiv.Controls.Add(oDiv2);
            return oGesamtDiv;
        }

        /// <summary>
        /// Wird ausgelöst wenn der Speichern Button angeklickt wird
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonSave(object sender, EventArgs e)
        {
            //Neue Interessen auslesen
            List<Interesse> oNewInteresen = new List<Interesse>();
            oInteressenTextBox.ForEach(each =>
            {
                oNewInteresen.Add(new Interesse(0,each.Text));
            });
            List<string> oSchlagworte = new List<string>();
            SchlagwortTextBox.ForEach(each =>
            {
                oSchlagworte.Add(each.Text);
            });
            for (int i = 0; i < oSchlagworte.Count; i++)
            {
                List<string> oSchlagworts = oSchlagworte[i].Split(' ').ToList();
                List<Schlagwort2> oSchlagwortes = new List<Schlagwort2>();
                oSchlagworts.ForEach(each =>
                {
                    oSchlagwortes.Add(new Schlagwort2(0,each));
                });
                oSchlagworts.ForEach(each =>
                {
                    oNewInteresen[i].Schlagworte = oSchlagwortes;
                });
            }
            //Bestehende Interessen anfügen
            List<Interesse> oBestehendeInteressen = new List<Interesse>();
            for (int i = 0; i < CheckBoxList.Items.Count; i++)
            {
                if (CheckBoxList.Items[i].Selected)
                {
                    string sName = CheckBoxList.Items[i].Text.Replace("&nbsp;", string.Empty);
                    AllInteressen.ForEach(each =>
                    {
                        if (each.sInteresse == sName)
                        {
                            oBestehendeInteressen.Add(each);
                        }
                    });
                }
            }

            //Organisationsname auslesen
            Organization oNewOrganization = new Organization(oOrganisationTextBox.Text);
            try
            {
                if (oNewOrganization.sOrganization != "")
                {
                    uint nOrganizationID = new ViewDB().SaveNewOrganisation(oNewOrganization);
                    oNewInteresen.ForEach(each =>
                    {
                        uint nInterssenID = new ViewDB().SaveNewInteresse(each);
                        each.ID = nInterssenID;
                        new ViewDB().AddInteresseToOrganisation(nOrganizationID, each);
                    });
                    oBestehendeInteressen.ForEach(each =>
                    {
                        new ViewDB().AddInteresseToOrganisation(nOrganizationID, each);
                    });
                    //Alle Controls entfernen
                    ControlsLeft.Clear();
                    ControlsRight.Clear();
                    ControlsOrganization.Clear();
                    FindControl("OrganisationPlaceholder").Controls.Clear();
                    FindControl("LeftPlaceholder").Controls.Clear();
                    FindControl("RightPlaceholder").Controls.Clear();
                    HtmlGenericControl oFertig = new HtmlGenericControl("h1");
                    oFertig.InnerHtml = "Änderungen Gespeichert";
                    FindControl("OrganisationPlaceholder").Controls.Add(oFertig);
                }


            }
            catch (Exception ex)
            {
                
            }
        }

        /// <summary>
        /// Erstellt ein neues Interessen-Control
        /// </summary>
        /// <returns></returns>
        public HtmlGenericControl GetInteresse()
        {
            HtmlGenericControl oGesamtDiv = new HtmlGenericControl("div");
            HtmlGenericControl oDiv1 = new HtmlGenericControl("div");
            HtmlGenericControl oDiv2 = new HtmlGenericControl("div");
            Label oLabel = new Label();
            oLabel.Text = "Interessenname";
            oLabel.ID = ID.ToString();
            ID++;
            HtmlGenericControl oH3 = new HtmlGenericControl("h4");
            HtmlGenericControl oH32 = new HtmlGenericControl("h4");
            HtmlGenericControl Zeile = new HtmlGenericControl("br");
            Label oLabel2 = new Label();
            oLabel2.Text = "Schlagwörter";
            oLabel2.ID = ID.ToString();
            ID++;
            oH3.Controls.Add(oLabel);
            oH32.Controls.Add(oLabel2);
            oDiv1.Controls.Add(oH3);
            oDiv1.Controls.Add(Zeile);
            oDiv1.Controls.Add(oH32);
            oDiv1.Attributes.Add("style", "float: left;");
            oGesamtDiv.Controls.Add(oDiv1);
            TextBox oTextBox = new TextBox();
            oTextBox.ID = ID.ToString();
            ID++;
            oDiv2.Controls.Add(oTextBox);
            oInteressenTextBox.Add(oTextBox);
            TextBox oTextBox2 = new TextBox();
            oTextBox2.ID = ID.ToString();
            ID++;
            SchlagwortTextBox.Add(oTextBox2);
            oDiv2.Controls.Add(Zeile);
            oDiv2.Controls.Add(oTextBox2);
            oDiv2.Attributes.Add("style","float:right;");
            oGesamtDiv.Controls.Add(oDiv2);
            oGesamtDiv.Controls.Add(oAbstand());
            oGesamtDiv.Attributes.Add("class", "col-md-12");

            

            ControlsLeft.Add(oGesamtDiv);
            return oGesamtDiv;
        }

        /// <summary>
        /// Erstellt einen Abstandshalter
        /// </summary>
        /// <returns></returns>
        public HtmlGenericControl oAbstand()
        {
            HtmlGenericControl oStrich3 = new HtmlGenericControl("div");
            oStrich3.Attributes.Add("class", "col-md-12");
            oStrich3.InnerHtml = "<hr/>";
            HtmlGenericControl oStrich2 = new HtmlGenericControl("div");
            oStrich2.Attributes.Add("class", "row");
            oStrich2.Controls.Add(oStrich3);
            HtmlGenericControl oStrich = new HtmlGenericControl("div");
            oStrich.Attributes.Add("class", "container");
            oStrich.Controls.Add(oStrich2);
            return oStrich;
        }

        /// <summary>
        /// Wird ausgelöst wenn der Neue Interesse Button aktiviert wird
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonAdd(object sender, EventArgs e)
        {
            FindControl("LeftPlaceholder").Controls.Add(GetInteresse());
        }
    }
}