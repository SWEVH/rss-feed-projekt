﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;
using System.IO;
using System.Net.Mail;
using System.Net;

public partial class Startseite : System.Web.UI.Page
{
    public string salt = "bgfe7384yeuhf49873y";
    public bool found = false;
    public Database oDatabase = new Database("root", "", "rss");

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void buttonLogin_Click(object sender, EventArgs e)
    {

        // Prüfen ob die eingegebene Email Adresse in der Datenbank schon vorhanden ist
        ArrayList selAr = oDatabase.GetColumnValues("user", "Email");
        ArrayList selUser = oDatabase.GetColumnValues("user", "Username");

        for (int i = 0; i < selAr.Count; i++)
        {
            if (selAr.Contains(Request.Form["benutzernameLog"].ToString()) || selUser.Contains(Request.Form["benutzernameLog"].ToString()))
            {

                //Versclüsselung den eingegebenen Passwort um zu prüfen ob er gleich wie in der Datenbank gespeichertes Passwort ist. 
                byte[] hash;
                string sTemp = Request.Form["passwortLog"].ToString();
                using (var sha1 = new SHA1CryptoServiceProvider())
                {
                    hash = sha1.ComputeHash(sha1.ComputeHash(Encoding.Unicode.GetBytes(Request.Form["passwortLog"].ToString() + salt)));
                }
                var sb = new StringBuilder();
                foreach (byte b in hash) sb.AppendFormat("{0:x2}", b);

                String passwordHolen = sb.ToString().Replace(" ", "");

                // Prüfen ob der eingegebenen Passwort gleich wie in der Datenbank gespeichertes Passwort mit der entsprechende Email Adresse ist
                ArrayList Password = oDatabase.ExceuteCommand("SELECT Passwort FROM user WHERE Email='" + Request.Form["benutzernameLog"].ToString() + "'");
                ArrayList selectPass = oDatabase.GetColumnValues("user", "Passwort");


                if (Password[0].ToString() == passwordHolen)
                {
                    Session.Add("Email", Request.Form["benutzernameLog"]);
                    Response.Redirect("Profil.aspx");
                }
                else
                {
                    found = true;
                }
            }
            else
            {
                found = false;

            }
        }
        if (found == true)
        {
            
            Response.Write("Passwort ist falsch");
        }
        else
        {
            Response.Write("Benutzername ist falsch");
        }


    }

    protected void buttonReg_Click(object sender, EventArgs e)
    {
       // Database oDatabase = new Database("root", "", "rss");


        //Prüfen ob die Email Adresse schon vorhanden ist
        ArrayList checkEmail = oDatabase.GetColumnValues("user", "Email");


        for (int i = 0; i < checkEmail.Count; i++)
        {

            if (checkEmail[i].ToString().Equals(Request.Form["EmailReg"].ToString()))
            {
                //Response.Write("Email Adresse ist schon vorhanden");
                return;
            }
        }

        // Passwort verschlüsseln
        byte[] hash;
        using (var sha1 = new SHA1CryptoServiceProvider())
        {
            hash = sha1.ComputeHash(sha1.ComputeHash(Encoding.Unicode.GetBytes(Request.Form["passwortReg"].ToString() + salt)));
        }
        var sb = new StringBuilder();
        foreach (byte b in hash) sb.AppendFormat("{0:x2}", b);

        //Email und Password in der Datenbank speichern - ein Konto in der Datenbank einlegen 

        var insAr = oDatabase.ExceuteCommand("insert into user(Email, Username, Passwort) values ( '" + Request.Form["EmailReg"].ToString() + "','" + Request.Form["benutzernameReg"].ToString() + "','" + sb.ToString() + "')");

        //Response.Write("Sie haben sich erfolgreich registrieren");

        Response.Redirect("https://www.google.de/?gws_rd=ssl");
    }

    protected void buttonPasswortVergessen_Click(object sender, EventArgs e)
    {
        // Prüfen ob die eingegebene Email Adresse in der Datenbank schon vorhanden ist
        ArrayList selAr = oDatabase.GetColumnValues("user", "Email");

        for (int i = 0; i < selAr.Count; i++)
        {
            if (selAr[i].ToString().Equals(Request.Form["EmailPassVergessen"].ToString()))
            {
                found = true;
                //Verschlüsselung den eingegebenen Passwort 
                byte[] hash;
                using (var sha1 = new SHA1CryptoServiceProvider())
                {
                    hash = sha1.ComputeHash(sha1.ComputeHash(Encoding.Unicode.GetBytes(Request.Form["PasswortAnfordern"].ToString() + salt)));
                }
                var sb = new StringBuilder();
                foreach (byte b in hash) sb.AppendFormat("{0:x2}", b);

                String passwordHolen = sb.ToString().Replace(" ", "");

                //Password in der Datenbank für die entsprechende Email Adresse ändern
                ArrayList insAr = oDatabase.ExceuteCommand("update user set Passwort ='" + sb.ToString() + "' where Email = '" + Request.Form["EmailPassVergessen"].ToString() + "'");

                Response.Write("Sie haben das Passwort geändert");

                //Response.Redirect("~/index.aspx");

            }
            else
            {
                found = false;
                // Response.Write("Falsche Email Adresse");
            }
        }
        if (found == false)
        {
            Response.Write("Email Adresse ist falsch");
        }

        Response.Redirect("index.aspx");
    }
}