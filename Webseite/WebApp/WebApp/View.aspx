﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="View.aspx.cs" Inherits="WebApp.View" %>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <title>Pressespiegel</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Css Files --> 
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/plugins.css" rel="stylesheet" type="text/css">
    <link href="css/homePopup.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <link href="css/pe-icon-7-stroke.css" rel="stylesheet" type="text/css">
    <link href="css/animate.css" rel="stylesheet" type="text/css">
    <!-- Font Awesome -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/buttonsRegLog.js"></script>
    <script type="text/javascript" src="js/buttonsValidierung.js"></script>
    <!-- Bootstrap -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" >
    <link href="https://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css" rel="stylesheet" type="text/css">
    <style>
    .tableSubtitle{
    display: table;
    margin:10px;    
}

.cellHeader{
    height:32px;
    display:table-cell;
    vertical-align: bottom;
 }
.cellImage{
      width:42px;
    height:32px;
    display:table-cell;
    vertical-align: bottom;
}

img, span{
   display:table-cell;
/*/*}*/*/        
    </style>

</head>
<body>
    <form id="form2" runat="server">
        <!-- Preloader -->
        <div id="preloader">
            <div pageid="status">
            </div>
        </div>
        <!--//Preloader -->
        <!-- #page -->
        <div id="page">
            <!-- header section -->
            <div id="topping">
            </div>
            <section id="top">
                <header>
                    <div class="container">
                        <!-- logo -->
                        <div id="logo">
                            <a href="#topping">Pressespiegel</a>
                        </div>
                        <!-- menu -->
                        <nav class="navmenu">
                            <ul>
                                <li class="scrollable"><a href="Profil.aspx#topping">Startseite</a></li>
                                <li class="scrollable"><a href="Profil.aspx#ueberUns">Über uns</a></li>
                                <li class="scrollable"><a href="#">Pressespiegel</a></li>
                                <li class="dropdown"><a href="#profileDropDown" id="profileDropDown" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" runat="server">Profil &nbsp;<i class="fa fa-caret-down"></i></a>
                                    <ul class="dropdown-menu" role="menu" id="profileMenueDropDown" style="display: none">
                                        <div class="row">
                                            <li><a href="Profil.aspx">Profil Einstellungen</a></li>
                                            <li><a href="Einstellungen.aspx">Pressespiegel Einstellungen</a></li>
                                            <li class="divider"></li>
                                            <li><a href="index.aspx">Abmelden</a></li>
                                                                                        <asp:PlaceHolder ID="MenuePlaceholder" runat="server">
                                                
                                            </asp:PlaceHolder>
                                        </div>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                        <!-- end menu -->
                        <div class="clear"></div>
                    </div>
                </header>
            </section>
            <!-- //end header section -->
        </div>
        <!-- end #page -->
        <header class="fixed-menu"></header>
        <!-- javascript files-->
        <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="js/sorting.js"></script>
        <script src="js/homePopupLoad.js" type="text/javascript"></script>
        <script src="js/homePopup.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/current.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>
        <!-- animation on scrolling-->
        <script>
            new WOW().init();
        </script>
    </form>
</body>
</html>
