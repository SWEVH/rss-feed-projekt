﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EinstellungenProfil.aspx.cs" Inherits="WebApp.EinstellungenProfil" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <title>Pressespiegel</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Css Files -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/plugins.css" rel="stylesheet" type="text/css">
    <link href="css/homePopup.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <link href="css/pe-icon-7-stroke.css" rel="stylesheet" type="text/css">
    <link href="css/animate.css" rel="stylesheet" type="text/css">
    <!-- Font Awesome -->
    <link href='css/google.css' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="js/buttonsRegLog.js"></script>
    <script type="text/javascript" src="js/buttonsValidierung.js"></script>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="css/pingendo_bootstrap.css" rel="stylesheet" type="text/css" />
   

</head>
<body>
    <form id="formEinstellungenProfile" runat="server">
        <div id="preloader">
            <div id="status">
            </div>
        </div>
        <!--//Preloader -->
        <!-- #page -->
        <div id="page">
            <!-- header section -->
            <div id="topping">
            </div>
            <section id="top">
                <header>
                    <div class="container">
                        <!-- logo -->
                        <div id="logo">
                            <a href="#topping">Pressespiegel</a>
                        </div>
                        <!-- menu -->
                        <nav class="navmenu">
                            <ul>
                                <li class="scrollable"><a href="#topping">Startseite</a></li>
                                <li class="scrollable"><a href="#ueberUns">Über uns</a></li>
                                <li class="scrollable"><a href="View.aspx">Pressespiegel</a></li>
                                <li class="dropdown"><a href="#profileDropDown" id="profileDropDown" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" runat="server">Profil &nbsp;<i class="fa fa-caret-down"></i></a>
                                    <ul class="dropdown-menu" role="menu" id="profileMenueDropDown" style="display: none">
                                        <div class="row">
                                            <li><a href="Profil.aspx">Profil Einstellungen</a></li>
                                            <li><a href="Einstellungen.aspx">Pressespiegel Einstellungen</a></li>
                                            <li class="divider"></li>
                                            <li><a href="index.aspx">Abmelden</a></li>
                                        </div>
                                    </ul>
                                </li>
                            </ul>
                        </nav>

                        <!-- end menu -->
                        <div class="clear"></div>
                    </div>
                </header>
            </section>
            <!-- end header section -->

            <!-- page content -->
            <div class="container">
                <div class="col-md-8">
                    <section class="page_section" id="contact">
                        <div class="container block-wrap wow fadeInUp">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="info-block bg_colored block" id="BlueBlock">
                                        <header class="head_section">
                                            <div class="icon-write">
                                                <i class="pe-5x pe-va pe-7s-diamond"></i>
                                            </div>
                                            <h2>Profil</h2>
                                            <div class="separator left"></div>
                                        </header>
                                        <p>Hier können Sie Ihre Benutzerdaten anpassen  </p>
                                        <br />
                                        <br />
                                        <div class="separator left"></div>
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="contact_form block">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <div id="note"></div>
                                            </div>
                                        </div>
                                        <div id="fields">
                                            <form id="ajax-contact-form" class="row" action="#">
                                                <!-- Benutzername-->
                                                <div class="col-md-6 col-sm-6" runat="server">
                                                    <asp:Label ID="LabelName" runat="server" Text="Benutzername: "></asp:Label>
                                                    <asp:TextBox ID="TextBoxName" runat="server" class="inp"></asp:TextBox>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <asp:Button class="shortcode_button" ID="ChangeUsername" OnClick="OnChangeUsername_Click" name="buttonChangeUsername" Text="Änderung speichern" runat="server" />
                                                    <!-- <input class="shortcode_button" type="submit" value="Änderung speichern" onclick="()" runat="server"> -->
                                                </div>
                                                <div class="clear"></div>

                                                <!-- Email -->
                                                <div class="col-md-6 col-sm-6">
                                                    <asp:Label ID="LabelEmail" runat="server" Text="EMail: "></asp:Label>
                                                    <asp:TextBox ID="TextBoxEmail" runat="server" class="inp"></asp:TextBox>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <input class="shortcode_button" type="submit" value="Änderung speichern" onclick="OnChangeEMail_Click()" runat="server">
                                                </div>
                                                <div class="clear"></div>

                                                <!-- Passwort -->
                                                <div class="col-md-6 col-sm-6">
                                                    <asp:Label ID="LabelPWHeading" runat="server" Text="PASSWORT ÄNDERN"></asp:Label>
                                                    <div class="separator left"></div>
                                                    <!-- Button Änderungen Speichern -->
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <!-- Button Änderungen Speichern -->
                                                    <input class="shortcode_button" type="submit" value="Änderung speichern" onclick="OnChangePassword_Click()" runat="server">
                                                </div>
                                                <br />
                                                <br />
                                                <br />
                                                <!-- altes Passwort -->
                                                <div class="col-md-6 col-sm-6">
                                                    <asp:Label ID="LabelPW" runat="server" Text="altes Passwort: "></asp:Label>
                                                    <asp:TextBox ID="TextBoxPW" runat="server" class="inp"></asp:TextBox>
                                                </div>
                                                <!-- neues Passwort Wiederholen -->
                                                <div class="col-md-6 col-sm-6">
                                                    <asp:Label ID="Label2" runat="server" Text="neues Passwort wiederholen: "></asp:Label>
                                                    <asp:TextBox ID="TextBoxPWneuWDH" runat="server" class="inp"></asp:TextBox>
                                                </div>
                                                <!-- neues Passwort -->
                                                <div class="col-md-6 col-sm-6">
                                                    <asp:Label ID="Label1" runat="server" Text="neues Passwort: "></asp:Label>
                                                    <asp:TextBox ID="TextBoxPWneu" runat="server" class="inp"></asp:TextBox>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <img src="images/AngabenAendern.jpg" width="250" height="400" />
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- end page content -->

            <!-- ueber uns content -->
            <section class="page_section" id="ueberUns">
                <div class="container block-wrap wow fadeInUp">
                    <div class="row carousel-box">
                        <div class="col-md-3">
                            <div class="control-block bg_black block">
                                <header class="head_section">
                                    <div class="icon-write">
                                        <i class="pe-5x pe-va pe-7s-diamond"></i>
                                    </div>
                                    <h2>Unser Projekt</h2>
                                    <div class="separator left"></div>
                                </header>
                                <p>Mit unserem Pressespiegel bekommen Sie täglich die neuesten Nachrichten.</p>
                                <p>Dieser Pressespiegel ist als Projekt von den Studenten der DHBW Stuttgart / Campus Horb.</p>
                                <div class="customNavigation">
                                    <a class="btn-prev"><i class="fa fa-angle-left"></i></a>
                                    <a class="btn-next"><i class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="owl-carousel">
                                <div class="item-service block">
                                    <img src="images/Projektmitglieder/Janna Dreher.jpg" alt="">
                                    <div class="cont">
                                        <h3>Janna Dreher</h3>
                                        <h4>Verantwortlich für Tests</h4>
                                        <div class="separator"></div>
                                        <p>Die Verantwortliche für Tests koordiniert die Erstellung eines Testkonzepts und überwacht dessen Umsetzung.</p>
                                        <a href="mailto:i14004@hb.dhbw-stuttgart.de">Kontakt</a>
                                    </div>
                                </div>
                                <div class="item-service block">
                                    <img src="images/Projektmitglieder/Felix Geiselhardt.jpg" alt="">
                                    <div class="cont">
                                        <h3>Felix Geiselhardt</h3>
                                        <h4>Verantwortlich für Recherche</h4>
                                        <div class="separator"></div>
                                        <p>Der Verantwortliche für Recherche koordiniert die Anforderungsanalyse.</p>
                                        <a href="mailto:i14009@hb.dhbw-stuttgart.de">Kontakt</a>
                                    </div>
                                </div>
                                <div class="item-service block">
                                    <img src="images/Projektmitglieder/Michal Liska.jpg" alt="">
                                    <div class="cont">
                                        <h3>Michal Liska</h3>
                                        <h4>Technische Assistent</h4>
                                        <div class="separator"></div>
                                        <p>Der technische Assistent ist für die Verwaltun der Grouppenressourcen verantwortlich.</p>
                                        <a href="mailto:i14016@hb.dhbw-stuttgart.de">Kontakt</a>
                                    </div>
                                </div>
                                <div class="item-service block">
                                    <img src="images/Projektmitglieder/Danche Nedeska.jpg" alt="">
                                    <div class="cont">
                                        <h3>Danche Nedeska</h3>
                                        <h4>Verantwortlich für Modellierung</h4>
                                        <div class="separator"></div>
                                        <p>Die Verantwortliche für Modellierung koordiniert die Modellierungsphase.</p>
                                        <a href="mailto:i14020@hb.dhbw-stuttgart.de">Kontakt</a>
                                    </div>
                                </div>
                                <div class="item-service block">
                                    <img src="images/Projektmitglieder/Sebastian Ritt.jpg" alt="">
                                    <div class="cont">
                                        <h3>Sebastian Ritt</h3>
                                        <h4>Projektleiter</h4>
                                        <div class="separator"></div>
                                        <p>Der Projektleiter koordiniert die Arbeit der Gruppe und ist Ansprechpartner für den Kunden.</p>
                                        <a href="mailto:i14021@hb.dhbw-stuttgart.de">Kontakt</a>
                                    </div>
                                </div>
                                <div class="item-service block">
                                    <img src="images/ticket book.jpg" alt="">
                                    <div class="cont">
                                        <h3>Christopher Schroth</h3>
                                        <h4>Verantwortlich für Implementierung</h4>
                                        <div class="separator"></div>
                                        <p>Der Verantwortliche für Implementierung koordiniert den Prozess der Implementierung.</p>
                                        <a href="mailto:i14022@hb.dhbw-stuttgart.de">Kontakt</a>
                                    </div>
                                </div>
                                <div class="item-service block">
                                    <img src="images/Projektmitglieder/Angela Werder.jpg" alt="">
                                    <div class="cont">
                                        <h3>Angela Werder</h3>
                                        <h4>Verantwortlich für Qualitätssicherung und Dokumentation</h4>
                                        <div class="separator"></div>
                                        <p>Die Verantwortliche für Qualitätssicherung und Dokumentation koordiniert die Erstellung eines Dokumentations- und Qualitätssicherungskonzepts, überwacht dessen Umsetzung und die termingerechte Erstellung, Abnahme und Veröffentlichung der erforderlichen Dokumente.</p>
                                        <a href="mailto:i14023@hb.dhbw-stuttgart.de">Kontakt</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end .container -->
            </section>
            <!-- //ende ueber uns content-->

            <!--footer-->
            <div class="footer">
                <!-- footer_bottom -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="footer_block">
                                <div class="title">
                                    <h3>Kontakt</h3>
                                </div>
                                <ul class="list-posts">
                                    <li>
                                        <p>Duale Studiengang Informatik</p>
                                        <p>Florianstraße 15</p>
                                        <p>72160 Horb am Neckar</p>
                                        <p>Projektleiter</p>
                                        <a href="mailto:i14021@hb.dhbw-stuttgart.de">Sebastian Ritt</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="footer_block">
                                <div class="title">
                                    <h3>Newsletter abonnieren</h3>
                                </div>
                                <p>In unserem Newsletter informieren wir Sie über alle wissenswerten Themen  rund um die Presseclipping.</p>
                                <div class="ns_block">
                                    <input type="text" class="ns_input" placeholder="Email Adresse" title="Email Adresse">
                                    <input type="submit" class="shortcode_button" value="Jetzt abonnieren">
                                </div>
                                <div class="social">
                                    <a href="#" class="soc-f">Pr</a><a href="#" class="soc-t">e</a><a href="#" class="soc-g">
                                    ss</a><a href="#" class="soc-n">e</a><a href="#" class="soc-v">sp</a><a href="#"
                                        class="soc-r">ie</a><a href="#" class="soc-i">g</a><a href="#" class="soc-i">e</a><a href="#" class="soc-i">l</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="copyright">
                        &copy; Copyright 2016. Pressespiegel All Rights Reserved.
                    </div>
                </div>
                <!-- //footer_bottom -->
            </div>
            <!--//footer-->
        </div>
        <!-- end #page -->
        <header class="fixed-menu"></header>
        <!-- javascript files-->
        <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="js/sorting.js"></script>
        <script src="js/homePopupLoad.js" type="text/javascript"></script>
        <script src="js/homePopup.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/current.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>
        <!-- animation on scrolling-->
        <script>
            new WOW().init();
        </script>
    </form>
</body>
</html>
