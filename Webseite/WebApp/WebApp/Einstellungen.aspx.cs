﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using DatabaseLibrary;
using ViewLibrary;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;


public partial class Einstellungen : System.Web.UI.Page
{
    #region variablen

    // Liste der ausgewählten Interessen
    public List<string> Interessen
    {
        get { return (List<string>)Session.Contents["Interessen"]; }
        set { Session.Add("Interessen", value); }
    }

    // Liste der ausgewählten Organisationen
    public List<string> Organisations
    {
        get { return (List<string>)Session.Contents["Organisations"]; }
        set { Session.Add("Organisations", value); }
    }

    // Liste der Checkboxen denen ein Event zugeordnet werden muss
    public List<CheckBox> Checkbox_List
    {
        get { return (List<CheckBox>)Session.Contents["Checkbox_List"]; }
        set { Session.Add("Checkbox_List", value); }
    }

    // Liste der Buttons denen bei jedem PostBack ein Event zugeordnet werden muss
    public List<Button> Button_List
    {
        get { return (List<Button>)Session.Contents["Button_List"]; }
        set { Session.Add("Button_List", value); }
    }

    // Liste der Controls die bei jedem PostBack erzeugt werden 
    public List<HtmlGenericControl> Controls
    {
        get { return (List<HtmlGenericControl>)Session.Contents["Controls"]; }
        set { Session.Add("Controls", value); }
    }

    // Index für die Identifizierung der Controls
    public int Index
    {
        get { return (int)Session.Contents["Index"]; }
        set { Session.Add("Index", value); }
    }

    public List<Interesse> UserInteressen
    {
        get { return (List<Interesse>)Session.Contents["UserInteressen"]; }
        set { Session.Add("UserInteressen", value); }
    }

    public List<Organization> AllOrganizations
    {
        get { return (List<Organization>)Session.Contents["AllOrganizations"]; }
        set { Session.Add("AllOrganizations", value); }
    }

    public List<Organization> UserOrganizations
    {
        get { return (List<Organization>)Session.Contents["UserOrganizations"]; }
        set { Session.Add("UserOrganizations", value); }
    }

    public List<Interesse> AllInteressen
    {
        get { return (List<Interesse>)Session.Contents["AllInteressen"]; }
        set { Session.Add("AllInteressen", value); }
    }

    public string EmailSendTime
    {
        get { return (string) Session.Contents["EmailSendTime"]; }
        set { Session.Add("EmailSendTime", value); }
    }

    public CheckBox EmailCheckBox
    {
        get { return (CheckBox) Session.Contents["EmailCheckBox"]; }
        set { Session.Add("EmailCheckBox", value); }
    }

    public bool ReceivesEmail
    {
        get { return (bool) Session.Contents["ReceivesEmail"]; }
        set { Session.Add("ReceivesEmail", value); }
    }

    public List<string> sTimes
    {
        get { return (List<string>) Session.Contents["sTimes"]; }
        set { Session.Add("sTimes", value); }
    }

    public List<string> sDropDownOrganizations
    {
        get { return (List<string>) Session.Contents["DropDownOrganizations"]; }
        set { Session.Contents.Add("DropDownOrganizations", value); }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

        // Falls Seite das erste mal geladen wird werden die Listen die in die Session-Variablen gespeichert werden
        // initialisiert
        //Session["Email"] = "mrdrum95@aol.com";
        //Session["IsAdmin"] = true;
        //Wenn user nicht eingeloggt ist wird er auf die Startseite umgeleitet
        if (Session["Email"] == null)
        {
            Response.Redirect("index.aspx");
        }
        bool isAdmin = (bool)Session["IsAdmin"];
        if (isAdmin)
        {
            //<li><a href="index.aspx">Abmelden</a></li>
            HtmlGenericControl oAdminMenue = new HtmlGenericControl("li");
            oAdminMenue.InnerHtml = "<a href=\"AdminPage.aspx\">Administration</a>";
            FindControl("MenuePlaceholder").Controls.Add(oAdminMenue);
        }

        if (!Page.IsPostBack)
        {
            List<HtmlGenericControl> Controls = new List<HtmlGenericControl>();
            Session.Add("Controls", Controls);
            int Index = 0;
            Session.Add("Index", Index);
            List<Button> Buttons = new List<Button>();
            Session.Add("Button_List", Buttons);
            List<string> Organisations = new List<string>();
            Session.Add("Organisations", Organisations);
            List<CheckBox> Checkboxes = new List<CheckBox>();
            Session.Add("Checkbox_List", Checkboxes);
            List<string> Ints = new List<string>();
            Session.Add("Interessen", Ints);
            
            EmailCheckBox = new CheckBox();
            EmailCheckBox.AutoPostBack = true;
            EmailCheckBox.Text = "&nbsp;Pressespiegel per Email erhalten";
            EmailCheckBox.ID = "Checkbox_Email";
            try
            {
                sTimes = new ViewDB().GetEmailTime(Session["Email"].ToString());
                UserOrganizations = new ViewDB().GetOrganizations(Session["Email"].ToString());
                Session.Add("Organizations", UserOrganizations);
                AllOrganizations = new ViewDB().GetAllOrganizations();
                Session.Add("AllOrganizations", AllOrganizations);
                UserInteressen = new ViewDB().GetInteressenForUser(Session["Email"].ToString());
                Session.Add("UserInteressen", UserInteressen);
                AllInteressen = new ViewDB().GetAllInteressen();
                Session.Add("AllInteressen", AllInteressen);
                ReceivesEmail = new ViewDB().UserReceivesEmail(Session["Email"].ToString());
                Session.Add("ReceivesEmail", ReceivesEmail);

                if (ReceivesEmail)
                {
                    sTimes = new ViewDB().GetEmailTime(Session["Email"].ToString());
                    EmailCheckBox.Checked = true;
                }
                sDropDownOrganizations = new List<string>();
                string[] sOrgs = new string[AllOrganizations.Count];
                for (int i = 0; i < sOrgs.Length; i++)
                {
                    sOrgs[i] = AllOrganizations[i].sOrganization;
                }
                List<string> sUserOrganizations = new List<string>();
                UserOrganizations.ForEach(each =>
                {
                    sUserOrganizations.Add(each.sOrganization);
                });
                sOrgs.ToList().ForEach(each =>
                {
                    if (!sUserOrganizations.Contains(each)) sDropDownOrganizations.Add(each);
                });
            }
            catch (Exception ex)
            {

            }


        }

        //Grundcontainer erstellen
        FindControl("Org_Container").Controls.Add(Horizontal());
        // Org_Container.Controls.Add(Horizontal());



        //Heading

        HtmlGenericControl Heading_row = new HtmlGenericControl("div");
        Heading_row.Attributes.Add("class", "row");
        HtmlGenericControl Heading = new HtmlGenericControl("div");
        Heading.Attributes.Add("class", "col-md-6");
        HtmlGenericControl Heading_text = new HtmlGenericControl("h1");
        Heading_text.Attributes.Add("class", "text-left");
        Heading_text.InnerText = "Organisationen";

        Heading.Controls.Add(Heading_text);
        Heading_row.Controls.Add(Heading);


        //Dropdown hinzufügen

        //string[] Orgs = new string[3];
        //Orgs[0] = "DHBW Horb";
        //Orgs[1] = "DHBW Stuttgart";
        //Orgs[2] = "Daimler";


        


        HtmlGenericControl dropdown_div = new HtmlGenericControl("div");
        dropdown_div.Attributes.Add("class", "col-md-6 text right");
        HtmlGenericControl dropdown_h1 = new HtmlGenericControl("h1");

        dropdown_h1.Controls.Add(Dropdown(sDropDownOrganizations.ToArray()));
        dropdown_div.Controls.Add(dropdown_h1);
        Heading_row.Controls.Add(dropdown_div);
        FindControl("Org_Container").Controls.Add(Heading_row);
        FindControl("Org_Container").Controls.Add(Horizontal());
        //Org_Container.Controls.Add(Heading_row);
        //Org_Container.Controls.Add(Horizontal());

        // Block für die Anzeige der Bereits vorhandenen Organisationen die bearbeitet werden können

        if (!Page.IsPostBack)
        {
            //Hier die Organisationen des User Laden

            HtmlGenericControl Organisationen = new HtmlGenericControl("ul");
            Organisationen.Attributes.Add("class", "media-list");
            List<Interesse> oUserInteressen = new ViewDB().GetInteressenForUser(Session["Email"].ToString());
            List<string> sUserInteressen = new List<string>();
            oUserInteressen.ForEach(each =>
            {
                sUserInteressen.Add(each.sInteresse);
            });
            UserOrganizations.ForEach(each =>
            {
                Organisation(each.sOrganization, each.GetInteressen().ToArray());
                Organisations.Add(each.sOrganization);
                Checkbox_List.ForEach(each1 =>
                {
                    string sTemp = each1.Text.Replace(" &nbsp; &nbsp; &nbsp;", string.Empty);
                    if (sUserInteressen.Contains(sTemp))
                    {
                        each1.Checked = true;
                    }
                });
            });
        }


        // Alle Buttons in der Liste erhalten das Remove event
        if (Checkbox_List.Count > 0)
        {
            foreach (CheckBox Check in Checkbox_List)
            {
                Check.CheckedChanged += Checked_Changed;
            }
        }
        if (Button_List.Count > 0)
        {
            foreach (Button Btn in Button_List)
            {
                Btn.Click += Remove_Click;
            }
        }
        EmailCheckBox.CheckedChanged += EmailCheckedChanged;
        FindControl("EmailCheckboxPlaceholder").Controls.Add(EmailCheckBox);

        #region EmailEinstellungen
        if (EmailCheckBox.Checked)
        {
            //Email Controls anfügen
            CheckBox Checkbox_Morning = new CheckBox();
            Checkbox_Morning.AutoPostBack = true;
            Checkbox_Morning.Text = "&nbsp;Morgends&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            Checkbox_Morning.ID = "Checkbox_Morning";
            Checkbox_Morning.CheckedChanged += TimeCheckedChanged;
            if (sTimes.Contains("08:00")) Checkbox_Morning.Checked = true;
            FindControl("EmailPlaceholder").Controls.Add(Checkbox_Morning);

            CheckBox Checkbox_Noon = new CheckBox();
            Checkbox_Noon.AutoPostBack = true;
            Checkbox_Noon.Text = "&nbsp;Mittags&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            Checkbox_Noon.ID = "Checkbox_Noon";
            Checkbox_Noon.CheckedChanged += TimeCheckedChanged;
            if (sTimes.Contains("12:00")) Checkbox_Noon.Checked = true;
            FindControl("EmailPlaceholder").Controls.Add(Checkbox_Noon);

            CheckBox Checkbox_Evening = new CheckBox();
            Checkbox_Evening.AutoPostBack = true;
            Checkbox_Evening.Text = "&nbsp;Abends";
            Checkbox_Evening.ID = "Checkbox_Evening";
            Checkbox_Evening.CheckedChanged += TimeCheckedChanged;
            if (sTimes.Contains("17:00")) Checkbox_Evening.Checked = true;
            FindControl("EmailPlaceholder").Controls.Add(Checkbox_Evening);

            //Button oSaveButton = new Button();
            //oSaveButton.Text = "Speichern";
            //oSaveButton.Click += EmailSave;
            //oSaveButton.ID = "Button_EmailSave";
            //oSaveButton.CssClass = "btn btn-lg btn-primary";
            //FindControl("EmailButton").Controls.Add(oSaveButton);
        }
#endregion
        // Alle Controls in der Liste werden der Seite hinzugefügt

        if (((List<HtmlGenericControl>)Session.Contents["Controls"]).Count > 0)
        {

            for (int i = Controls.Count - 1; i >= 0; i--)
            {
                HtmlGenericControl Orgis = new HtmlGenericControl("ul");
                Orgis.Attributes.Add("class", "media-list");
                Orgis.Controls.Add(Controls[i]);
                FindControl("Orglist").Controls.Add(Orgis);
                //Orglist.Controls.Add(Orgis);
            }

        }

    }

    /// <summary>
    /// Speichert die Änderungen des Emailversands
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EmailSave(object sender, EventArgs e)
    {
        try
        {
            new ViewDB().SaveEmailTime(Session["Email"].ToString(), sTimes);
        }
        catch (Exception ex)
        {
            
        }
    }
    
    /// <summary>
    /// Registriert eine Änderung der Emailzeitauswahl
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EmailCheckedChanged(object sender, EventArgs e)
    {
        try
        {
            sTimes = new ViewDB().GetEmailTime(Session["Email"].ToString());
        }
        catch (Exception ex)
        {
            
            
        }

        CheckBox Checkbox_Morning = new CheckBox();
        Checkbox_Morning.AutoPostBack = true;
        Checkbox_Morning.Text = "&nbsp;Morgends&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        Checkbox_Morning.ID = "Checkbox_Morning";
        Checkbox_Morning.CheckedChanged += TimeCheckedChanged;
        if (sTimes.Contains("08:00")) Checkbox_Morning.Checked = true;
        CheckBox Checkbox_Noon = new CheckBox();
        Checkbox_Noon.AutoPostBack = true;
        Checkbox_Noon.Text = "&nbsp;Mittags&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        Checkbox_Noon.ID = "Checkbox_Noon";
        Checkbox_Noon.CheckedChanged += TimeCheckedChanged;
        if (sTimes.Contains("12:00")) Checkbox_Noon.Checked = true;
        CheckBox Checkbox_Evening = new CheckBox();
        Checkbox_Evening.AutoPostBack = true;
        Checkbox_Evening.Text = "&nbsp;Abends";
        Checkbox_Evening.ID = "Checkbox_Evening";
        Checkbox_Evening.CheckedChanged += TimeCheckedChanged;
        if (sTimes.Contains("17:00")) Checkbox_Evening.Checked = true;
        CheckBox oSender = sender as CheckBox;
        if (oSender.Checked)
        {
            //Email Controls anfügen

            FindControl("EmailPlaceholder").Controls.Add(Checkbox_Morning);


            FindControl("EmailPlaceholder").Controls.Add(Checkbox_Noon);


            FindControl("EmailPlaceholder").Controls.Add(Checkbox_Evening);

            //Button oSaveButton = new Button();
            //oSaveButton.Text = "Speichern";
            //oSaveButton.Click += EmailSave;
            //oSaveButton.ID = "Button_EmailSave";
            //oSaveButton.CssClass = "btn btn-lg btn-primary";
            //FindControl("EmailButton").Controls.Add(oSaveButton);
        }
        else
        {
            FindControl("EmailPlaceholder").Controls.Clear();
            //FindControl("EmailButton").Controls.Clear();
            sTimes.Clear();
        }
    }

    /// <summary>
    /// Wird beim Ändern der Timecheckboxen ausgelöst
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TimeCheckedChanged(object sender, EventArgs e)
    {
        CheckBox oSender = sender as CheckBox;
        if (oSender.Checked)
        {
            if (oSender.ID.Contains("Morning"))
            {
                if (!sTimes.Contains("08:00"))
                {
                    sTimes.Add("08:00");
                }
            }
            else if (oSender.ID.Contains("Noon"))
            {
                if (!sTimes.Contains("12:00"))
                {
                    sTimes.Add("12:00");
                }
            }
            else if (oSender.ID.Contains("Evening"))
            {
                if (!sTimes.Contains("17:00"))
                {
                    sTimes.Add("17:00");
                }
            }
        }
        else
        {
            if (oSender.ID.Contains("Morning"))
            {
                if (sTimes.Contains("08:00"))
                {
                    sTimes.Remove("08:00");
                }
            }
            else if (oSender.ID.Contains("Noon"))
            {
                if (sTimes.Contains("12:00"))
                {
                    sTimes.Remove("12:00");
                }
            }
            else if (oSender.ID.Contains("Evening"))
            {
                if (sTimes.Contains("17:00"))
                {
                    sTimes.Remove("17:00");
                }
            }
        }
        
    }

    /// <summary>
    /// Erstellt das Dropdown-Menü neu
    /// </summary>
    private void RebuildDropdown()
    {
        FindControl("Org_Container").Controls.Clear();
        HtmlGenericControl Heading_row = new HtmlGenericControl("div");
        Heading_row.Attributes.Add("class", "row");
        HtmlGenericControl Heading = new HtmlGenericControl("div");
        Heading.Attributes.Add("class", "col-md-6");
        HtmlGenericControl Heading_text = new HtmlGenericControl("h1");
        Heading_text.Attributes.Add("class", "text-left");
        Heading_text.InnerText = "Organisationen";

        Heading.Controls.Add(Heading_text);
        Heading_row.Controls.Add(Heading);
        HtmlGenericControl dropdown_div = new HtmlGenericControl("div");
        dropdown_div.Attributes.Add("class", "col-md-6 text right");
        HtmlGenericControl dropdown_h1 = new HtmlGenericControl("h1");

        dropdown_h1.Controls.Add(Dropdown(sDropDownOrganizations.ToArray()));
        dropdown_div.Controls.Add(dropdown_h1);
        Heading_row.Controls.Add(dropdown_div);
        FindControl("Org_Container").Controls.Add(Heading_row);
        FindControl("Org_Container").Controls.Add(Horizontal());
    }

    /// <summary>
    /// Wird ein Element des Dropdowns angeklickt wird die entsprechende Organisation erzeugt und direkt hinzugefügt.
    /// Außerdem wird sie der Liste der Controls hinzugefügt um auch nach dem PostBack noch existent zu sein.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Add_Organisation(object sender, EventArgs e)
    {

        string Name = (sender as Button).Text;
        if (!Organisations.Contains(Name))
        {
            Organisations.Add(Name);
            sDropDownOrganizations.Remove(Name);
            RebuildDropdown();
            UserOrganizations.Add(AllOrganizations.Single(o => o.sOrganization == Name));
            (sender as Button).Enabled = false;
            
            //string[] Interessen = new string[3];
            //Interessen[0] = "Beispiel 1";
            //Interessen[1] = "Beispiel 2";
            //Interessen[2] = "Beispiel 3";
            HtmlGenericControl Organisationen = new HtmlGenericControl("ul");

            Organisationen.ID = "Org_" + (FindControl("Orglist").Controls.Count + Index).ToString();

            //Organisationen.ID = "Org_" + (Orglist.Controls.Count + Index).ToString();
            Organisationen.Attributes.Add("class", "media-list");

            //Hier Interessen aus dem Objekt für die Organisation nehmen
            List<Organization> oAllOrganizations = Session.Contents["AllOrganizations"] as List<Organization>;
            string[] sInteressen = new string[0];
            oAllOrganizations?.ForEach((each) =>
            {
                if (each.sOrganization == Name)
                {
                    sInteressen = each.GetInteressen().ToArray();
                }
            });
            Organisationen.Controls.Add(Organisation(Name, sInteressen));

            FindControl("Orglist").Controls.AddAt(0, Organisationen);
            //Orglist.Controls.AddAt(0, Organisationen);
        }
        else
        {
            return;
        }
    }


    /// <summary>
    /// Funktion die ein Abstandsobjekt erzeugt
    /// </summary>
    /// <returns></returns>
    public HtmlGenericControl Horizontal()
    {
        HtmlGenericControl row = new HtmlGenericControl("div");
        row.Attributes.Add("class", "row");
        HtmlGenericControl col = new HtmlGenericControl("div");
        col.Attributes.Add("class", "col-md-12");
        col.InnerHtml = "<hr>";
        row.Controls.Add(col);
        return row;
    }


    /// <summary>
    /// Generiert das Dropdown-Menü zum Hinzufügen der Organisationen
    /// </summary>
    /// <param name="Organisationen"></param>
    /// <returns></returns>
    public HtmlGenericControl Dropdown(string[] Organisationen)
    {
        HtmlGenericControl Dropdown_row = new HtmlGenericControl("div");
        Dropdown_row.Attributes.Add("class", "row");
        HtmlGenericControl Dropdown_column = new HtmlGenericControl("div");
        Dropdown_column.Attributes.Add("class", "col-md-12 text-right");
        HtmlGenericControl Dropdown_div = new HtmlGenericControl("div");
        Dropdown_div.Attributes.Add("class", "btn-group btn-group-lg");
        HtmlGenericControl Dropdown_Button = new HtmlGenericControl("a");
        Dropdown_Button.Attributes.Add("class", "btn btn-primary dropdown-toggle");
        Dropdown_Button.Attributes.Add("data-toggle", "dropdown");
        Dropdown_Button.InnerHtml = "Hinzufügen &nbsp; &nbsp;<span class=\"fa fa-caret-down\"></span>";


        // Dropdown Items

        HtmlGenericControl Dropdown_menu = new HtmlGenericControl("ul");
        Dropdown_menu.Attributes.Add("class", "dropdown-menu");
        Dropdown_menu.Attributes.Add("role", "menu");
        Dropdown_div.Controls.Add(Dropdown_Button);

        for (int i = 0; i < Organisationen.Length; i++)
        {
            HtmlGenericControl Dropdown_Item = new HtmlGenericControl("li");
            Dropdown_Item.Attributes.Add("class", "text-center");
            Button Dropdown_Action = new Button();
            Dropdown_Action.ID = Organisationen[i];
            Dropdown_Action.Attributes.Add("class", "btn btn-default btn-lg btn-block");
            Dropdown_Action.Attributes.Add("style", "border:none");
            Dropdown_Action.Text = Organisationen[i];
            Dropdown_Action.Click += Add_Organisation;
            Dropdown_Item.Controls.Add(Dropdown_Action);
            Dropdown_menu.Controls.Add(Dropdown_Item);
        }

        Dropdown_div.Controls.Add(Dropdown_menu);
        Dropdown_column.Controls.Add(Dropdown_div);
        Dropdown_row.Controls.Add(Dropdown_column);
        return Dropdown_row;
    }


    /// <summary>
    ///Erzeugt ein Organisations-Listitem, fügt es der Liste der Controls hinzu und gibt es zurück, sodass es direkt angezeigt werden kann
    /// </summary>
    /// <param name="Name"></param>
    /// <param name="Interesen"></param>
    /// <returns></returns>

    public HtmlGenericControl Organisation(string Name, string[] Interesen)
    {

        HtmlGenericControl Listitem = new HtmlGenericControl("li");
        Listitem.Attributes.Add("class", "media");

        Listitem.ID = "Listitem_" + (FindControl("Orglist").Controls.Count + Index).ToString();
        // Listitem.ID = "Listitem_" + (Orglist.Controls.Count + Index).ToString();

        HtmlGenericControl a = new HtmlGenericControl("a");
        a.Attributes.Add("class", "pull-left");
        a.Attributes.Add("href", "#");
        a.ID = "a_" + (FindControl("Orglist").Controls.Count + Index).ToString();
        //a.ID = "a_" + (Orglist.Controls.Count + Index).ToString();


        HtmlGenericControl img = new HtmlGenericControl("img");
        img.Attributes.Add("class", "img-circle media-object");
        //Hier das IMG der Organisation laden
        if (System.IO.File.Exists(Server.MapPath("~") + @"images/Organisation icons/" + Name + ".jpg"))
        {
            img.Attributes.Add("src", @"images/Organisation icons/" + Name + ".jpg");
        }
        else
        {
            img.Attributes.Add("src", @"images/Organisation icons/logo.png");
        }
        img.Attributes.Add("height", "64");
        img.Attributes.Add("width", "64");
        img.ID = "img_" + (FindControl("Orglist").Controls.Count + Index).ToString();
        //        img.ID = "img_" + (Orglist.Controls.Count + Index).ToString();
        a.Controls.Add(img);

        Listitem.Controls.Add(a);

        HtmlGenericControl body = new HtmlGenericControl("div");
        body.Attributes.Add("class", "media-body");

        HtmlGenericControl heading = new HtmlGenericControl("h4");
        heading.Attributes.Add("class", "col-md-6");
        heading.InnerText = Name;
        heading.ID = "heading_" + (FindControl("Orglist").Controls.Count + Index).ToString();
        // heading.ID = "heading_" + (Orglist.Controls.Count + Index).ToString();

        HtmlGenericControl div = new HtmlGenericControl("div");
        div.Attributes.Add("class", "col-md-6 text-right");

        div.ID = "div_" + (FindControl("Orglist").Controls.Count + Index).ToString();
        //div.ID = "div_" + (Orglist.Controls.Count + Index).ToString();
        Button Remove = new Button();
        Remove.ID = "Button_" + Name; //+ (Orglist.Controls.Count + Index).ToString();
        Remove.Text = "Entfernen";
        Remove.Attributes.Add("class", "btn btn-primary");
        Button_List.Add(Remove);
        div.Controls.Add(Remove);
        body.Controls.Add(heading);
        body.Controls.Add(div);
        body.Controls.Add(Horizontal());

        HtmlGenericControl int_div = new HtmlGenericControl("div");
        int_div.Attributes.Add("class", "col-md-12 text-right");

        int_div.ID = "intdiv_" + (FindControl("Orglist").Controls.Count + Index).ToString();
        //int_div.ID = "intdiv_" + (Orglist.Controls.Count + Index).ToString();
        int i = 0;
        foreach (string Interesse in Interesen)
        {

            CheckBox Check = new CheckBox();
            Check.ID = "Check_" + Name + "_" + (FindControl("Org_Container").Controls.Count + i + Index).ToString();
            // Check.ID = "Check_" + Name + "_" + (Org_Container.Controls.Count + i + Index).ToString();
            Check.Text = Interesse + " &nbsp; &nbsp; &nbsp;";
            Check.TextAlign = TextAlign.Left;
            Check.AutoPostBack = true;
            Checkbox_List.Add(Check);
            int_div.Controls.Add(Check);
            HtmlGenericControl br = new HtmlGenericControl("br");

            br.ID = "br_" + Name + "_" + (FindControl("Org_Container").Controls.Count + i + Index).ToString();
            //br.ID = "br_" + Name + "_" + (Org_Container.Controls.Count + i + Index).ToString();
            int_div.Controls.Add(br);
            i++;
        }
        body.Controls.Add(int_div);

        body.ID = "body_" + (FindControl("Orglist").Controls.Count + Index).ToString();
        // body.ID = "body_" + (Orglist.Controls.Count + Index).ToString();
        Listitem.Controls.Add(body);
        Controls.Add(Listitem);
        Index++;
        return Listitem;
    }

    /// <summary>
    /// Entfernt eine Organisation aus der Ansicht
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Remove_Click(object sender, EventArgs e)
    {
        //Die Organisation in den UserOrganizations entfernen
        Button oSender = sender as Button;
        string sOrganization = oSender.ID.Substring(oSender.ID.IndexOf('_') + 1);
        sDropDownOrganizations.Add(sOrganization);
        RebuildDropdown();
        //Nun die korrespondieren Organisation in AllOrganizations finden und aus den UserOrganizations entfernen
        for (int i = 0; i < UserOrganizations.Count; i++)
        {
            if (UserOrganizations[i].sOrganization == sOrganization)
            {
                //Die Interessen dieser Organisation auch entfernen
                foreach (Interesse oInteresse in UserOrganizations[i].sInteressen)
                {
                    for (int j = 0; j < UserInteressen.Count; j++)
                    {
                        if (UserInteressen[j].sInteresse == oInteresse.sInteresse)
                        {
                            UserInteressen.RemoveAt(j);
                            break;
                        }
                    }
                }
                UserOrganizations.RemoveAt(i);
                break;
            }
        }

        string Org = (sender as Button).ID.Replace("Button_", "");
        Organisations.Remove(Org);
        Controls.Remove(((HtmlGenericControl)(sender as Button).Parent.Parent.Parent));

        FindControl("Orglist").Controls.Remove(((HtmlGenericControl)(sender as Button).Parent.Parent.Parent.Parent));
        //       Orglist.Controls.Remove(((HtmlGenericControl)(sender as Button).Parent.Parent.Parent.Parent));   
    }

    /// <summary>
    /// Wird beim drücken des Speichern Buttons der Organisationsauswahl aktiviert
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Button_Save(object sender, EventArgs e)
    {
        //Nun UserInteressen und UserOrganizations in Datenbank speichern
        try
        {
            new ViewDB().SaveUserInteressen(UserInteressen, Session.Contents["Email"].ToString());
            new ViewDB().SaveUserOrganizations(UserOrganizations, Session.Contents["Email"].ToString());
        }
        catch (Exception ex)
        {

        }
    }

    /// <summary>
    /// Wenn sich der Status ändert wird das Interesse der Liste hinzugefügt oder daraus entfernt
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Checked_Changed(object sender, EventArgs e)
    {
        CheckBox oSender = sender as CheckBox;
        if (oSender.Checked)
        {
            string sTemp = oSender.Text.Replace(" &nbsp; &nbsp; &nbsp;", string.Empty);
            Interesse oInteresse = new Interesse(0, "");
            bool bFound = false;

            //Das korrespondierende Interessen Objekt in der AllInteressen Liste suchen und den User Interessen anfügen
            AllInteressen.ForEach(each =>
            {
                if (each.sInteresse == sTemp)
                {
                    bFound = true;
                    oInteresse = each;
                }
            });
            if (bFound)
            {
                UserInteressen.Add(oInteresse);
            }
        }
        else if (oSender.Checked == false)
        {
            string sTemp = oSender.Text.Replace(" &nbsp; &nbsp; &nbsp;", string.Empty);
            Interesse oInteresse = new Interesse(0, "");
            bool bFound = false;

            //Das korrespondierende Interessen Objekt in der AllInteressen Liste suchen und in den UserInteressen entfernen
            AllInteressen.ForEach(each =>
            {
                if (each.sInteresse == sTemp)
                {
                    bFound = true;
                    oInteresse = each;
                }
            });
            if (bFound)
            {
                for (int i = 0; i < UserInteressen.Count; i++)
                {
                    if (UserInteressen[i].sInteresse == sTemp)
                    {
                        UserInteressen.RemoveAt(i);
                        break;
                    }
                }
            }
        }

        if ((sender as CheckBox).Checked)
        {
            Interessen.Add((sender as CheckBox).Text);
        }
        else
        {
            Interessen.Remove((sender as CheckBox).Text);
        }
    }
}