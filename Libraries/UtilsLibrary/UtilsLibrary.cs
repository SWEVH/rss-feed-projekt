﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilsLibrary
{
    public class Interesse
    {
        private uint nId;
        public string sInteresse;
        public List<Schlagwort> oSchlagworte = new List<Schlagwort>();

        public Interesse(string sInteresse, List<Schlagwort> oSchlagworts)
        {
            this.sInteresse = sInteresse;
            this.oSchlagworte = oSchlagworts;
        }

        public void SetID(uint nId)
        {
            this.nId = nId;
        }

        public uint GetID()
        {
            return this.nId;
        }
    }

    public class Schlagwort
    {
        uint nId;
        public string sSchlagwort;

        public Schlagwort(string Schlagwort)
        {
            this.sSchlagwort = Schlagwort.Replace("\t",string.Empty);
            this.sSchlagwort = this.sSchlagwort.Replace("\r", string.Empty);
        }

        public void SetId(uint nId)
        {
            this.nId = nId;
        }

        public uint GetID()
        {
            return this.nId;
        }
    }

}
