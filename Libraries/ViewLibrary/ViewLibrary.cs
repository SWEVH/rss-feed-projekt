﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ViewLibrary
{
    /// <summary>
    /// Throws: FileNotFoundException, ExternalException, OutOfMemoryException
    /// Klasse die das Speichern und Laden der Icons für die View ermöglicht
    /// </summary>
    public class OrganizationIcon
    {
        private const string Directory = @"C:\Projekt\Pictures\Organizations\";
        uint nId;

        public OrganizationIcon(uint OrganizationID)
        {
            this.nId = OrganizationID;
        }

        /// <summary>
        /// Lädt das zur Organisation gehörende Icon aus dem Dateisystem
        /// </summary>
        /// <returns>Organisations Icon</returns>
        public Image LoadIcon()
        {
            return Image.FromFile(Directory+nId);
        }

        /// <summary>
        /// Speichert das zur Organsiation gehörende Icon in das Dateisystem
        /// </summary>
        /// <param name="oImage"></param>
        public bool SetIcon(Image oImage)
        {
            try
            {
                oImage.Save(Directory + nId);
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }

    public class Organization
    {
        public string sOrganization;
        public List<Interesse> sInteressen;
        public uint ID;

        public Organization(string sName)
        {
            this.sOrganization = sName;
        }

        public void SetInteressen(List<Interesse> sList)
        {
            this.sInteressen = sList;
        }

        public List<string> GetInteressen()
        {
            List<string> sInteressenList = new List<string>();
            sInteressen.ForEach(each =>
            {
                sInteressenList.Add(each.sInteresse);
            });
            return sInteressenList;
        } 
    }

    public class Interesse
    {
        public uint ID;
        public string sInteresse;
        public List<Schlagwort2> Schlagworte = new List<Schlagwort2>();

        public Interesse(uint nId, string sInteresse)
        {
            this.ID = nId;
            this.sInteresse = sInteresse;
        }
    }

    public class Schlagwort2
    {
        public uint nID;
        public string sSchlagwort;

        public Schlagwort2(uint nID, string sSchlagwort)
        {
            this.nID = nID;
            this.sSchlagwort = sSchlagwort;
        }
    }
}
