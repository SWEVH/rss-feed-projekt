﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CrawlerLibrary
{
    /// <summary>
    /// Data beinhaltet eine gecrawlte Nachricht
    /// </summary>
    public class Data
    {
        /// <value name="subject">Titel der Nachricht</value>
        public string subject;
        /// <value name="link">Link zum kompletten Artikel</value>
        public string link;
        /// <value name="summary">Zusammenfassung des Artikels</value>
        public string summary;

        public Data(string subject, string link, string summary, string regex)
        {
            if (subject == null)
            {
                throw new ArgumentNullException("subject cannot be null.");
            }
            if (link == null)
            {
                throw new ArgumentNullException("link cannot be null.");
            }
            if (summary == null)
            {
                throw new ArgumentNullException("summary cannot be null.");
            }
            if (regex == null)
            {
                this.subject = replace(subject, "'", "''");
                this.link = link;
                //this.summary = summary;
                this.summary = finalClean(summary);
            }
            else
            {
                Regex rgx = new Regex(regex);

                this.subject = replace(subject, "'", "''");
                this.link = link;
                //this.summary = summary;
                this.summary = showMatch(summary, regex);
            }

        }

        public Data(string subject, string link, string summary)
        {
            if (subject == null)
            {
                throw new ArgumentNullException("subject cannot be null.");
            }
            if (link == null)
            {
                throw new ArgumentNullException("link cannot be null.");
            }
            if (summary == null)
            {
                throw new ArgumentNullException("summary cannot be null.");
            }

            this.subject = replace(subject, "'", "''");
            this.link = link;
            //this.summary = summary;
            this.summary = finalClean(summary);

        }

        /// <summary>
        /// Gibt Data aus in die Console
        /// </summary>
        public void printToConsole()
        {
            Console.WriteLine(subject);
            Console.WriteLine(summary);
            Console.WriteLine(link);
            Console.WriteLine();
        }

        /// <summary>
        /// Text mit einem Regex bearbeiten und resultat ausgeben
        /// </summary>
        /// <param name="text">Text der bearbeitet werden soll</param>
        /// <param name="expr">Regex</param>
        /// <returns>String - alle gecapturten Gruppen</returns>
        private string showMatch(string text, string expr)
        {
            string result = "";
            //Console.WriteLine("The Expression: " + expr);
            MatchCollection mc = Regex.Matches(text, expr, RegexOptions.Singleline);
            foreach (Match m in mc)
            {
                for (int i = 1; i < m.Groups.Count; ++i)
                    result += (m.Groups[i] + "\n");
            }
            return finalClean(result); //remove all HTML tags and escape ' for use in SQL 
        }

        /// <summary>
        /// Suchen und ersetzen
        /// </summary>
        /// <param name="text">Eingangstext</param>
        /// <param name="expr">Suchausdruck</param>
        /// <param name="replacement">Ersetzungsausdruck</param>
        /// <returns>String- bearbeiteten Text</returns>
        private string replace(string text, string expr, string replacement)
        {
            Regex rgx = new Regex(expr);
            return rgx.Replace(text, replacement);
        }

        ///<summary>
        ///Clean -> remove tags and escape for SQL use 
        /// 
        ///</summary>
        private string finalClean(string input)
        {
            return replace(replace(input, @"<(?:.*?)>", ""), @"'", @"''");
        }

        /// <summary>
        /// Schreibt Nachricht in Tabelle falls diese noch nicht drin ist.
        /// </summary>
        public void writeDatatoDatebase()
        {
            //Insert into Database if not yet in database
            //oDatabase.ExceuteCommand("INSERT INTO bericht (Titel,Quelle,Text) SELECT * FROM (SELECT'" + subject + "','" + link + "','" + summary + "') AS tmp WHERE NOT EXISTS (SELECT 1 FROM bericht WHERE Quelle = '" + link + "');");

            //DatabaseLibrary.CrawlerDB oCrawlerDb = new CrawlerDB();
            //try
            //{
            //    oCrawlerDb.StoreInDatabase(this);
            //}
            //catch (Exception e)
            //{
            //    throw e;
            //}

        }
    }
}
