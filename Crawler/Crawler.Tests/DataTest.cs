// <copyright file="DataTest.cs">Copyright ©  2016</copyright>
using System;
using Crawler;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CrawlerLibrary;

namespace Crawler.Tests
{
    /// <summary>This class contains parameterized unit tests for Data</summary>
    [PexClass(typeof(Data))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class DataTest
    {
        /// <summary>Test stub for .ctor(String, String, String, String)</summary>
        [PexMethod]
        internal Data ConstructorTest(
            [PexAssumeNotNull]string subject,
            [PexAssumeNotNull]string link,
            [PexAssumeNotNull]string summary,
            [PexAssumeNotNull]string regex
        )
        {
            Data target = new Data(subject, link, summary, regex);
            return target;
            // TODO: add assertions to method DataTest.ConstructorTest(String, String, String, String)
        }

        /// <summary>Test stub for .ctor(String, String, String)</summary>
        [PexMethod]
        internal Data ConstructorTest(
            [PexAssumeNotNull]string subject,
            [PexAssumeNotNull]string link,
            [PexAssumeNotNull]string summary
        )
        {
            Data target = new Data(subject, link, summary);
            return target;
            // TODO: add assertions to method DataTest.ConstructorTest(String, String, String)
        }
    }
}
