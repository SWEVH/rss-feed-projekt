using System;
using Crawler;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Crawler.Tests
{
    /// <summary>This class contains parameterized unit tests for RSSReader</summary>
    [TestClass]
    [PexClass(typeof(RSSReader))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class RSSReaderTest
    {

        /// <summary>Test stub for printToConsole()</summary>
        [PexMethod]
        internal void printToConsoleTest([PexAssumeUnderTest][PexAssumeNotNull]RSSReader target)
        {
            target.printToConsole();
            // TODO: add assertions to method RSSReaderTest.printToConsoleTest(RSSReader)
        }

        /// <summary>Test stub for writeToDB()</summary>
        [PexMethod]
        internal void writeToDBTest([PexAssumeUnderTest][PexAssumeNotNull]RSSReader target)
        {
            target.writeToDB();
            // TODO: add assertions to method RSSReaderTest.writeToDBTest(RSSReader)
        }
    }
}
