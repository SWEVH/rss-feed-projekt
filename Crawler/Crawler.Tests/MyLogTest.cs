// <copyright file="MyLogTest.cs">Copyright ©  2016</copyright>

using System;
using Crawler;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Crawler.Tests
{
    /// <summary>This class contains parameterized unit tests for MyLog</summary>
    [TestClass]
    [PexClass(typeof(MyLog))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class MyLogTest
    {

        /// <summary>Test stub for logDivider()</summary>
        [PexMethod]
        internal void logDividerTest()
        {
            MyLog.logDivider();
            // TODO: add assertions to method MyLogTest.logDividerTest()
        }

        /// <summary>Test stub for logException(Exception)</summary>
        [PexMethod]
        internal void logExceptionTest([PexAssumeNotNull]Exception ex)
        {
            MyLog.logException(ex);
            // TODO: add assertions to method MyLogTest.logExceptionTest(Exception)
        }

        /// <summary>Test stub for logString(String)</summary>
        [PexMethod]
        internal void logStringTest(string text)
        {
            MyLog.logString(text);
            // TODO: add assertions to method MyLogTest.logStringTest(String)
        }
    }
}
