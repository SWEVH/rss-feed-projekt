﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CrawlerLibrary;

namespace Crawler
{
    /// <summary>
    /// RSS-Reader für https://www.mercedes-benz.com/de/ressort/mercedes-benz/feed/
    /// </summary>
    class RSSReaderMercedesBenz : RSSReader
    {
        private const string _url = "https://www.mercedes-benz.com/de/ressort/mercedes-benz/feed/";
        private const string _regex = @"(.*)";

        public RSSReaderMercedesBenz()
        {
            XmlReader reader = XmlReader.Create(_url);
            SyndicationFeed feed = SyndicationFeed.Load(reader);
            reader.Close();
            this._title = feed.Title.Text;
            _newsList = new List<Data>();
            foreach (SyndicationItem item in feed.Items)
            {
                _newsList.Add(new Data(item.Title.Text, item.Links.First().Uri.OriginalString, item.Summary.Text, _regex));
            }
        }
    }
}
