﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using CrawlerLibrary;

namespace Crawler
{
    /// <summary>
    /// RSS-Reader für http://www.faz.net/rss/aktuell/
    /// </summary>
    class RSSReaderFAZ : RSSReader
    {
        private const string _url = "http://www.faz.net/rss/aktuell/";
        private const string _regex = @"<p>(.*)</p>";

        public RSSReaderFAZ ()
        {
            XmlReader reader = XmlReader.Create(_url);
            SyndicationFeed feed = SyndicationFeed.Load(reader);
            reader.Close();
            this._title = feed.Title.Text;
            _newsList = new List<Data>();
            foreach (SyndicationItem item in feed.Items)
            {
                _newsList.Add(new Data(item.Title.Text, item.Links.First().Uri.OriginalString, item.Summary.Text, _regex));
            }
        }

    }
}
