﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CrawlerLibrary;

namespace Crawler
{
    /// <summary>
    /// RSS-Reader für http://www.schwarzwaelder-bote.de/.rss.feed
    /// </summary>
    class RssReaderSchwarzwaelderBote : RSSReader
    {
        private const string _url = "http://www.schwarzwaelder-bote.de/.rss.feed";

        public RssReaderSchwarzwaelderBote()
        {
            XmlReader reader = XmlReader.Create(_url);
            SyndicationFeed feed = SyndicationFeed.Load(reader);
            reader.Close();
            this._title = feed.Title.Text;
            _newsList = new List<Data>();
            foreach (SyndicationItem item in feed.Items)
            {
                _newsList.Add(new Data(item.Title.Text, item.Links.First().Uri.OriginalString, item.Summary.Text));
            }
        }
    }
}
