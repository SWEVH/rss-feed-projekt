﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Crawler
{
    static class RSSReaderParameters
    {

        static List<RSSReaderUltimate> list;

        public static void ExecuteReaders()
        {
            MyLog.logString("Crawling FAZ");
            RSSReaderUltimate rssFAZ = new RSSReaderUltimate("http://www.faz.net/rss/aktuell/", @"<p>(.*)</p>",
                RSSTitle.Title, RSSLink.Links, RSSSummary.Summary);
            //rssFAZ.printToConsole();
            //rssFAZ.writeToDB();

            MyLog.logString("Crawling DHBW Stuttgart");
            RSSReaderUltimate rssDHBW = new RSSReaderUltimate("http://www.dhbw-stuttgart.de/home/rss.xml",
                @"(.*)<divider><img.*><div.*>(?:<p>\s?(.*?)\s?<\/p>\s?)+", RSSTitle.Title, RSSLink.Id,
                RSSSummary.SummaryAndContent);
            //rssDHBW.printToConsole();
            //rssDHBW.writeToDB();

            MyLog.logString("Crawling Golem");
            RSSReaderUltimate rssGolem = new RSSReaderUltimate("http://rss.golem.de/rss.php?feed=ATOM1.0", @"(.*)\(<a",
                RSSTitle.Title, RSSLink.Links, RSSSummary.Summary);
            //rssGolem.crawl();
            //rssGolem.printToConsole();
            //rssGolem.writeToDB();

            MyLog.logString("Crawling Mercedes Benz");
            RSSReaderUltimate rssMercedesBenz =
                new RSSReaderUltimate("https://www.mercedes-benz.com/de/ressort/mercedes-benz/feed/", @"(.*)",
                    RSSTitle.Title, RSSLink.Links, RSSSummary.Summary);
            rssMercedesBenz.crawl();
            rssMercedesBenz.printToConsole();
            //rssMercedesBenz.writeToDB();

            MyLog.logString("Crawling Heise");
            RSSReaderUltimate rssHeise = new RSSReaderUltimate("http://www.heise.de/newsticker/heise-atom.xml",null,RSSTitle.Title, RSSLink.Id, RSSSummary.Summary);
            //rssHeise.printToConsole();
            //rssHeise.writeToDB();

            MyLog.logString("Crawling Spiegel");
            RSSReaderUltimate rssSpiegel = new RSSReaderUltimate("http://www.spiegel.de/schlagzeilen/index.rss",null,RSSTitle.Title, RSSLink.Links, RSSSummary.Summary);
            //rssSpiegel.printToConsole();
            //rssSpiegel.writeToDB();

            MyLog.logString("Crawling Schwarzwaelder-Bote");
            RSSReaderUltimate rssSchwarzwaelderBote = new RSSReaderUltimate("http://www.schwarzwaelder-bote.de/.rss.feed",null,RSSTitle.Title, RSSLink.Links, RSSSummary.Summary);
            //rssSchwarzwaelderBote.printToConsole();
            //rssSchwarzwaelderBote.writeToDB();

            //Console.WriteLine(rssSchwarzwaelderBote.Serialize());
        }

        public static void ExecuteCrawlersFromXML()
        {
            List<RSSReaderUltimate> list;

            var serilizer = new XmlSerializer(typeof(List<RSSReaderUltimate>));

            using (var stream = new FileStream(@"CrawlerSettings.xml", FileMode.Open))
            {
                list = serilizer.Deserialize(stream) as List<RSSReaderUltimate>;
            }

            foreach (var Vertreter in list)
            {
                try
                {
                    Console.WriteLine("Crawling: " + Vertreter._url);
                    MyLog.logString("Crawling: " + Vertreter._url);
                    Vertreter.crawl();
                    Vertreter.printToConsole();
                    Vertreter.writeToDB();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    MyLog.logException(ex);
                }

            }
        }
    }
}