﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Crawler
{
    //experimental
    class FAZSinglePage
    {
        public string content;
        private string regex = @"<div class=""FAZArtikelText"" itemprop=""articleBody"">(?:.*?)(<p class=""First PreviewPagemarker"" id=""pageIndex_1"">(?:.*?))(?=<span class=""quelle"">)";
        private string regexnew = @"<div class=""FAZArtikelText"" itemprop=""articleBody"">(.*?)(?=<span class=""quelle"">)";
        // Get Text between <p> tags
        private string regex2 = @"(?:<\s?p(?: class=""First PreviewPagemarker"" id=""pageIndex_1"")?>((?:[„""a-zA-Z]|(?:â\?z)).*?)<\/p>)";
        private string regex2new = @"(?:<\s?p(?: class=""First PreviewPagemarker"" id=""pageIndex_1"")?>((?:[„""a-zA-Z]|(?:â\?z)).*?)<\/p>)|(?:<\s?h[1-6]>((?:[„""a-zA-Z]|(?:â\?z)).*?)<\/h[1-6]>)";
        //remove left inline Tags
        private string regex3 = @"<(?:.*?)>";

        public FAZSinglePage(string url)
        {
            string rawhtml = new System.Net.WebClient().DownloadStringAwareOfEncoding(new Uri(url));
            //System.Console.WriteLine(rawhtml);
            string selectedhtml = showMatch(rawhtml,regexnew);
            //System.Console.WriteLine(selectedhtml);
            string selectedhtml2 = showMatch(selectedhtml, regex2new);
            //System.Console.WriteLine(selectedhtml2);
            content = replace(selectedhtml2, regex3, "");
        }
        private string showMatch(string text, string expr)
        {
            string result = "";
            //Console.WriteLine("The Expression: " + expr);
            MatchCollection mc = Regex.Matches(text, expr, RegexOptions.Singleline);
            foreach (Match m in mc)
            {
                for (int i = 1; i < m.Groups.Count; ++i)
                    result += (m.Groups[i] + "\n");
            }
            return result;
        }

        private string replace(string text, string expr, string replacement)
        {
            Regex rgx = new Regex(expr);
            return rgx.Replace(text, replacement);
        }
    }
}
