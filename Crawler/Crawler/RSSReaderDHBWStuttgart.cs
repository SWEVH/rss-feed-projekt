﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using CrawlerLibrary;

namespace Crawler
{
    /// <summary>
    /// RSS-Reader für http://www.dhbw-stuttgart.de/home/rss.xml
    /// </summary>
    class RSSReaderDHBWStuttgart : RSSReader
    {
        private const string _url = "http://www.dhbw-stuttgart.de/home/rss.xml";
        private const string _regex = @"(.*)<divider><img.*><div.*>(?:<p>\s?(.*?)\s?<\/p>\s?)+";

        public RSSReaderDHBWStuttgart()
        {
            XmlReader reader = XmlReader.Create(_url);
            SyndicationFeed feed = SyndicationFeed.Load(reader);
            reader.Close();
            this._title = feed.Title.Text;
            _newsList = new List<Data>();
            foreach (SyndicationItem item in feed.Items)
            {
                string Content = "";
                foreach (SyndicationElementExtension ext in item.ElementExtensions)
                {
                    if (ext.GetObject<XElement>().Name.LocalName == "encoded")
                        Content += ext.GetObject<XElement>().Value;
                }
                _newsList.Add(new Data(item.Title.Text, item.Id, item.Summary.Text + "<divider>" + Content, _regex));

            }
        }
    }
}
