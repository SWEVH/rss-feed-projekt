﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CrawlerLibrary;

namespace Crawler
{
    /// <summary>
    /// RSS-Reader für http://www.heise.de/newsticker/heise-atom.xml
    /// </summary>
    class RSSReaderHeise : RSSReader
    {
        private string _url = "http://www.heise.de/newsticker/heise-atom.xml";

        public RSSReaderHeise()
        {
            XmlReader reader = XmlReader.Create(_url);
            SyndicationFeed feed = SyndicationFeed.Load(reader);
            reader.Close();
            this._title = feed.Title.Text;
            _newsList = new List<Data>();
            foreach (SyndicationItem item in feed.Items)
            {
                _newsList.Add(new Data(item.Title.Text, item.Id, item.Summary.Text));
            }
        }
    }
}
