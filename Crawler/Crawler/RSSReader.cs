﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrawlerLibrary;
using DatabaseLibrary;

namespace Crawler
{
    ///<summary>
    /// Superklasse für alle RSS-Reader
    /// Stellt gemeinsame Funktionen zur Verfügung
    /// </summary>
    class RSSReader
    {
        /// <value name="_title">Titel des RSS-Feeds</value>
        protected string _title;
        /// <value name="_newsList">Liste mit Data Instanzen, diese enthalten jeweils eine Nachricht des RSS-Streams</value>
        public List<Data> _newsList { get; set; } = new List<Data>();

        /// <summary>
        /// Methode welche alle Nachrichten aus _newsList in der Console ausgibt
        /// </summary>
        public void printToConsole()
        {
            Console.WriteLine(_title);
            Console.WriteLine("-------------------------------------------------------------------------------");
            Console.WriteLine();
            foreach (Data data in _newsList)
            {
                data.printToConsole();
            }
            Console.WriteLine();
            Console.WriteLine("-------------------------------------------------------------------------------");
        }
        /// <summary>
        /// Methode welche alle neuen Nachrichten aus _newsList in die Datenbank speichert, indem sie für jede Data-Instanz die entsprechende Methode aufruft
        /// </summary>
        public void writeToDB()
        {
            foreach (Data d in _newsList)
            {
                try
                {
                    new CrawlerDB().StoreInDatabase(d);
                }
                catch (Exception e)
                {
                    //Todo 
                    throw e;
                }
            }
        }
    }
}
