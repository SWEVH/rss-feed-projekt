﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.ServiceModel.Syndication;
using System.Text.RegularExpressions;
using System.IO;

namespace Crawler
{
    /// <summary>
    /// Crawler ruft sequenziell RSSFeeds ab und speichert neue Nachrichten in Datenbank.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                MyLog.logString("Starting Crawler");

                //RSSReaderParameters.ExecuteReaders();

                RSSReaderParameters.ExecuteCrawlersFromXML();
/*
                MyLog.logString("Crawling FAZ");
                RSSReaderFAZ rssFAZ = new RSSReaderFAZ();
                rssFAZ.printToConsole();
                rssFAZ.writeToDB();

                MyLog.logString("Crawling DHBW Stuttgart");
                RSSReaderDHBWStuttgart rssDHBW = new RSSReaderDHBWStuttgart();
                rssDHBW.printToConsole();
                rssDHBW.writeToDB();

                MyLog.logString("Crawling Golem");
                RSSReaderGolem rssGolem = new RSSReaderGolem();
                rssGolem.printToConsole();
                rssGolem.writeToDB();

                MyLog.logString("Crawling Mercedes Benz");
                RSSReaderMercedesBenz rssMercedesBenz = new RSSReaderMercedesBenz();
                rssMercedesBenz.printToConsole();
                rssMercedesBenz.writeToDB();

                MyLog.logString("Crawling Heise");
                RSSReaderHeise rssHeise = new RSSReaderHeise();
                rssHeise.printToConsole();
                rssHeise.writeToDB();

                MyLog.logString("Crawling Spiegel");
                RSSReaderSpiegel rssSpiegel = new RSSReaderSpiegel();
                rssSpiegel.printToConsole();
                rssSpiegel.writeToDB();

                MyLog.logString("Crawling Schwarzwaelder-Bote");
                RssReaderSchwarzwaelderBote rssSchwarzwaelderBote = new RssReaderSchwarzwaelderBote();
                rssSchwarzwaelderBote.printToConsole();
                rssSchwarzwaelderBote.writeToDB();*/

                //FAZSinglePage news = new FAZSinglePage("http://www.faz.net/aktuell/wirtschaft/netzwirtschaft/was-microsoft-mit-dem-bot-tay-von-der-netzgemeinde-gelernt-hat-14146188.html");
                //System.Console.WriteLine(news.content);

                //FAZSinglePage news = new FAZSinglePage("http://www.faz.net/aktuell/politik/wahl-in-amerika/hillary-clinton-kritisiert-donald-trumps-aeusserung-zu-abtreibung-14152713.html");
                //System.Console.WriteLine(news.content);

                //FAZSinglePage news = new FAZSinglePage(" http://www.faz.net/aktuell/politik/ausland/europa/amerikaner-wollen-panzerbrigade-nach-osteuropa-verlegen-14152757.html");
                //System.Console.WriteLine(news.content);

                MyLog.logString("Crawling completed");
                MyLog.logDivider();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                MyLog.logException(ex);
                MyLog.logDivider();
            }
        }
    }
}
