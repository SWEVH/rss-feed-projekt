﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CrawlerLibrary;

namespace Crawler
{
    /// <summary>
    /// RSS-Reader für http://rss.golem.de/rss.php?feed=ATOM1.0"
    /// </summary>
    class RSSReaderGolem : RSSReader
    {
        private const string _url = "http://rss.golem.de/rss.php?feed=ATOM1.0";
        private const string _regex = @"(.*)\(<a";

        public RSSReaderGolem()
        {
            XmlReader reader = XmlReader.Create(_url);
            SyndicationFeed feed = SyndicationFeed.Load(reader);
            reader.Close();
            this._title = feed.Title.Text;
            _newsList = new List<Data>();
            foreach (SyndicationItem item in feed.Items)
            {
                _newsList.Add(new Data(item.Title.Text, item.Links.First().Uri.OriginalString, item.Summary.Text, _regex));
            }
        }
    }
}
