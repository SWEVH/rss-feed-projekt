﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crawler
{
    /// <summary>
    /// Stellt Loggingfunktion für das Programm bereit
    /// </summary>
    static class MyLog
    {
        /// <value name="_logPath">Pfad zum Log</value>
        private static string _logPath = @"Log.txt";
        /// <value name="_errorLogPath">Pfad zum Fehlerlog</value>
        private static string _errorLogPath = @"Error.txt";
        /// <value name="_dateFormat">Datumsformat: yyyy/MM/dd HH:mm:ss</value>
        private static string _dateFormat = "yyyy/MM/dd HH:mm:ss";

        /// <summary>
        /// Logt einem String im normalen Log
        /// </summary>
        /// <param name="text">Logtext</param>
        public static void logString (string text)
        {
            using (StreamWriter writer = new StreamWriter(_logPath, true))
            {
                writer.WriteLine(DateTime.Now.ToString(_dateFormat) +": " + text);
            }
        }

        /// <summary>
        /// Logt eine fehler Meldung im normalen Log und eine ausfühlichere im Error Log
        /// </summary>
        /// <param name="ex">Exception die gelogt werden soll</param>
        public static void logException(Exception ex)
        {
            using (StreamWriter writer = new StreamWriter(_errorLogPath, true))
            {
                writer.WriteLine("Date: " + DateTime.Now.ToString(_dateFormat) + Environment.NewLine + ex.ToString()) ;
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }
            logString("Error occured, please check Error.txt for details.");
        }

        /// <summary>
        /// Teiler für das Ende des Logs nach einem Programablauf.
        /// </summary>
        public static void logDivider()
        {
            using (StreamWriter writer = new StreamWriter(_logPath, true))
            {
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }
        }
    }
}
