﻿using System;
using System.Runtime.Serialization;

namespace Crawler
{
    [Serializable]
    internal class UnknownEncodingException : Exception
    {
        private string charsetName;
        private ArgumentException ex;
        private string v;

        public UnknownEncodingException()
        {
        }

        public UnknownEncodingException(string message) : base(message)
        {
        }

        public UnknownEncodingException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public UnknownEncodingException(string charsetName, string v, ArgumentException ex)
        {
            this.charsetName = charsetName;
            this.v = v;
            this.ex = ex;
        }

        protected UnknownEncodingException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}