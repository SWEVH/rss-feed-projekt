﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using CrawlerLibrary;
using DatabaseLibrary;

namespace Crawler
{
    public enum RSSTitle
    {
        Title = 0
    }

    public enum RSSLink
    {
        Id = 0,
        Links = 1
    }

    public enum RSSSummary
    {
        Summary = 0,
        SummaryAndContent = 1
    }

    ///<summary>
    /// Superklasse für alle RSS-Reader
    /// 
    /// </summary>
    [Serializable]
    public class RSSReaderUltimate
    {
        /// <value name="_title">Titel des RSS-Feeds</value>
        [XmlIgnore()]
        protected string _title { get; set; }

        /// <value name="_newsList">Liste mit Data Instanzen, diese enthalten jeweils eine Nachricht des RSS-Streams</value>
        [XmlIgnore()]
        public List<Data> _newsList { get; set; } = new List<Data>();

        //Eingabe Parameter
        public string _url { get; set; }
        public string _regex { get; set; }
        public RSSTitle _RssTitle { get; set; }
        public RSSLink _RssLink { get; set; }
        public RSSSummary _RssSummary { get; set; }

        public RSSReaderUltimate()
        {
            
        }

        public RSSReaderUltimate(string url, string regex, RSSTitle rssTitle, RSSLink rssLink, RSSSummary rssSummary)
        {
            _url = url;
            _regex = regex;

            _RssTitle = rssTitle;
            _RssLink = rssLink;
            _RssSummary = rssSummary;
        }


        /// <summary>
        /// Methode welche alle Nachrichten aus _newsList in der Console ausgibt
        /// </summary>
        public void printToConsole()
        {
            Console.WriteLine(_title);
            Console.WriteLine("-------------------------------------------------------------------------------");
            Console.WriteLine();
            foreach (Data data in _newsList)
            {
                data.printToConsole();
            }
            Console.WriteLine();
            Console.WriteLine("-------------------------------------------------------------------------------");
        }

        public void crawl()
        {
            XmlReader reader = XmlReader.Create(_url);
            SyndicationFeed feed = SyndicationFeed.Load(reader);
            reader.Close();
            this._title = feed.Title.Text;
            _newsList = new List<Data>();
            foreach (SyndicationItem item in feed.Items)
            {
                string Content = "";
                if (_RssSummary == RSSSummary.SummaryAndContent)
                {
                    foreach (SyndicationElementExtension ext in item.ElementExtensions)
                    {
                        if (ext.GetObject<XElement>().Name.LocalName == "encoded")
                            Content += ext.GetObject<XElement>().Value;
                    }
                }
                switch (_RssTitle)
                {
                    case RSSTitle.Title:
                        {
                            switch (_RssLink)
                            {
                                case RSSLink.Id:
                                    {
                                        switch (_RssSummary)
                                        {
                                            case RSSSummary.Summary:
                                                {
                                                    _newsList.Add(new Data(item.Title.Text, item.Id,
                                                        item.Summary.Text, _regex));
                                                    break;
                                                }
                                            case RSSSummary.SummaryAndContent:
                                                {
                                                    _newsList.Add(new Data(item.Title.Text, item.Id,
                                                        item.Summary.Text + "<divider>" + Content, _regex));
                                                    break;
                                                }
                                        }
                                        break;
                                    }
                                case RSSLink.Links:
                                    {
                                        switch (_RssSummary)
                                        {
                                            case RSSSummary.Summary:
                                                {
                                                    _newsList.Add(new Data(item.Title.Text, item.Links.First().Uri.OriginalString,
                                                        item.Summary.Text, _regex));
                                                    break;
                                                }
                                            case RSSSummary.SummaryAndContent:
                                                {
                                                    _newsList.Add(new Data(item.Title.Text, item.Links.First().Uri.OriginalString,
                                                        item.Summary.Text + "<divider>" + Content, _regex));
                                                    break;
                                                }
                                        }
                                        break;
                                    }
                            }
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// Methode welche alle neuen Nachrichten aus _newsList in die Datenbank speichert, indem sie für jede Data-Instanz die entsprechende Methode aufruft
        /// </summary>
        public void writeToDB()
        {
            foreach (Data d in _newsList)
            {
                try
                {
                    new CrawlerDB().StoreInDatabase(d);
                }
                catch (Exception e)
                {
                    //Todo 
                    throw e;
                }
            }
        }
    }
}