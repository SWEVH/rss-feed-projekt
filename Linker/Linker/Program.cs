﻿using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Xml;
using LinkerLibrary;
using DatabaseLibrary;

namespace Linker
{
    class Program
    {
        public enum LinkingMethod
        {
            ALL,
            TODAY,
            UNKNOWN,
        };
        static void Main(string[] args)
        {
            #region Objekte erstellen
            System.Diagnostics.Stopwatch oWatch = new Stopwatch();
            oWatch.Start();
            LinkingMethod eMethod = LinkingMethod.UNKNOWN;
            StreamWriter oLogFile = File.AppendText(@"../../../LinkerLog.txt");
            oLogFile.Write("----------- Linker " + DateTime.Now + " -----------" + Environment.NewLine);
            Database oDatabase = new Database("root", "", "rss");
            #endregion

            #region Startparameter auswerten

            if (args.Length != 1)
            {
                oLogFile.Write(DateTime.Now + "Ungültige Startparameter");
                oLogFile.Write("----------- Durchlauf mit Fehlern beendet -----------" + Environment.NewLine);
                oLogFile.Flush();
                oLogFile.Close();
                Environment.Exit(1);
            }
            else
            {
                if (args[0] == "-all")
                {
                    eMethod = LinkingMethod.ALL;
                }
                else if (args[0] == "-today")
                {
                    eMethod = LinkingMethod.TODAY;
                }
                else
                {
                    oLogFile.Write(DateTime.Now + "Ungültige Startparameter");
                    oLogFile.Write("----------- Durchlauf mit Fehlern beendet -----------" + Environment.NewLine);
                    oLogFile.Flush();
                    oLogFile.Close();
                    Environment.Exit(1);
                }
            }
            #endregion

            #region Programmdurchlauf
            List<Document> oDocuments = new List<Document>();
            try
            {
                //DB-Framework
                LinkerDB oDb = new LinkerDB();
                
                   
                //Zuerst alle Interessen holen
                List<Document> oDocumentsList = new List<Document>();
                List<Interesse> oInteressensList = oDb.GetInteressen();

                //List<Interesse> oInteressensList = Utils.CreateInteressenList(oDatabase.ExceuteCommand("SELECT * FROM `interesse` "));
                List<Document> oQueryDocuments = new List<Document>();
                oInteressensList.ForEach(each => oQueryDocuments.Add(Utils.CreateDocumentFromInterest(each)));
                
                //Alle Dokumente Abrufen
                
                if (eMethod == LinkingMethod.ALL)
                {
                    //Alle Dokumente in der DB abrufen
                    oDocumentsList = oDb.GetDocuments(DateTime.MinValue);
;                    //oDocumentsList = Utils.CreateDocuments(oDatabase.ExceuteCommand("SELECT `ID`, `Titel`, `Quelle`, `Text` FROM `bericht`"));
                }
                else if (eMethod == LinkingMethod.TODAY)
                {
                    //Alle Dokumente vom Heutigen Tag abrufen
                    oDocumentsList = oDb.GetDocuments(DateTime.Today);
                    //oDocumentsList = Utils.CreateDocuments(oDatabase.ExceuteCommand("SELECT `ID`, `Titel`, `Quelle`, `Text` FROM `bericht` WHERE DATE(Abrufzeit) = DATE(CURDATE())"));
                }
                else
                {
                    //UNKNOWN - Abschließender Fehlerfall
                    oLogFile.Write(DateTime.Now + "Ungültige Startparameter");
                    oLogFile.Write("----------- Durchlauf mit Fehlern beendet -----------" + Environment.NewLine);
                    oLogFile.Flush();
                    oLogFile.Close();
                    Environment.Exit(1);
                }
                oLogFile.Write(DateTime.Now + ": Anzahl Dokumente: "+oDocumentsList.Count + Environment.NewLine);
                //Jedes Dokument mit Interesse Vergleichen
                foreach (Document oQueryDocument in oQueryDocuments)
                {
                    oLogFile.Write(DateTime.Now + ": Verarbeite Interese mit ID: " + oQueryDocument.GetID() + Environment.NewLine);
                    //Index auf Dokumenten und Suchquery erstellen
                    Index Index = new Index(oDocumentsList);
                    Index.SetQuery(oQueryDocument);
                    Index.BuildIndex();

                    //Ähnlichkeiten von Dokumenten mit Query berechnen
                    Index.CalculateSimilarity();

                    //Ergebnisse erhalten
                    Result oResult = Index.CreateResult();

                    //Ergebnisse in Datenbank zurücktragen
                    //oResult.StoreResultInDatabase();
                    new LinkerDB().StoreResultInDatabase(oResult);
                    //oResult.StoreResultInDatabase();
                }
                oWatch.Stop();
                oLogFile.Write("----------- Laufzeit: "+ oWatch.Elapsed + "-----------" + Environment.NewLine);
                oLogFile.Write("----------- Durchlauf erfolgreich beendet -----------" + Environment.NewLine);
                oLogFile.Flush();
                oLogFile.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                oLogFile.Write(DateTime.Now + ": Fehler: "+e.Message + Environment.NewLine);
                oLogFile.Flush();
                oLogFile.Close();
                Environment.Exit(1);
            }
            Environment.Exit(0);
            #endregion
        }
    }
}
