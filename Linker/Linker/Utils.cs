﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinkerLibrary;

namespace Linker
{
    /// <summary>
    /// Klasse die diverse Funktionen für die einfache Verarbeitung von Daten bereitstellt
    /// </summary>
    public class Utils
    {
        /// <summary>
        /// Erstellt aus den in der ArrayList enthaltenen aus der Datenbank abgerufenen Berichten Document-Objekte welche dann im Index erfasst und durchsucht werden können
        /// </summary>
        /// <param name="oArrayList"></param>
        /// <returns></returns>
        public static List<Document> CreateDocuments(ArrayList oArrayList)
        {
            List<Document> oDocuments = new List<Document>();
            for (int i = 0; i < oArrayList.Count; i += 4)
            {
                try
                {
                    uint nId = uint.Parse(oArrayList[i].ToString());
                    //Hier noch Titel mit Text verbinden um Genauigkeit zu erhöhen
                    oDocuments.Add(new Document(oArrayList[i + 3].ToString(), oArrayList[i + 1].ToString(), nId));
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }

            }
            return oDocuments;
        }

        /// <summary>
        /// Erstellt aus einer Arraylist mit InterssensID und InterssensWörtern eine Liste mit Interessenobjekten
        /// </summary>
        /// <param name="oInteressenSelect"></param>
        /// <returns></returns>
        public static List<Interesse> CreateInteressenList(ArrayList oInteressenSelect)
        {
            List<Interesse> oInteressenList = new List<Interesse>();
            Database oDatabase = new Database("root","","rss");
            for (int i = 0; i < oInteressenSelect.Count; i+=2)
            {
                //Schlagwörter und ID zu einer InteressensID bekommen
                ArrayList oTemp = oDatabase.ExceuteCommand("SELECT schlagwort.ID, schlagwort.Name FROM schlagwort WHERE schlagwort.ID IN(SELECT interessenwoerter.SchlagwortID FROM interessenwoerter WHERE interessenwoerter.InteressenID = "+ uint.Parse(oInteressenSelect[i].ToString()) +")");
                oInteressenList.Add(new Interesse(oTemp,oInteressenSelect[i+1].ToString(), uint.Parse(oInteressenSelect[i].ToString())));
            }
            return oInteressenList;
        }

        /// <summary>
        /// Erstellt aus einem Interessen-Objekt ein Document-Objekt welches anschließend als QueryDocument verwendet werden kann
        /// </summary>
        /// <param name="oInteresse"></param>
        /// <returns></returns>
        public static Document CreateDocumentFromInterest(Interesse oInteresse)
        {
            List<string> InteressenStrings = oInteresse.GetInteressenStrings();
            string sInteressen = "";
            foreach (string sInteresse in InteressenStrings)
            {
                sInteressen += " " + sInteresse;
            }
            return new Document(sInteressen, "Interesse", oInteresse.GetID());
        }



        #region Nicht verwendet
        /// <summary>
        /// Erstellt aus einer ArrayList eine Dokumentenliste
        /// </summary>
        /// <param name="oArrayList"></param>
        /// <returns></returns>
        public static List<Document> CreateInteressenDocuments(ArrayList oArrayList)
        {
            List<Document> oQueryDocuments = new List<Document>();
            for (int i = 0; i < oArrayList.Count; i += 2)
            {
                try
                {
                    uint nID = uint.Parse(oArrayList[i].ToString());
                    oQueryDocuments.Add(new Document(oArrayList[i + 1].ToString(), "Interesse", nID));
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
            return oQueryDocuments;
        }
        #endregion
    }
}