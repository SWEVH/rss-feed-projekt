﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinkerLibrary;

namespace Linker
{
   /// <summary>
    /// Diese Klasse Prepräsentiert einen Index, auf diesem sind Suchen mittels des Vektormodells möglich
    /// </summary>
    public class Index
    {
        List<Document> oDocuments;
        private Document oQueryDocument;
        private List<List<Term>> oTermList;
        private List<List<double>> TfIdfGewichte;   //Die Letzte List in TfIdfGewichte ist der Queryvektor

        #region Konstruktoren

        public Index()
        {
            oDocuments = new List<Document>();
        }

        public Index(Document oDocument)
        {
            oDocuments = new List<Document>();
            oDocuments.Add(oDocument);
        }

        public Index(List<Document> oDocuments)
        {
            this.oDocuments = new List<Document>(oDocuments);
        }


        #endregion

        /// <summary>
        /// Setzt das Querydokument
        /// </summary>
        /// <param name="oQueryDocument"></param>
        public void SetQuery(Document oQueryDocument)
        {
            this.oQueryDocument = oQueryDocument;
            oDocuments.Add(this.oQueryDocument);
        }

        /// <summary>
        /// Erstellt einen Index auf der Dokumentenkollektion und dem Querydokument
        /// Berechnet auch TfIdf-Gewichte
        /// </summary>
        public void BuildIndex()
        {
            foreach (Document oDocument in oDocuments)
            {
                //oDocument.sText = oDocument.sText.Replace('-', ' ');
                oDocument.Stem();
            }
            oTermList = new List<List<Term>>(oDocuments.Count);
            for (int i = 0; i < oDocuments.Count; i++)
            {
                oTermList.Add(new List<Term>());
            }

            #region Alle Dokumente durchgehen
            for (int i = 0; i < oDocuments.Count; i++)
            {
                Document oCurrDocument = oDocuments[i];
                string sTemp = "";
                List<string> lTempTermList = new List<string>();


                //Alle Wörter durchgehen
                for (int j = 0; j < oCurrDocument.sText.Length; j++)
                {
                    char cCurrChar = oCurrDocument.sText[j];
                    if (cCurrChar != ' ') sTemp += cCurrChar;
                    else
                    {
                        //Wenn aktueller Term noch nicht vorhanden ist --> Hinzufügen
                        if (!lTempTermList.Contains(sTemp))
                        {
                            lTempTermList.Add(sTemp);
                            oTermList[i].Add(new Term(sTemp));
                        }
                        else
                        {
                            for (int k = 0; k < oTermList[i].Count; k++)
                            {
                                if (oTermList[i][k].sTerm == sTemp)
                                {
                                    oTermList[i][k].nDocFreq++;
                                    break;
                                }
                            }
                        }
                        sTemp = "";
                    }
                }
                if (!lTempTermList.Contains(sTemp))
                {
                    lTempTermList.Add(sTemp);
                    oTermList[i].Add(new Term(sTemp));
                }
                else
                {
                    for (int k = 0; k < oTermList[i].Count; k++)
                    {
                        if (oTermList[i][k].sTerm == sTemp)
                        {
                            oTermList[i][k].nDocFreq++;
                            break;
                        }
                    }
                }

            }
            #endregion

            //Jetzt eine gemeinsame Termliste erstellen
            List<Term> oGesamtTerms = new List<Term>();
            foreach (Term oTerm in oTermList.SelectMany(oTerms => oTerms.Where(oTerm => !oGesamtTerms.Contains(oTerm))))
            {
                oGesamtTerms.Add(oTerm);
            }

            #region Für jedes Dokument tf-Idf Gewichte für die Terme in oGesamtTerms ermitteln
            TfIdfGewichte = new List<List<double>>();
            for (int i = 0; i < oDocuments.Count; i++)
            {
                TfIdfGewichte.Add(new List<double>());
            }
            int nI = 0;
            foreach (List<Term> oDocTermList in oTermList)
            {
                foreach (Term oTerm in oGesamtTerms)
                {
                    if (oDocTermList.Contains(oTerm))
                    {
                        //TfIdf Ausrechnen
                        int dft = 0;
                        foreach (List<Term> oTerms in oTermList)
                        {
                            if (oTerms.Contains(oTerm)) dft++;
                        }
                        //Formel für TfIdfGewicht = (1 + Log10(Termfreqeunz in diesem Dokument)) * log(Gesamtzahl der Dokumente / Dft)
                        double tfIdfWeigth = (1 + Math.Log10(oTerm.nDocFreq)*Math.Log10(oDocuments.Count/dft));
                        TfIdfGewichte[nI].Add(tfIdfWeigth);   
                    }
                    else
                    {
                        TfIdfGewichte[nI].Add(0);
                    }
                }
                nI++;
            }
#endregion
        }

        /// <summary>
        /// Berechnet für jedes Dokument das Kosinusähnlichkeitsmaß zwischen Dokument und Querydokument
        /// </summary>
        public void CalculateSimilarity()
        {
            double dTemp = 0;
            List<double> SimilarityResults = new List<double>();
            foreach (double bQueryWeight in TfIdfGewichte[TfIdfGewichte.Count - 1])
            {
                dTemp += bQueryWeight * bQueryWeight;
            }
            List<double> QueryVector = TfIdfGewichte[TfIdfGewichte.Count - 1];
            double BetragQueryVector = Math.Sqrt(dTemp);
            //Jeden Documentvector mit dem Queryvector vergleichen
            for (int i = 0; i < TfIdfGewichte.Count - 1; i++)
            {
                dTemp = 0;
                double BetragDocVector = 0;
                foreach (double DocWeight in TfIdfGewichte[i])
                {
                    dTemp += DocWeight * DocWeight;
                }
                BetragDocVector = Math.Sqrt(dTemp);
                double dSimilarity = 0;
                for (int j = 0; j < TfIdfGewichte[i].Count; j++)
                {
                    double oben = (QueryVector[j]* BetragQueryVector);
                    double unten = (TfIdfGewichte[i][j] * BetragDocVector);
                    if (!(oben == 0) && !(unten == 0))
                    {
                        dSimilarity += oben / unten;
                    }
                }
                if (double.IsInfinity(dSimilarity)) dSimilarity = 0;

                SimilarityResults.Add(dSimilarity);
            }
            //Speichern des Ergebnisses in dem jeweiligen Document-Objekt
            for (int i = 0; i < SimilarityResults.Count; i++)
            {
                oDocuments[i].dSimilarity = SimilarityResults[i];
            }
        }

        /// <summary>
        /// Fügt ein Dokument der Kollektion hinzu
        /// @Warning: Darf nur vor Aufruf von BuildIndex() erfolgen
        /// </summary>
        /// <param name="oDocument"></param>
        public void AddDocument(Document oDocument)
        {
            this.oDocuments.Add(oDocument);
        }

        /// <summary>
        /// Gibt die Top N Dokumente des Ergebnisses der Berechnung zurück
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public List<Document> GetNTopDocuments(int n)
        {
            List<Document> oSortedDocuments = new List<Document>();
            Document[] oOriginalDocuments = new Document[oDocuments.Count];
            oDocuments.CopyTo(oOriginalDocuments, 0);
            List<Document> oOriginalDocumentsList = oOriginalDocuments.ToList();
            for (int i = 0; i < oOriginalDocumentsList.Count - 1; i++)
            {
                
                //Bei jedem durchlauf das kleinste Ergebnis zurückgeben
                Document oTempDocument = oOriginalDocumentsList[0];
                for (int j = 0; j < oOriginalDocumentsList.Count - 1; j++)
                {
                    if (oTempDocument.dSimilarity < oOriginalDocumentsList[j].dSimilarity)
                    {
                        oTempDocument = oOriginalDocumentsList[j];
                    }
                }
                //Aufhören wenn keine ähnlichen Dokumente mehr gefunden werden können
                if (oTempDocument.dSimilarity == 0)
                {
                    return oSortedDocuments;
                }
                oSortedDocuments.Add(oTempDocument);
                oOriginalDocumentsList.Remove(oTempDocument);
                if (oSortedDocuments.Count == n)
                {
                    break;
                }
                i--;
            }
            return oSortedDocuments;
        }

        /// <summary>
        /// Erstellt aus dem Ergebnis ein Objekt vom Typ Result
        /// </summary>
        /// <returns></returns>
        public Result CreateResult()
        {
            //Dokumente anhand ihrer Ähnlichkeiten sortiert zurückschreiben

            List<Document> oReturnDocuments = new List<Document>();
            foreach (Document oDocument in oDocuments)
            {
                if (oDocument.dSimilarity != 0) oReturnDocuments.Add(oDocument);
            }
            return new Result(oReturnDocuments, oQueryDocument.GetOriginalText(), oQueryDocument.GetID());
        }

        /// <summary>
        /// Nur für Debug Zwecke
        /// </summary>
        public void PrintIndex()
        {
            foreach (List<Term> oTerms in oTermList)
            {
                foreach (Term oTerm in oTerms)
                {
                    Console.WriteLine("Term: "+oTerm.sTerm+"  Frequenz: "+oTerm.nDocFreq.ToString());
                }
            }   
        }
    }
}
