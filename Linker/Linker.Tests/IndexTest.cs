// <copyright file="IndexTest.cs">Copyright ©  2016</copyright>
using System;
using System.Collections.Generic;
using Linker;
using LinkerLibrary;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Linker.Tests
{
    /// <summary>Diese Klasse enthält parametrisierte Komponententests für Index.</summary>
    [PexClass(typeof(Index))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class IndexTest
    {
        /// <summary>Test-Stub für AddDocument(Document)</summary>
        [PexMethod]
        public void AddDocumentTest([PexAssumeUnderTest]Index target, Document oDocument)
        {
            target = new Index();
            target.AddDocument(oDocument);
            target.SetQuery(oDocument);
            target.BuildIndex();
            target.CalculateSimilarity();
            Result oResult = target.CreateResult();
            Assert.AreEqual(oResult.oDocuments.Length, 1);
        }

        /// <summary>Test-Stub für CalculateSimilarity()</summary>
        [PexMethod]
        public void CalculateSimilarityTest([PexAssumeUnderTest]Index target)
        {
            target.CalculateSimilarity();
            // TODO: Assertionen zu Methode IndexTest.CalculateSimilarityTest(Index) hinzufügen
        }

        /// <summary>Test-Stub für .ctor()</summary>
        [PexMethod]
        public Index ConstructorTest()
        {
            Index target = new Index();
            Assert.AreNotEqual(null,target);
            return target;
        }

        /// <summary>Test-Stub für .ctor(Document)</summary>
        [PexMethod]
        public Index ConstructorTest01(Document oDocument)
        {
            Index target = new Index(oDocument);
            Assert.AreNotEqual(null,target);
            return target;
        }

        /// <summary>Test-Stub für .ctor(List`1&lt;Document&gt;)</summary>
        [PexMethod]
        public Index ConstructorTest02(List<Document> oDocuments)
        {
            Index target = new Index(oDocuments);
            Assert.AreNotEqual(null,target);
            return target;
        }

        /// <summary>Test-Stub für CreateResult()</summary>
        [PexMethod]
        public Result CreateResultTest([PexAssumeUnderTest]Index target)
        {
            Result result = target.CreateResult();
            Assert.AreNotEqual(null,result);
            return result;
        }

        /// <summary>Test-Stub für GetNTopDocuments(Int32)</summary>
        [PexMethod]
        public List<Document> GetNTopDocumentsTest([PexAssumeUnderTest]Index target, int n)
        {
            List<Document> result = target.GetNTopDocuments(n);
            Assert.AreEqual(n,result.Count);
            return result;
        }

        /// <summary>Test-Stub für SetQuery(Document)</summary>
        [PexMethod]
        public void SetQueryTest([PexAssumeUnderTest]Index target, Document oQueryDocument)
        {
            target.SetQuery(oQueryDocument);
        }
    }
}
