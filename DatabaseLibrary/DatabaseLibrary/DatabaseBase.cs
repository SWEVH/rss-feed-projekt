﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace DatabaseLibrary
{
    class DatabaseBase
    {
        private const string sServer = "127.0.0.1";

        private MySqlConnection oConnection;

        public DatabaseBase(string sUsername, string sPassword, string sDatabaseName)
        {
            oConnection = new MySqlConnection("server=" + sServer + ";uid=" + sUsername + ";pwd=" + sPassword + ";database=" + sDatabaseName);
        }

        public DataTable ExecuteCommand(string sMySQLCommand)
        {
            try
            {
                oConnection.Open();
                MySqlCommand oCommand = new MySqlCommand(sMySQLCommand, oConnection);
                MySqlDataReader oReader = oCommand.ExecuteReader();
                DataTable oTable = new DataTable();
                oTable.Load(oReader);
                return oTable;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                oConnection.Close();        
            }
        }

    }
}
