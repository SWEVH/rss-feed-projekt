﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewLibrary;

namespace DatabaseLibrary
{
    public class ViewDB
    {
        private DatabaseBase oDataBase = new DatabaseBase("root", "", "rss");

        /// <summary>
        /// Gibt alle Organisationen die einem User gehören zurück
        /// </summary>
        /// <param name="sEmailAdress"></param>
        /// <returns></returns>
        public List<Organization> GetOrganizations(string sEmailAdress)
        {
            DataTable oTable =
                oDataBase.ExecuteCommand("SELECT `ID`, `Name` FROM `organisation` WHERE `ID` IN(" +
                    "SELECT OrganisationID FROM `userorganisation` WHERE `UserID` = (SELECT `ID` FROM `user` WHERE `Email` = '" +
                    sEmailAdress + "')"+")");
            List<Organization> oOrganizations = new List<Organization>();
            foreach (DataRow row in oTable.Rows)
            {
                Organization oOrganization = new Organization(row.ItemArray[1].ToString());
                oOrganization.ID = uint.Parse(row.ItemArray[0].ToString());
                oOrganizations.Add(oOrganization);
            }
            oOrganizations.ForEach(each =>
            {
                DataTable oInterests =
                    oDataBase.ExecuteCommand(
                        "SELECT * FROM `interesse` WHERE interesse.ID IN(SELECT `InteressenID` FROM `organisationsinteresse` WHERE organisationsinteresse.OrganisationsID = " +
                        each.ID + ")");
                List<Interesse> oInteresses = new List<Interesse>();
                foreach (DataRow row in oInterests.Rows)
                {
                    oInteresses.Add(new Interesse(uint.Parse(row.ItemArray[0].ToString()), row.ItemArray[1].ToString()));
                }
                each.SetInteressen(oInteresses);
            });
            return oOrganizations;
        }

        /// <summary>
        /// Gibt alle Organisationen der Datenbank zurück
        /// </summary>
        /// <returns></returns>
        public List<Organization> GetAllOrganizations()
        {
            try
            {
                //Alle Organisationen abrufen
                DataTable oTable = oDataBase.ExecuteCommand("SELECT `ID`, `Name` FROM `organisation`");
                List<Organization> organizations = new List<Organization>();
                foreach (DataRow row in oTable.Rows)
                {
                    Organization organization = new Organization(row.ItemArray[1].ToString());
                    organization.ID = uint.Parse(row.ItemArray[0].ToString());
                    organizations.Add(organization);
                }
                // Interessen der Organisationen hinzufügen
                organizations.ForEach(each =>
                {
                    DataTable oInterests =
                        oDataBase.ExecuteCommand(
                            "SELECT * FROM `interesse` WHERE interesse.ID IN(SELECT `InteressenID` FROM `organisationsinteresse` WHERE OrganisationsID = " +
                            each.ID + ")");
                    List<Interesse> oInteresses = new List<Interesse>();
                    foreach (DataRow row in oInterests.Rows)
                    {
                        oInteresses.Add(new Interesse(uint.Parse(row.ItemArray[0].ToString()), row.ItemArray[1].ToString()));
                    }
                    each.SetInteressen(oInteresses);
                });
                return organizations;
            }
            catch (Exception e)
            {
                throw e;
            }
            
        }

        public List<Interesse> GetInteressenForUser(string sEmailadress)
        {
            try
            {
                DataTable oResultTable = oDataBase.ExecuteCommand(
                    "SELECT * FROM `interesse` WHERE ID IN(SELECT InteressenID FROM `userinteresse` WHERE UserID = (SELECT ID FROM `user` WHERE Email = '" +
                    sEmailadress + "'))");
                List<Interesse> oInteressen = new List<Interesse>();
                foreach (DataRow row in oResultTable.Rows)
                {
                    oInteressen.Add(new Interesse(uint.Parse(row.ItemArray[0].ToString()), row.ItemArray[1].ToString()));
                }
                return oInteressen;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Interesse> GetAllInteressen()
        {
            try
            {
                DataTable oResultTable = oDataBase.ExecuteCommand("SELECT * FROM `interesse`");
                List<Interesse> oInteressen = new List<Interesse>();
                foreach (DataRow oRow in oResultTable.Rows)
                {
                    oInteressen.Add(new Interesse(uint.Parse(oRow.ItemArray[0].ToString()), oRow.ItemArray[1].ToString()));
                }
                return oInteressen;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void SaveUserOrganizations(List<Organization> oUserOrganizations, string sEmail)
        {
            try
            {
                uint UserID = uint.Parse(oDataBase.ExecuteCommand("SELECT ID FROM `user` WHERE Email ='"+sEmail+"'").Rows[0].ItemArray[0].ToString());
                oDataBase.ExecuteCommand("DELETE FROM `userorganisation` WHERE UserID=" + UserID);
                oUserOrganizations.ForEach(each =>
                {
                    try
                    {
                        oDataBase.ExecuteCommand("INSERT INTO `userorganisation`(`UserID`, `OrganisationID`) VALUES (" +
                                                 UserID + "," + each.ID + ")");
                    }
                    catch (Exception ex)
                    {
                        
                    }
                });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void SaveUserInteressen(List<Interesse> oUserInteressen, string sEmail)
        {
            try
            {
                uint UserID =
                    uint.Parse(
                        oDataBase.ExecuteCommand("SELECT ID FROM `user` WHERE Email='" + sEmail + "'").Rows[0].ItemArray
                            [0].ToString());
                oDataBase.ExecuteCommand("DELETE FROM `userinteresse` WHERE UserID = " + UserID);
                oUserInteressen.ForEach(each =>
                {
                    try
                    {
                        oDataBase.ExecuteCommand(
                            "INSERT INTO `userinteresse`(`UserID`, `InteressenID`) VALUES (" + UserID + "," + each.ID +
                            ")");
                    }
                    catch (Exception ex)
                    {
                        
                    }
                });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool UserReceivesEmail(string sEmail)
        {
            try
            {
                DataTable oTable = oDataBase.ExecuteCommand("SELECT * FROM `useremail` WHERE UserID=(SELECT ID FROM `user` WHERE Email='" + sEmail + "')");
                if (oTable.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<string> GetEmailTime(string sEmail)
        {
            try
            {
                uint UserID = uint.Parse(oDataBase.ExecuteCommand("SELECT ID FROM `user` WHERE Email='" + sEmail + "'").Rows[0].ItemArray[0].ToString());
                DataTable oResultTable =
                    oDataBase.ExecuteCommand("SELECT SendTime FROM `useremail` WHERE UserID=" + UserID.ToString());
                List<string> oTimes = new List<string>();
                foreach (DataRow row in oResultTable.Rows)
                {
                    oTimes.Add(row.ItemArray[0].ToString().Remove(5));
                }
                return oTimes;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void SaveEmailTime(string sEmail, List<string> sSendTime)
        {
            try
            {
                uint UserID =
                    uint.Parse(
                        oDataBase.ExecuteCommand("SELECT ID FROM `user` WHERE Email='" + sEmail + "'").Rows[0].ItemArray
                            [0].ToString());
                oDataBase.ExecuteCommand("DELETE FROM `useremail` WHERE UserID=" + UserID.ToString());
                sSendTime.ForEach(each =>
                {
                    oDataBase.ExecuteCommand("INSERT INTO `useremail`(`UserID`, `SendTime`) VALUES("+UserID+", '"+each + ":00')");
                });
                    
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool IsAdmin(string sEmail)
        {
            try
            {
                uint UserID =
                    uint.Parse(
                        oDataBase.ExecuteCommand("SELECT ID FROM `user` WHERE Email = '" + sEmail + "'").Rows[0]
                            .ItemArray[0].ToString());
                DataTable oAdminTable = oDataBase.ExecuteCommand("SELECT * FROM `admin` WHERE UserID=" + UserID);
                if (oAdminTable.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private uint SaveNewSchlagwort(Schlagwort2 oSchlagwort)
        {
            try
            {
                oDataBase.ExecuteCommand("INSERT INTO `schlagwort`(`Name`) VALUES ('" + oSchlagwort.sSchlagwort + "')");
                DataTable oResultTable =
                    oDataBase.ExecuteCommand("SELECT ID FROM `schlagwort` WHERE Name = '" + oSchlagwort.sSchlagwort +
                                             "'");
                return uint.Parse(oResultTable.Rows[0].ItemArray[0].ToString());
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public uint SaveNewInteresse(Interesse oInteresse)
        {
            try
            {
                if(int.Parse(oDataBase.ExecuteCommand("SELECT COUNT(*) FROM `interesse` WHERE Name='" + oInteresse.sInteresse + "'").Rows[0].ItemArray[0].ToString()) == 0)
                {
                    int oTemp = 0;
                    oInteresse.Schlagworte.ForEach(each =>
                    {
                        oTemp += (int.Parse(oDataBase.ExecuteCommand("SELECT COUNT(*) FROM `schlagwort` WHERE Name='"+each.sSchlagwort+"'").Rows[0].ItemArray[0].ToString()));
                    });
                    if (oTemp == 0)
                    {
                        oDataBase.ExecuteCommand("INSERT INTO `interesse`(`Name`) VALUES ('" + oInteresse.sInteresse +
                                                 "')");
                        uint nInteresseID =
                            uint.Parse(
                                oDataBase.ExecuteCommand("SELECT ID FROM `interesse` WHERE Name='" +
                                                         oInteresse.sInteresse + "'").Rows[0].ItemArray[0].ToString());

                        List<uint> nSchlagwortID = new List<uint>();
                        oInteresse.Schlagworte.ForEach(each =>
                        {
                            nSchlagwortID.Add(SaveNewSchlagwort(each));
                        });
                        //IDs verbinden
                        nSchlagwortID.ForEach(each =>
                        {
                            oDataBase.ExecuteCommand(
                                "INSERT INTO `interessenwoerter`(`InteressenID`, `SchlagwortID`) VALUES (" +
                                nInteresseID + "," +
                                each + ")");
                        });
                        return nInteresseID;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }
      
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public uint SaveNewOrganisation(Organization oOrganization)
        {
            try
            {
                if (int.Parse(oDataBase.ExecuteCommand("SELECT COUNT(Name) FROM `organisation` WHERE Name='"+oOrganization.sOrganization+"'").Rows[0].ItemArray[0].ToString()) == 0)
                {
                    oDataBase.ExecuteCommand("INSERT INTO `organisation`(`Name`) VALUES ('" +
                                             oOrganization.sOrganization + "')");
                    return
                        uint.Parse(
                            oDataBase.ExecuteCommand("SELECT ID FROM `organisation` WHERE Name='" +
                                                     oOrganization.sOrganization + "'").Rows[0].ItemArray[0].ToString());
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void AddInteresseToOrganisation(uint nOrganisationID, Interesse oInteressen)
        {
            try
            {
                
                    oDataBase.ExecuteCommand(
                        "INSERT INTO `organisationsinteresse`(`OrganisationsID`, `InteressenID`) VALUES ("+nOrganisationID+","+oInteressen.ID+")");
                
            }
            catch (Exception e)
            {
                
            }
        }
    }
}
