﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilsLibrary;

namespace DatabaseLibrary
{

    public class InteressenDB
    {
        DatabaseBase oDB = new DatabaseBase("root","","rss");

        public void InsertInteresse(Interesse oInteresse)
        {
            foreach (Schlagwort oSchlagwort in oInteresse.oSchlagworte)
            {
                oDB.ExecuteCommand("INSERT INTO `schlagwort` (`Name`) VALUES ('" + oSchlagwort.sSchlagwort + "')");
                uint nId = uint.Parse(oDB.ExecuteCommand("SELECT `ID` FROM `schlagwort` WHERE `Name` ='" + oSchlagwort.sSchlagwort + "'").Rows[0][0].ToString());
                oSchlagwort.SetId(nId);
            }
            oDB.ExecuteCommand("INSERT INTO `interesse` (`Name`) VALUES ('"+oInteresse.sInteresse+"')");
            uint nID = uint.Parse(oDB.ExecuteCommand("SELECT `ID` FROM `interesse` WHERE `Name` ='" + oInteresse.sInteresse + "'").Rows[0][0].ToString());
            oInteresse.SetID(nID);
            foreach (Schlagwort oSchlagwort in oInteresse.oSchlagworte)
            {
                oDB.ExecuteCommand("INSERT INTO `interessenwoerter` (`InteressenID`, `SchlagwortID`) VALUES ("+oInteresse.GetID()+","+oSchlagwort.GetID()+")");
            }
        }
    }

    public class EmailDB
    {
        public static List<string> GetEmailAdresses()
        {
            List<string> sEmailAdresses = new List<string>();
            DatabaseBase oBase = new DatabaseBase("root","","rss");
            DataTable oResultTable = oBase.ExecuteCommand("SELECT Email FROM `user` WHERE ID IN(SELECT UserID FROM `useremail` WHERE SendTime = '" + DateTime.Now.TimeOfDay.Hours +":00')");
            foreach (DataRow oRow in oResultTable.Rows)
            {
                sEmailAdresses.Add(oRow[0].ToString());
            }
            return sEmailAdresses;
        }
    }
}
