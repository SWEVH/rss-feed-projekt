﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinkerLibrary;
using MySql.Data.MySqlClient;

namespace DatabaseLibrary
{

    public class LinkerDB
    {
        DatabaseBase oDatabase = new DatabaseBase("root","","rss");

        /// <summary>
        /// Gibt alle Dokumente in der Datenbank zurück
        /// </summary>
        public List<Document> GetDocuments()
        {
            DataTable oResult = oDatabase.ExecuteCommand("SELECT * FROM `bericht`");
            List<Document> oDocuments = new List<Document>();
            foreach (DataRow oRow in oResult.Rows)
            {
                uint nId;
                try
                {
                    nId = Convert.ToUInt32(oRow.ItemArray[0]);
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
                oDocuments.Add(new Document(oRow.ItemArray[4].ToString(), oRow.ItemArray[1].ToString(), nId));
            }
            return oDocuments;
        }

        /// <summary>
        /// Gibt alle Dokumente die jünger als das übergebene Datum sind
        /// </summary>
        /// <param name="oTime"></param>
        /// <returns></returns>
        public List<Document> GetDocuments(DateTime oTime)
        {
            DataTable oResult = oDatabase.ExecuteCommand("SELECT * FROM `bericht` WHERE `Abrufzeit` > '" + oTime.ToString("yyyy-MM-dd H:mm:ss") + "'");
            List<Document> oDocuments = new List<Document>();
            foreach (DataRow oRow in oResult.Rows)
            {
                uint nId;
                try
                {
                    nId = Convert.ToUInt32(oRow.ItemArray[0]);
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
                oDocuments.Add(new Document(oRow.ItemArray[4].ToString(), oRow.ItemArray[1].ToString(), nId));
            }
            return oDocuments;
        }

        /// <summary>
        /// Gibt alle Interessen aus der Datenbank zurück
        /// </summary>
        /// <returns></returns>
        public List<Interesse> GetInteressen()
        {
            List<Interesse> oInteressen = new List<Interesse>();
            DataTable oResult = oDatabase.ExecuteCommand("SELECT * FROM `Interesse`");

            for (int i = 0; i < oResult.Rows.Count; i++)
            {
                //Schlagwörter und ID zu einer InteressensID bekommen
                DataTable oTempResult = oDatabase.ExecuteCommand("SELECT ID, Name FROM `schlagwort` WHERE ID IN(SELECT interessenwoerter.SchlagwortID FROM interessenwoerter WHERE interessenwoerter.InteressenID = " + uint.Parse(oResult.Rows[i][0].ToString()) + ")");
                ArrayList oTemp = new ArrayList();
                foreach (DataRow oRow in oTempResult.Rows)
                {
                    foreach (object oObject in oRow.ItemArray)
                    {
                        oTemp.Add(oObject.ToString());
                    }
                }
                oInteressen.Add(new Interesse(oTemp, oResult.Rows[i][1].ToString(), uint.Parse(oResult.Rows[i][0].ToString())));
            }            
            return oInteressen;
        }

        /// <summary>
        /// Speichert ein Objekt vom Typ Result in der Datenbank ab
        /// </summary>
        /// <param name="oResult"></param>
        /// <returns></returns>
        public bool StoreResultInDatabase(Result oResult)
        {
            List<Document> oDocuments = oResult.oDocuments.OrderByDescending(each => each.dSimilarity).ToList();
            foreach (Document oDocument in oDocuments)
            {
                string sQuery = "INSERT INTO `berichtinteresse` (`BerichtID`,`InteressenID`) VALUES (" + oDocument.GetID() + "," + oResult.nInteressenID + ")";
                try
                {
                    oDatabase.ExecuteCommand(sQuery);
                }
                catch (MySql.Data.MySqlClient.MySqlException e)
                {
                    if (e.Number != 1062)
                    {
                        throw new Exception(e.Message);
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return true;
        }
    }

}
 